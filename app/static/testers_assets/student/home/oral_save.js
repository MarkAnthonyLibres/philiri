(function(jq) {

    let que = 0;
    let on_play = 0;

    setInterval(function() {
        que+=1;
    },500);

    jq(".js-play").on("on_play_record", function() {
        on_play +=1;
    });


jq("#get_score").click(function(e) {

    const params = jQuery("form#oral");
    const url = params[0].action;
    const $element = jq(this);
    const container = $element.parents(".parent");
    const spinner = container.find(".overlay-spinner");

    const data = new FormData();
    const inputs = params.serialize_form();

    inputs["time"] = que;
    inputs["playRecord"] = on_play;

    Object.keys(inputs).forEach(function (e) {
        const per_input = inputs[e];
        data.append(e, per_input);
    });

    if (inputs.audio_blob.trim().length <= 0)
         return jq.cus_alert("Please record an audio first!");

    data.append("audio", record_audio, "test.wav");

    const send = jq.ajax({
         enctype: "multipart/form-data",
         processData: false,
         contentType: false,
         cache: false,
         url: url,
         data: data,
         type: "post",
         beforeSend: function ()
         {
            spinner.addClass("show");
         },
         success: function() {
            spinner.addClass("hide");
         },
         headers: {"X-CSRFToken": $.cookie("csrftoken")}
     });

     send.done(function(respond) {
        document.write(respond);
     });

});

})(jQuery);
