(function(jq) {

    jq.fn.populate_list_student = function(url, obj = {})
    {

    const page_container = jq(".pagination");

    obj = obj || {};
    const $element = jq(this);
    const parent_element = $element.parents(".parent");
    $element.jsGrid({
      inserting: false,
      pagerContainer : page_container,
      editing: false,
      filtering: false,
      height: "90%",
      width: "100%",
      heading: false,
      autoload: true,
      pageSize: 10,
      paging:true,
      pageLoading:true,
      pageIndex:1,
      controller: {
        loadData : function(filter)
        {
            filter.search = filter.search || "";
            filter = {...filter,...obj};

            const send = jq.ajax({
                url : url,
                headers: {"X-CSRFToken": jq.cookie("csrftoken")},
                data : filter,
                type : "post"
            });

            return new Promise(function(resolve) {

                send.done(function(response) {
                    const $element = jq(response);
                    const length = $element.attr("item_count");

                    if(!length || length <= 0) return resolve({data : [],itemsCount : 0});

                    resolve({data: [response],itemsCount: length});

                });
            });

        }
      },
      loadIndicator : {
        show : () => parent_element.find(".overlay-spinner").addClass("show"),
        hide : () => parent_element.find(".overlay-spinner").removeClass("show")
      },
      rowRenderer : function(template)
      {
         return jQuery(template);
      },
      noDataContent: function () {
        const container = jq("<div>");
        container.addClass("inner_content")

        const per = jq("<div>");
        container.addClass("no_display");

        const title = jq("<h4>");
        title.html("No display");

        per.append(title);
        per.append('<p>' +
          '<span>'+
          'We cant find any information or source' +
          '</span>' +
          '</p>');

        container.append(per);

        return container;

      }
    });


    }


    const grid = jq(".jsgrid.main");
    grid.populate_list_student("/teacher/task/populate");

    const $form = jq("#main form#search");

    $form.on("keyup",function() {
       const values = $form.serialize_form();
       const send = grid.jsGrid("search",values);
    });

    $form.submit(function(e) {
        e.preventDefault();
    });

    /** load after seconds **/

    const load_data = function() {

        setTimeout(function() {
            const values = $form.serialize_form();
            grid.jsGrid("loadData",values);
            load_data();
        },2000);


    };


    load_data();





})(jQuery);
