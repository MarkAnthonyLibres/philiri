(function(jq) {


$.fn.modal_event_action = function()
{
    const all_modal = jQuery(this);
    all_modal.on("hidden.bs.modal", function() {
        all_modal.find('[data-dismiss="modal"]').click();
    });

    all_modal.on("hide.bs.modal", function() {
        all_modal.css("display","none");
        jq(".modal-backdrop").css("display","none");
    });


}

jQuery(".modal").modal_event_action();


$.fn.init_modal = function(obj)
{

    const on_default = {
        callback_func : typeof(obj) === "function" ? obj : function(){},
        string_parent_container : null,
        confirmation : false,
        confirmation_message : "Are you sure you want to made a changes"
    };

    if(typeof(obj) !== "function")
        obj = {...on_default,obj};
    else
        obj = on_default;


    const $element = jQuery(this);

    const change_state = function(form) {

        const $selected_element = jQuery(form);

        const parent_container = obj.string_parent_container
            ? jq(string_parent_container)
            : $selected_element.parents(".modal");

        const spinner = parent_container.find(".overlay-spinner");
        const data = $selected_element.serialize_form();
        const modal = jQuery(".modal");

        const send = jq.ajax({
            url : form.action,
            headers: {"X-CSRFToken": jq.cookie("csrftoken")},
            data : data,
            type : "post",
            error: function() {
                return alert('Error occured during fetching contents, please reload the page!');
            },
            beforeSend : function() {
                spinner.addClass("show");
            },
            success : function() {
                spinner.removeClass("show");
            }
        });

        send.done(function(response) {
            /** hide all shown and existing modal **/

//            modal.modal('hide');

            /** end modal **/

            $selected_element[0].reset();
            const template = jQuery(response);

            if(!template.is(".modal-content"))
                return alert('Error occured during fetching contents, please reload the page!');

            const that_modal = jq(".modal");
            that_modal.find(".modal-content").before(template);
            that_modal.find(".modal-content.default").addClass("hide");

            that_modal.restore_fullscreen();
            that_modal.on("hidden.bs.modal", function() {
                that_modal.find(".modal-content").not(".default").remove();
                that_modal.find(".modal-content.default").removeClass("hide");
            });

            obj.callback_func(that_modal);
        });

    };

    $element.off("submit");
    $element.submit(function(event) {
        event.preventDefault();
        const that_form = this;

        const validation = $element
           .parsley()
           .validate({force: true});

        if (!validation) return false;

        if(obj.confirmation)
        {

            return vex.dialog.confirm({
                message : obj.confirmation_message,
                callback : function(value)
                {
                    if (!value) return false;
                    return change_state(that_form);
                }
            });

        }

        return change_state(that_form);


    });


};

})(jQuery);



(function(jq) {

const $modal = jq(".modal.task_add_batch_modal");
const wizard = $modal.find("#wizard-steps");
const spinner = $modal.find(".overlay-spinner");


$.fn.iri_wizard = function() {
    const element = jq(this);
    const steps = element.steps({
        state_step : Array(wizard.find(".step-steps li").length).fill(false),
        onChange: function(index, next, has_been_init = false)
        {
            const steps_li = wizard.find(".step-steps li");
            const stepBy_index= jq(steps_li[index]);
            const stepBy_index_href = stepBy_index.find("a").attr("href");

            const root = this;
            this.params = {
                href : stepBy_index_href,
                element : wizard.find(stepBy_index_href),
                index : index,
                li : stepBy_index,
                state : this.state_step[index],
                showError : function() {
                    stepBy_index.removeClass(root.doneClass);
                    stepBy_index.addClass(root.errorClass);
                    root.state_step[index] = false;
                    return false;
                },
                done : function() {
                    root.state_step[index] = true;
                    return true;
                }

            };

            if(has_been_init === "forward")
                wizard.trigger(stepBy_index_href, this.params);

            if(has_been_init === "none")
                return true;

            return this.state_step[index];

   }
    });

    const steps_api = steps.data('plugin_Steps');
    const steps_options = steps_api.options;
    steps_api.options.onChange(
        steps_api.options.startAt,
        steps_api.options.startAt + 1,
        true
    );


};



const step1_form = jq("form#step1_form");
step1_form.init_modal(function(template) {

    const right_spinner = template.find(".right_side .overlay-spinner");

    $.fn.trigger_form_action = function()
    {

        /** start action function **/
        const action = function()
        {
            e.preventDefault();
            const data = jQuery(this).serialize_form();
            const send = jq.ajax({
                url : this.action,
                headers: {"X-CSRFToken": jq.cookie("csrftoken")},
                data : data,
                type : "post",
                error: function() {
                    return alert('Error occured during fetching contents, please reload the page!');
                },
                beforeSend : function() {
                    right_spinner.addClass("show");
                },
                success : function() {
                    right_spinner.removeClass("show");
                }
            });

            send.done(function() {

            });

        };

        /** end action function **/

        const this_element = jq(this);
        this_element.on("submit", action);
    }

    template.find("#wizard-steps").iri_wizard();
    template.find("form#add_passage").on("submit", add_passage);

});

})(jQuery)
