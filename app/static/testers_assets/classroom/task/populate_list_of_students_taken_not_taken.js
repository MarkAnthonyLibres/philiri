(function(jq) {

    jq.fn.populate_list_student = function(url, obj = {})
    {

    obj = obj || {};
    const $element = jq(this);
    const parent_element = $element.parents(".parent");
    $element.jsGrid({
      inserting: false,
      editing: false,
      filtering: false,
      height: "90%",
      width: "100%",
      heading: false,
      paging: true,
      autoload: true,
      pageSize: 6,
      paging:false,
      pageLoading:true,
      pageIndex:1,
      controller: {
        loadData : function(filter)
        {
            filter.search = filter.search || "";
            filter.task = window.fn_task;
            filter.test_type = window.fn_test_type;

            filter = {...filter,...obj};

            const send = jq.ajax({
                url : url,
                headers: {"X-CSRFToken": jq.cookie("csrftoken")},
                data : filter,
                type : "post"
            });

            return new Promise(function(resolve) {

                send.done(function(response) {
                    const $element = jq(response);
                    const length = $element.attr("item_count");

                    if(!length || length <= 0) return resolve({data : [],itemsCount : 0});

                    resolve({data: [response],itemsCount: length});

                });
            });

        }
      },
      loadIndicator : {
        show : () => parent_element.find(".overlay-spinner").addClass("show"),
        hide : () => parent_element.find(".overlay-spinner").removeClass("show")
      },
      rowRenderer : function(template)
      {
         return jQuery(template);
      },
      noDataContent: function () {
        const container = jq("<div>");
        container.addClass("inner_content")

        const per = jq("<div>");
        container.addClass("no_display");

        const title = jq("<h4>");
        title.html("No display");

        per.append(title);
        per.append('<p>' +
          '<span>'+
          'We cant find any information or source' +
          '</span>' +
          '</p>');

        container.append(per);

        return container;

      }
    });


    const $form = jq(".list_task form.search");

    $form.keyup(function() {
       const values = $form.serialize_form();
       const send = $element.jsGrid("search",values);
    });

    $form.submit(function(e) {
        e.preventDefault();
    });

    }


    const taken = jq(".list_completed");
    taken.populate_list_student(
        "/teacher/task/result/students/completed/list",
        { state : 1 }
    );

    const not_taken = jq(".not_list_completed");
    not_taken.populate_list_student(
        "/teacher/task/result/students/completed/list",
        { state : 0 }
    );

    /** load after seconds **/

    const load_data = function() {

        setTimeout(function() {

            taken.jsGrid("loadData");
            not_taken.jsGrid("loadData");
            load_data();


        },2000);


    };


    load_data();





})(jQuery);
