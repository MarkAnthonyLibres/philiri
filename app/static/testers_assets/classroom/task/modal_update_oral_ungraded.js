(function(jq) {


$.fn.modal_get_template = function(url, data) {

    const $parent = jq(this);
    const $modal_body = $parent.find(".modal-body");
    const spinner = $parent.find(".overlay-spinner");

    const send = jq.ajax({
       url : url,
       headers: {"X-CSRFToken": jq.cookie("csrftoken")},
       data : data,
       type : "post",
       error : e => document.write(e),
       beforeSend : function() {
            $modal_body.html("");
            spinner.addClass("show");
       },
       success : function() {
            spinner.removeClass("show");
       }
    });

     send.done(function(response) {
        const template = jq(response);
        $modal_body.append(template);
        $parent.trigger("when_load", template);
     });




};


})(jQuery);



(function(jq) {

    $.fn.clickable_word = function() {
        const $element = jq(this);
        const $all_element = $element.find("*");
        const all_element = $all_element.get();

        const words_finder = function(e)
        {
            if(!e.forEach) return;

            e.forEach(function(that_element) {

                const inner_element = jq(that_element);
                const all_inner_element = inner_element.find("*");
                const content = inner_element.clone()
                    .children() //select all the children
                    .remove()   //remove all the children
                    .end()  //again go back to selected element
                    .text();

                const splitted_text = content.trim().split(/(\s+)/).reverse();
                inner_element[0].childNodes.forEach(e => e.nodeValue = "");

                splitted_text.forEach(function(per_text) {

                    if(per_text.replace(/^\s+|\s+$/gm,'').length == 0)
                        return per_text;

                    const span = jq("<span>");
                    span.addClass("clickable_word--span");
                    span.text([per_text, " "].join(""));

                    span.click(function() {
                        $element.trigger("text_clicked",this);
                    });

                    inner_element.prepend(span);

                });


                if(all_inner_element.length > 0)
                    return words_finder(all_inner_element);

            });


        }

        words_finder(all_element);



    };



})(jQuery);


(function(jq) {

    $.fn.phil_iri_tags_input = function() {

        let $element = jq(this);

        const $parent = $element.parents(".modal");
        const $modal_body = $parent.find(".modal-body");
        const spinner = $parent.find(".overlay-spinner");

        $element.tagsinput({
            tagClass: 'badge badge-success cus-tag',
            itemValue: 'text'
         });

        const send_request = function(form_data ,url, onerror = function() {})
        {
            const send = jq.ajax({
                 url : url,
                 headers: {"X-CSRFToken": jq.cookie("csrftoken")},
                 data : form_data,
                 type : "post",
                 dataType : "json",
                 error : onerror,
                 beforeSend : function() {
                    spinner.addClass("show");
                 },
                 success : function() {
                    spinner.removeClass("show");;
                 }
            });

            send.error(function() {
                spinner.removeClass("show");;
            });

         }

        $element.on('beforeItemAdd', function(event) {

            var tag = event.item;

            const is_prevent = !(!event.options || !event.options.preventPost);
            // Do some processing her

            if (is_prevent) return;

            const that_element = jq(this);
            const that_form = that_element.parents("form");
            const form_data = that_form.serialize_form();

            send_request({...tag,...form_data}, that_form[0].action, function(e) {
                console.log(e);
                $element.tagsinput('remove', tag, {preventPost: true});
            });

        });

        $element.on('beforeItemRemove', function(event) {

            var tag = event.item;
            const is_prevent = !(!event.options || !event.options.preventPost);
            // Do some processing her

            if (is_prevent) return;

            const that_element = jq(this);
            const that_form = that_element.parents("form");
            const form_data = that_form.serialize_form();

            send_request({...tag,...form_data}, "/teacher/task/oral/miscue/remove", function(e) {
                console.log(e);
                $element.tagsinput('add', tag, {preventPost: true});
            });


        });

        $element.on("init", function(e) {

               const that_element = jq(e.currentTarget);
               const data_list = that_element
                .parents(".inputs-tags-custom")
                .find("datalist");

               const options = data_list.find("option").get();
               options.forEach(function(eachItem) {
                    eachItem = jq(eachItem);
                    const obj = {
                        id : eachItem.attr("value"),
                        text : eachItem.text()
                    };

                    that_element.tagsinput('add', obj, {preventPost: true});
               });


        });

        $element.trigger("init");


    }

})(jQuery);


(function(jq) {

jq.fn.play_record = function() {
    $element = jq(this);
    $parent = $element.parents(".recorder");
    $audio = $parent.find("audio");
    audio = $audio[0];
    const progress_container = jq(".audio-player-container");
    const progress_bar_span = progress_container.find("#audio-progress-bar");
    const pointer = progress_container.find("#draggable-point");
    window.play_element = audio;

    var timer;
    var percent = 0;

    pointer.draggable({
        axis: 'x',
        containment: ".audio-player-container"
    });

    pointer.draggable({
        drag: function() {
            var offset = $(this).offset();
            var xPos = (100 * parseFloat($(this).css("left"))) / (parseFloat($(this).parent().css("width")));
            progress_bar_span.css({
                'width': xPos + "%"
            });


         }
    });

    audio.onended = function() {
        progress_bar_span.css("width", '0%');
        pointer.css("left", '0%');
    };

    audio.onpause = function() {

       $parent.removeClass("active");
       $parent
            .find("i.fa-pause")
            .removeClass("fa-pause")
            .addClass("fa-play");

       clearTimeout(timer);

    };

    audio.onplaying = function(_event) {
       $parent.addClass("active");
       $parent
        .find("i.fa-play")
        .removeClass("fa-play")
        .addClass("fa-pause");

        var duration = _event.target.duration;
        advance(duration, audio);

    };

    var advance = function(duration, element) {
        increment = 10/duration
        percent = Math.min(increment * element.currentTime * 10, 100);
        progress_bar_span.css("width", percent+'%');
        pointer.css("left", percent+'%');
        startTimer(duration, element);
    }

    var startTimer = function(duration, element){
        if(percent < 100) {
            timer = setTimeout(function (){advance(duration, element)}, 100);
        }
    }


    function stop() {
        audio.pause();
        $element.off("click").on("click", play)
    }

    function play() {
        audio.play();
        $element.off("click").on("click", stop )
    };

    $element.on("click", play )



};

})(jQuery);


(function(jq) {

    const cus_modal = jq(".modal.update_oral_passage_modal");

    cus_modal.on("hidden.bs.modal", function() {
        jq(".ungraded_passage").jsGrid("loadData");

        if (window.hasOwnProperty("play_element"))
        {
            play_element.pause();
            play_element.currentTime = 0;
        }



    });

    jq(".ungraded_passage").on("template_loaded", function(e, template) {
        template = jq(template);
        template.find("form.update_oral_assesment").submit(function(e) {

            e.preventDefault();
            cus_modal.modal("show");
            try {

                const form_data = jq(this).serialize_form();
                cus_modal.modal_get_template(this.action, form_data);

                cus_modal.on("when_load", function(e, modal_template) {

                    modal_template = jQuery(modal_template);
                    const text_clickable = modal_template.find(".clickable_word");
                    const input_tags = modal_template.find(".inputs-tags-custom .tag_input");
                    const miscue_popup = modal_template.find(".add_miscue");
                    const close_popup = modal_template.find(".close_popup");
                    const play_button = modal_template.find(".js-play");

                    play_button.play_record();

                    text_clickable.clickable_word();
                    input_tags.phil_iri_tags_input();

                    const those_clickable_word = jq(".clickable_word--span");

                    let selected_input_tag = null;


                    //no need to change "input" name the jquery plugin tags already
                    //change it to input

                    modal_template.find(".inputs-tags-custom input").focus(function() {
                        const input_parents = input_tags.parents(".inputs-tags-custom");
                        input_parents.removeClass("active");

                        const self_parent = jq(this).parents(".inputs-tags-custom");
                        self_parent.addClass("active");

                        const index = jq( ".inputs-tags-custom" ).index(self_parent);
                        const self_input = jq(input_tags.get(index));

                        const inputs_items = self_input.tagsinput('items');

                        those_clickable_word.removeClass("active");

                        inputs_items.forEach(function(eachItem) {
                            const $item_element = jq(those_clickable_word.get(eachItem.id));
                            $item_element.addClass("active")
                        })

                        selected_input_tag = self_input;

                    });


                    text_clickable.on("text_clicked", function(e, span) {
                         if(!selected_input_tag) return;
                         const index = jq( ".clickable_word--span").index(span);
                         const $element = jq(span);
                         const inputs_items = selected_input_tag.tagsinput('items');

                         selected_input_tag.tagsinput('add', { id: index, text: $element.text() });

                         inputs_items.forEach(function(eachItem) {
                               const $item_element = jq(those_clickable_word.get(eachItem.id));
                               $item_element.addClass("active");
                         });

                    });

                    modal_template.find("form#next").submit(function(e) {
                        jq(".ungraded_passage").jsGrid("loadData");
                        const form_data = jq(this).serialize_form();
                        cus_modal.modal_get_template(this.action, form_data);
                    });


                });

            }catch(err) {
                console.log(err);
                alert("Error occured");
            }

        });

    });


})(jQuery)
