(function(jq) {

    jq.fn.populate_ungraded_oral = function(url, obj = {})
    {

    obj = obj || {};
    const $element = jq(this);
    const parent_element = $element.parents(".parent");
    $element.jsGrid({
      inserting: false,
      editing: false,
      filtering: false,
      height: "90%",
      width: "100%",
      heading: false,
      paging: true,
      autoload: true,
      pageSize: 6,
      paging:false,
      pageLoading:true,
      pageIndex:1,
      controller: {
        loadData : function(filter)
        {
            filter.search = filter.search || "";
            filter.test_type = window.fn_test_type;
            filter = {...filter,...obj};

            const send = jq.ajax({
                url : url,
                headers: {"X-CSRFToken": jq.cookie("csrftoken")},
                data : filter,
                type : "post",
                error : e => document.write(e)
            });

            return new Promise(function(resolve) {

                send.done(function(response) {
                    const $element = jq(response);
                    const length = $element.attr("item_count");

                    if(!length || length <= 0) return resolve({data : [],itemsCount : 0});

                    resolve({data: [response],itemsCount: length});

                });
            });

        }
      },
//      onDataLoaded : function() {
//            window.jsgrid_data_loaded();
//      },
      loadIndicator : {
        show : () => parent_element.find(".overlay-spinner").addClass("show"),
        hide : () => parent_element.find(".overlay-spinner").removeClass("show")
      },
      rowRenderer : function(template)
      {
         const on_template = jQuery(template);
         $element.trigger("template_loaded",on_template);
         return on_template;
      },
      noDataContent: function () {
        const container = jq("<div>");
        container.addClass("inner_content")

        const per = jq("<div>");
        container.addClass("no_display");

        const title = jq("<h4>");
        title.html("No ungraded passages found");

        per.append(title);
        per.append('<p>' +
          '<a href="#">'+
          'Show more pending request in this activity' +
          '</span>' +
          '</p>');

        container.append(per);

        return container;

      }
    });


    const $form = jq(".list_task form.search");

    $form.keyup(function() {
       const values = $form.serialize_form();
       const send = $element.jsGrid("search",values);
    });

    $form.submit(function(e) {
        e.preventDefault();
    });

    }


    jq(".ungraded_passage").populate_ungraded_oral(
        "/teacher/task/oral/student/pending/populate",
        { quiz : fn_quiz_id }
    );



})(jQuery);
