(function (jq, lodash) {

    const colRenderer = function (value, item) {
        if (!value) return;

        const script_template = jq("script#student_list");

        if (!script_template.length)

            throw new Error("Template not found");

        const firstname = value.firstname
            ? value.firstname + " "
            : "";

        const lastname = value.lastname
            ? value.lastname + " "
            : "";

        const middlename = value.middlename
            ? value.middlename + " "
            : "";


        const gender = !value.gender || Number(value.gender) <= 0 ? "Male" : "Female";

        return String(script_template.html()).setTokens({
            name: firstname + lastname + middlename,
            gender: gender
        });

    };

    const table = jq("#students .student_list").jsGrid({
        headerRowRenderer: e => null,
        filterRowRenderer: e => null,
        height: "auto",
        pagerFormat: "Pages: {first} {prev} {pages} {next} {last}    {pageIndex} of {pageCount}",
        pagerContainer: jq(".pagination"),
        filtering: true,

        autoload: true,
        paging: true,
        pageLoading: true,
        pageIndex: 0,
        pageSize: 8,


        width: "100%",
        controller: {
            loadData: function (args) {

                const deferred = $.Deferred();
                args.search = jq("#search").val() || null;
                args.section = window.section_id;
                args.batch = window.batch_id;
                args.state = jq("button.active[name=state]").val() || null;

                jq.ajax({
                    url: 'class/students',
                    dataType: 'json',
                    type: "post",
                    data: args,
                    headers: {"X-CSRFToken": $.cookie("csrftoken")},
                    error: e => document.write(e.responseText),
                    success: function (result) {

                        let chunk = lodash.chunk(result.data, 2);

                        chunk = chunk.map(function (per) {
                            return {
                                col1: per[0],
                                col2: per[1] || null
                            }
                        });

                        /** required **/

                        deferred.resolve({
                            data: chunk,
                            itemsCount: result.length
                        });

                    }
                });

                return deferred.promise();
            }
        },

        fields: [
            {
                name: "col1",
                headercss: "hide",
                itemTemplate: colRenderer,
            },
            {
                name: "col2",
                headercss: "hide",
                itemTemplate: colRenderer,
            }
        ]
    });

    jq("#students #search").keyup(function () {
        table.jsGrid("loadData");
    });

    jq("#students form#state-type").submit(function (e) {
        e.preventDefault();
        table.jsGrid("loadData");
    })

    String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };

})(jQuery, _);
