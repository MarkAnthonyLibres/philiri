(function (jq) {

    const list_activity = jq(".list_task");
    list_activity.jsGrid("destroy");
    const section_id = window.section_id;

    list_activity.jsGrid({
        height: "90%",
        inserting: false,
        editing: false,
        filtering: false,
        width: "100%",
        paging: true,
        autoload: true,
        pageSize: 4,
        data: [],
        pageLoading: true,
        noDataContent: function () {
            const container = jq("<div>");
            container.addClass("inner_content");
            container.css("padding", "19px");

            const per = jq("<div>");
            container.addClass("no_display");

            const title = jq("<h4>");
            title.html("No activity to display");

            per.append(title);
            per.append('<p>' +
                '<a href="/activity/modal_add?section=' + section_id + '" rel="modal:open">' +
                'Click to add' +
                '</a>' +
                '</p>');

            container.append(per);

            return container;


        },

        headerRowRenderer: function (args) {
            const container = jq("<tr>");
            container.addClass("heading");

            const icon = jq("<td>");
            // icon.css("width", "5%")
            icon.addClass("cell-icon");

            const title = jq("<td>");
            title.addClass("cell-title");
            // title.css("width", "55%");
            title.html("Title");

            const pre_test = jq("<td>");
            pre_test.addClass("cell-status");
            // pre_test.css("width", "15%");
            pre_test.html("Pre Test");

            const post_test = jq("<td>");
            post_test.addClass("cell-status");
            // post_test.css("width", "15%");
            post_test.html("Post Test");

            // const date_added = jq("<td>");
            // date_added.addClass("cell-status");
            // date_added.css("width","20%");
            // date_added.html("Date created");

            container.append(icon);
            container.append(title);
            container.append(pre_test);
            container.append(post_test);
            // container.append(date_added);

            return container;

        },
        rowRenderer: function (item) {

            const main = jq("<row>");

            const container = jq("<tr>");
            container.addClass("task");

            const icon = jq("<td>");
            icon.addClass("cell-icon");
            // icon.css("width", "5%");
            icon.html('<i class="icon-checker high"></i>');
            icon.attr("rowspan","2");

            container.append(icon);

            const title_td = jq("<td>");
            // title_td.css("width", "55%");
            title_td.attr("rowspan","2");
            title_td.addClass("cell-title");

            const title = jq("<div>");

            const title_h4 = jq("<h4>");
            const title_a = jq("<a>");
            let link = "/activity?id={activity}&task=true";
            link = link.setTokens({activity : item.activity_id});

            title_a.attr("href", link);
            title_a.html(String(item.title).ucwords());
            title_h4.append(title_a);
            title.append(title_h4);

            const date = (function () {
                const moment_date = moment(item.date_added, "YYYY-MM-DD");
                if (!moment_date.isValid()) return "";
                return moment_date.format("LL");
            })();

            const _author = String(item.author).capitalize();
            const title_p = jq("<p>");
            title_p.html([_author,date].join(", "));

            const description = jq("<section>");
            description.css("word-break","break-all");
            description.addClass("read_more_toggle more");
            description.html(item.description);
            description.expandable({'height': 100});
            description.read_more();

            title.append(title_p);
            title.append(description);
            title_td.append(title);

            container.append(title_td);

            const pre_test = jq("<td>");
            pre_test.addClass("cell-status");
            pre_test.css({
                // width : "15px",
                height : "50%",
                "vertical-align" : "middle"
            });

            const pre_icon = jq("<i>");
            pre_icon.addClass("icon-check");
            pre_icon.css("font-size","21px");
            pre_icon.css("color", "#468847");

            pre_test.append(pre_icon);

            container.append(pre_test);

            const post_test = jq("<td>");
            post_test.addClass("cell-status");
            post_test.css({
                // width : "15px",
                height : "50%",
                "vertical-align" : "middle"
            });

            const post_icon = jq("<i>");
            post_icon.addClass("icon-remove-sign");
            post_icon.css("font-size","21px");
            post_icon.css("color", "rgb(193, 76, 32)");

            post_test.append(post_icon);

            container.append(post_test);
            main.append(container);


            const container2 = jq("<tr>");
            container2.addClass("task");

            const td1 = jq("<td>");
            td1.addClass("cell-status");
            td1.css({
                // width : "15px",
                height : "50%",
                "vertical-align" : "middle"
            });
            td1.html("9/9");

            //container2.append(td1);

            const td2 = jq("<td>");
            td2.addClass("cell-status");
            td2.css({
                // width : "15px",
                height : "50%",
                "vertical-align" : "middle"
            });
            td2.html("8/10");

            //container2.append(td2);
            //main.append(container2);

            return main;

        },
        onDataLoaded : function() {
          const body_table = this._body.find("table");

          const table_head = jq("<thead>");
          table_head.css("display","none");

          const header = this._header.find("table tr.heading td");

          //table_head.append(header);
          //body_table.prepend(table_head);

        },
        controller: {
            loadData: function (args) {

                args.search = jq("#search_activity").val() || null;

                const send = jq.ajax({
                    method: "POST",
                    data: {...{
                            'section': window.section_id,
                            'batch' : window.batch_id
                        }, ...args},
                    url: "/task/load",
                    error: (e) => document.write(e.responseText),
                    dataType: "json",
                    headers: {"X-CSRFToken": jq.cookie("csrftoken")}
                });

                return new Promise(function (resolve, reject) {
                    send.done(function (response) {

                        const mapper = response.map(function (per) {

                            const fullName = [
                                per.user_added__first_name,
                                per.user_added__last_name
                            ].join(" ").trim();

                            const obj = {
                                title: per.title,
                                user_id: per.user_added__id,
                                activity_id: per.activity__id,
                                date_added: per.exercise_date,
                                description: per.description,
                                get author() {
                                    if (fullName.length) return fullName;
                                    if (String(per.user_added__email).length) return per.user_added__email;

                                    return "@" + per.user_added__username;

                                }
                            };

                            return obj;
                        });

                        const a = {
                            data: mapper,
                            itemsCount: response.length
                        };

                        resolve({
                            data: mapper,
                            itemsCount: response.length
                        });

                    });
                    send.error(reason => reject(reason));
                });


            }
        }
    });


    jq("#search_activity").keyup(function () {
        list_activity.jsGrid("loadData");
    });


})(jQuery);