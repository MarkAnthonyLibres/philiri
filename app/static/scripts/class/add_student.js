(function (jq) {


    const colRenderer = function (value, item) {

        if (!value) return;

        const script_template = jq("script#student_list_add");

        if (!script_template.length)

            throw new Error("Template not found");

        const firstname = value.firstname
            ? value.firstname + " "
            : "";

        const lastname = value.lastname
            ? value.lastname + " "
            : "";

        const middlename = value.middlename
            ? value.middlename + " "
            : "";


        const gender = !value.gender || Number(value.gender) <= 0 ? "Male" : "Female";

        let template =  String(script_template.html()).setTokens({
            name: firstname + lastname + middlename,
            gender: gender,
            student : value.id
        });
        return template;

    };

    const add_form = function (element) {

        const $element = $(element);
        const value = $element.serialize_form();
        const submitButton = $element.find("[type='submit']");

        var l = Ladda.create( submitButton[0] );

        const request = jq.ajax({
            beforeSend : e => l.start(),
            url : "class/student/add",
            data : value,
            type : "post",
            dataType : "json",
            headers: {"X-CSRFToken": $.cookie("csrftoken")},
            error : e => document.write(e.responseText)
        });

        request.done(function () {
            l.stop();

            submitButton.attr("class","ladda-button btn btn-success");
            submitButton.attr("disabled","");
            const label = submitButton.find(".ladda-label");
            label.html('<i class="icon icon-check"></i> Joined')

        });


    };


    const table = jq("#add_student_table").jsGrid({
        headerRowRenderer: e => null,
        filterRowRenderer: e => null,
        height: "383px",
        filtering: true,
        autoload: true,
        paging: false,
        pageLoading: true,
        pageIndex: 0,
        pageSize: 5,
        width: "100%",
        onDataLoaded : function()
        {
            jQuery(".add_student_form").submit(function (e) {

                e.preventDefault();
                try { add_form(this); }catch (e) { throw new Error(e); }

            });
        },
        controller: {
            loadData: function (args) {

                const deferred = $.Deferred();
                args.batch = window.add_batch_id;
                args.section = window.add_section_id;
                args.search = jq("#add_student .search").val()

                jq.ajax({
                    url: 'class/student/not_listed',
                    dataType: 'json',
                    type: "post",
                    data: args,
                    headers: {"X-CSRFToken": $.cookie("csrftoken")},
                    error: e => document.write(e.responseText),
                    success: function (result) {

                        /** required **/
                        deferred.resolve({
                            data: result.map(e => new Object({"col1" : e})),
                            itemsCount: result.length
                        });

                    }
                });

                return deferred.promise();
            }
        },

        fields: [
            {
                name: "col1",
                headercss: "hide",
                itemTemplate: colRenderer
            }
        ]
    });


     jq("#add_student .search").keyup(function () {
        table.jsGrid("loadData");
    });



})(jQuery);