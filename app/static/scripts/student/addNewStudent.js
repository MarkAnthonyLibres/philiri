(function (jq) {

    jq("#generate_key").off("click").on("click", function () {

                const $element = jQuery(this);

                const rand = Array
                    .apply(null, Array(4))
                    .map(e => Math.floor(Math.random() * 100))
                    .join("");

                $element.siblings("input").val(rand);

            });

})(jQuery);