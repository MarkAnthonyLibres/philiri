(function(jq) {

    const sec = jq(".sec");
    sec.addClass("hidden");

    const spinner = jq(".spinner");
    spinner.removeClass("hidden");

    const data = {
        quiz_activity : window.quiz_activity,
        of_is_continue : window.of_is_continue
    };

    const send = jq.ajax({
        url : "/student/activity/get_question",
        method : "post",
        data : data,
        error: (e) => document.write(e.responseText),
        dataType: "html",
        headers: {"X-CSRFToken": jq.cookie("csrftoken")}
    });

    send.done(function(res) {
        container = jq(".container_card").html('');
        container.html(res);
    })

})(jQuery)
