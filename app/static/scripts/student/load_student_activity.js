(function(jq) {

    const $student_activity = jq(".student_activity");

    $student_activity.jsGrid({
        height: "100%",
        inserting: false,
        editing: false,
        filtering: false,
        width: "100%",
        paging: false,
        autoload: true,
        pageSize: 4,
        data: [],
        headerRowRenderer : null,
        filterRowRender : null,
        noDataContent : function() {

            const container = jq("<div>");
            container.addClass("app-page-title no-data");

            const sub_container = jq("<div>");
            sub_container.addClass("text-center");

            const h3 = jq("<h3>");
            h3.text("No task");
            sub_container.append(h3);

            const sub_heading = jq("<div>");
            sub_heading.addClass("page-title-subheading");
            sub_heading.text(
            "We can't find any available task at this moment or ask your teacher for help about this behaviour.");

            sub_container.append(sub_heading);
            container.append(sub_container);



            return container;

        },
        rowRenderer: function (item, index) {

            const container = jq("<div>");
            container.addClass("col-lg-4");

            const sub_container = jq("<div>");
            sub_container.addClass("card mb-3 widget-content widget-a1");

            const widget_wrapper = jq("<div>");
            widget_wrapper.addClass("widget-content-wrapper");

            const left = jq("<div>");
            left.addClass("widget-content-left");

            const left_star_state = jq("<div>");

            left_star_state.addClass("widget-numbers text-primary");

            const left_span = jq("<span>");
            left_span.html('<i class="fa fa-star"></i>');

            left_star_state.append(left_span);
            left.append(left_star_state);
            widget_wrapper.append(left);

            const right = jq("<div>");
            right.addClass("widget-content-right");

            const right_heading = jq("<div>");
            right_heading.addClass("widget-heading");

            const right_link = jq("<a>");
            let link = "/student/activity/test?id={id}"

            link = link.setTokens({
                id : item.id
            });

            right_link.attr("href",link);
            right_link.html(String(item.title).capitalizeAll());

            right_heading.append(right_link);
            right.append(right_heading);

            const right_sub_heading = jq("<div>");
            right_sub_heading.addClass("widget-subheading");

            const right_sub_heading_content = jq("<b>");
            right_sub_heading_content.html(String(item.level).capitalize() + " - ");

            const right_section = jq("<a>");
            right_section.attr("href","#");
            right_section.html(item.section);

            right_sub_heading_content.append(right_section);

            right_sub_heading.append(right_sub_heading_content);
            right.append(right_sub_heading);

            widget_wrapper.append(left);
            widget_wrapper.append(right);
            sub_container.append(widget_wrapper);
            container.append(sub_container);
            return container;

        },
        controller: {
            loadData: function (args) {

                const send = jq.ajax({
                    dataType: "json",
                    method: "GET",
                    url: "/student/load/activity",
                    error: (e) => document.write(e.responseText),
                    headers: {"X-CSRFToken": jq.cookie("csrftoken")}
                });

                return send;




            }
        }
    });


    const refresh_grid = function()
    {
        setTimeout(function()
        {
            $student_activity.jsGrid("loadData");
            refresh_grid();
        },2000);

    };

    //refresh_grid();



})(jQuery);
