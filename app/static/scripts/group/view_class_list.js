(function (jq, lodash) {

    // group/section/batch/list

    window.onload = function () {
        const of_section_id = window.section_id;

        const table = jq("#class div#class_list_table").jsGrid({
            headerRowRenderer: e => null,
            filterRowRenderer: e => null,
            height: "auto",
            pagerFormat: "Pages:  {pages} {next} {last}    {pageIndex} of {pageCount}",
            pagerContainer: jq("#class .pagination-class-list"),
            filtering: true,
            selecting: false,

            autoload: true,
            paging: true,
            pageLoading: true,
            pageIndex: 0,
            pageSize: 5,
            onDataLoaded: function () {
                jq('.read_more_toggle').expandable({
                    'height': 100
                });
            },


            width: "100%",
            noDataContent: "Nothing to show",
            controller: {
                loadData: function (args) {

                    const deferred = $.Deferred();
                    args.search = jq("#class #search-class").val() || null;
                    args.index = of_section_id;

                    jq.ajax({
                        url: '/group/section/batch/list',
                        dataType: 'json',
                        type: "post",
                        data: args,
                        headers: {"X-CSRFToken": $.cookie("csrftoken")},
                        error: e => document.write(e.responseText),
                        success: function (result) {

                            deferred.resolve({
                                data: result.data.map(function (per) {
                                    return {col1: per}
                                }),
                                itemsCount: result.length
                            });

                        }
                    });

                    return deferred.promise();
                }
            },

            fields: [
                {
                    name: "col1",
                    headercss: "hide",
                    itemTemplate: function (value, i) {

                        const script_template = jq("script#class_list");
                        let template = script_template.html();
                        template = template.setTokens({
                            id: value.id,
                            name: String(value.name).toUpperCase(),
                            start_date : moment(String(value.start_date)).format("MMM DD YYYY"),
                            end_date : moment(String(value.end_date)).format("MMM DD YYYY")
                        });
                        return template;
                    },
                }
            ]
        });

        jq("#class #search-class").keyup(function () {
            table.jsGrid("loadData");
        });

        window.test = table;

    }

})(jQuery, _);