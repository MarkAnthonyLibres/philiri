(function (jq, lodash) {

    window.onload = function () {
        const ofGroupId = window.group_id;

        const table = jq("div#section_list_table").jsGrid({
            headerRowRenderer: e => null,
            filterRowRenderer: e => null,
            height: "auto",
            pagerFormat: "Pages:  {pages} {next} {last}    {pageIndex} of {pageCount}",
            pagerContainer: jq(".pagination"),
            filtering: true,
            selecting: false,

            autoload: true,
            paging: true,
            pageLoading: true,
            pageIndex: 0,
            pageSize: 3,
            onDataLoaded: function () {
                jq('.read_more_toggle').expandable({
                    'height': 100
                });
            },


            width: "100%",
            noDataContent: "Nothing to show",
            controller: {
                loadData: function (args) {

                    const deferred = $.Deferred();
                    args.index = ofGroupId;
                    args.search = jq("#search").val() || null;
                    args.state = jq("button.active[name=state]").val() || null;

                    jq.ajax({
                        url: '/groups/section/list',
                        dataType: 'json',
                        type: "post",
                        data: args,
                        headers: {"X-CSRFToken": $.cookie("csrftoken")},
                        error: e => document.write(e.responseText),
                        success: function (result) {

                            deferred.resolve({
                                data: result.data.map(function (per) {
                                    return {col1: per}
                                }),
                                itemsCount: result.length
                            });

                        }
                    });

                    return deferred.promise();
                }
            },

            fields: [
                {
                    name: "col1",
                    headercss: "hide",
                    itemTemplate: function (value, i) {
                        const script_template = jq("script#section_list");
                        let template = script_template.html();
                        template = template.setTokens({
                            id: value.id,
                            name: String(value.name).capitalize() || "SECTION I",
                            description: value.description || "<i>No description</i>",
                            state: value.state
                                ? '<b class="label orange">Active</b>'
                                : '<b class="label">Offline</b>'
                        });
                        return template;
                    },
                }
            ]
        });

        jq("#search").keyup(function () {
            table.jsGrid("loadData");
        });

        jq("form#state-type").submit(function (e) {
            e.preventDefault();
            table.jsGrid("loadData");
        })

        window.test = table;

    }

})(jQuery, _);