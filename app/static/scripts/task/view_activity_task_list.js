(function (jq) {

    const list_activity = jq(".list_activity");
    list_activity.jsGrid("destroy");
    const section_id = window.section_id;

    list_activity.jsGrid({
        height: "90%",
        inserting: false,
        editing: false,
        filtering: false,
        width: "100%",
        paging: true,
        autoload: true,
        pageSize: 4,
        data: [],
        pageLoading: true,
        noDataContent: function () {
            const container = jq("<div>");
            container.addClass("inner_content");
            container.css("padding", "19px");

            const per = jq("<div>");
            container.addClass("no_display");

            const title = jq("<h4>");
            title.html("No activity to display");

            per.append(title);
            per.append('<p>' +
                '<a href="/groups/section/info?index='+section_id+'#activity" rel="modal:open">' +
                'Go to Activity List' +
                '</a>' +
                '</p>');

            container.append(per);

            return container;


        },
        headerRowRenderer: function (args) {
            const container = jq("<tr>");
            container.addClass("heading");

            const icon = jq("<td>");
            icon.css("width", "5%")
            icon.addClass("cell-icon");

            const title = jq("<td>");
            title.addClass("cell-title");
            title.css("width", "55%");
            title.html("Title");

            const author = jq("<td>");
            author.addClass("cell-status");
            author.css("width", "30%");
            author.html("Action");

            // const date_added = jq("<td>");
            // date_added.addClass("cell-status");
            // date_added.css("width","20%");
            // date_added.html("Date created");

            container.append(icon);
            container.append(title);
            container.append(author);
            // container.append(date_added);

            return container;

        },
        rowRenderer: function (item) {

            const container = jq("<tr>");
            container.addClass("task");

            const icon = jq("<td>");
            icon.addClass("cell-icon");
            icon.css("width", "5%");
            icon.html('<i class="icon-checker high"></i>');

            container.append(icon);

            const title = jq("<td>");
            title.css("width", "55%");
            const title_h4 = jq("<h4>");
            const title_a = jq("<a>");
            title_a.attr("href", "/activity?id=" + item.activity_id);
            title_a.html(String(item.title).ucwords());
            title_h4.append(title_a);
            title.append(title_h4);

            const date = (function () {
                const moment_date = moment(item.date_added, "YYYY-MM-DD");
                if (!moment_date.isValid()) return "";
                return moment_date.format("LL");
            })();

            const _author = String(item.author).capitalize();
            const title_p = jq("<p>");
            title_p.html([_author,date].join(", "));

            const description = jq("<section>");
            description.addClass("read_more_toggle more");
            description.html(item.description);
            description.expandable({'height': 100});
            description.read_more();

            title.append(title_p);
            title.append(description);
            container.append(title);

            const action = jq("<td>");
            action.addClass("cell-status");
            action.css("width", "30%");

            const button = jq("<a>");
            button.addClass("btn btn-primary");

            button.attr("href",
                "/task/activity?id="
                + item.activity_id
                + "&section=" + window.section_id
                + "&batch=" + window.batch_id
            );

            const button_span = jq("<span>");
            button_span.addClass("ladda-label");

            const button_icon = jq("<i>");
            button_icon.addClass("icon icon-plus-sign");
            button_icon.css("color","white");

            button_span.append(button_icon);
            button_span.append(" Add");
            button.append(button_span);
            action.append(button);

            container.append(action);

            return container;

        },
        controller: {
            loadData: function (args) {

                args.search = jq("#search_activity").val() || null;
                args.batch = window.batch_id;
                args.group_level = window.group_level;

                const send = jq.ajax({
                    method: "POST",
                    data: {...{'section_id': section_id}, ...args},
                    url: "/task/available",
                    error: (e) => document.write(e.responseText),
                    dataType: "json",
                    headers: {"X-CSRFToken": jq.cookie("csrftoken")}
                });

                return new Promise(function (resolve, reject) {
                    send.done(function (response) {

                        const mapper = response.data.map(function (per) {

                            const fullName = [
                                per.user_added__first_name,
                                per.user_added__last_name
                            ].join(" ").trim();

                            const obj = {
                                title: per.title,
                                user_id: per.user_added__id,
                                activity_id: per.id,
                                date_added: per.date_added,
                                description: per.description,
                                get author() {
                                    if (fullName.length) return fullName;
                                    if (String(per.user_added__email).length) return per.user_added__email;

                                    return "@" + per.user_added__username;

                                }
                            };
                            return obj;
                        });

                        resolve({
                            data: mapper,
                            itemsCount: response.length
                        });

                    });
                    send.error(reason => reject(reason));
                });


            }
        }
    });


    jq("#search_activity").keyup(function () {
        list_activity.jsGrid("loadData");
    });


})(jQuery);