(function () {

    $.fn.phil_iri = {};

    /** To prevent a form submitting again when refresh **/

    String.prototype.setTokens = function (replacePairs, callBack = new Function) {

        let str = this.toString(), key, re;

        for (key in replacePairs) {

            if (!isNaN(key)) key = "\\" + key;

            re = new RegExp("\{" + key + "\}", "gm");
            str = str.replace(re, replacePairs[key]);

            if (typeof callBack !== "function") continue;

            callBack.prototype.constructor({
                current: key,
                isEqual: re,
                value: str
            });


        }
        return str;

    };


    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href)
    }

    String.prototype.capitalize = function () {
        return this.charAt(0).toUpperCase() + String(this.slice(1)).toLowerCase();
    }

    String.prototype.capitalizeAll = function () {
        return this.replace(/(?:^|\s)\S/g,function(a) { return a.toUpperCase() })
    }

    String.prototype.ucwords = function () {
        return this.replace(/(?:^|\s)\S/g, function (a) {
            return a.toUpperCase();
        });
    }

    window.define_const = function (name, value, property = window) {

        Object.defineProperty(property, name, {
            value: value,
            enumerable: true,
            configurable: true,
            writable: false
        });

    };

    window.define_value = function (name, value, property = window) {

        Object.defineProperty(property, name, {
            value: value,
            enumerable: true,
            configurable: true,
            writable: true
        });

    };

    define_const("serialize_form", function () {

        const self = this[0];
        const nodeName = self.nodeName;

        if (nodeName !== "FORM")
            throw new Error("Invalid node");

        if (this.length > 1)
            throw new Error("Invalid length of node");

        const serialize = this.serializeArray();
        const obj = {};

        serialize.forEach(function (value) {
            obj[value.name] = value.value;
        });

        return obj;


    }, $.fn);

    if (jQuery.fn.hasOwnProperty("expandable"))
    {
        jQuery('.read_more_toggle').expandable({
         'height': 100
        });
    }


    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        const tab_by_url_element = $('.nav-tabs a[href="#' + url.split('#')[1] + '"]');
        if(tab_by_url_element.length)
            tab_by_url_element.tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    });

    const card_body_state_hide = function()
    {
        const $target = jQuery(this);
        const $parent = $target.parents(".card");
        const $body = $parent.find(".card-body");
        const $icon = $target.find("i");

        $body.slideUp();
        $icon.removeClass("fa-angle-up");
        $icon.addClass("fa-angle-down");

        jQuery($target).off("click").on("click",function() {
            $body.slideDown();
            $icon.removeClass("fa-angle-down");
            $icon.addClass("fa-angle-up");
            jQuery($target).off("click").on("click",card_body_state_hide);
        });

    }

    jQuery("button.cad_body_state")
        .off("click")
        .on("click",card_body_state_hide);



    window.card_body_state = function()
    {
        console.log(this);
        debugger;
    }



})();


(function(jq)
{

const full_screen_element_off = function(that_element, on = true) {

const element = jq(this === window ? that_element : this);
const parent = element.parents(".modal-lg, .fullscreen-container");
const literal_btn_fullscreen = parent.find("button.btn-fullscreen");

parent.removeClass("fullscreen");
const content = parent.find(".modal-content, fullscreen-content");
content.removeClass("full-height");

if(document.fullscreenElement) document.exitFullscreen();

literal_btn_fullscreen.find("i.fa")
    .removeClass("fa-window-restore")
    .addClass("fa-window-maximize");

literal_btn_fullscreen.find("i.fa").attr("data-original-title","Fullscreen");

if(this && on) element.off("click").on("click", full_screen_element_on);

}


const full_screen_element_on = function(that_element) {

const element = jq(this === window ? that_element : this);
const parent = element.parents(".modal-lg, .fullscreen-container");
const literal_btn_fullscreen = parent.find("button.btn-fullscreen");

parent.addClass("fullscreen");
const content = parent.find(".modal-content, fullscreen-content");
content.addClass("full-height");

try { parent[0].requestFullscreen();  }catch(err) {  }

literal_btn_fullscreen.find("i.fa")
        .removeClass("fa-window-maximize")
        .addClass("fa-window-restore");

literal_btn_fullscreen.find("i.fa").attr("data-original-title","Close to exit fullscreen");
literal_btn_fullscreen.off("click").on("click", full_screen_element_off);

const close_equivalent = parent.find(".header-action button, .close-fullscreen ");

document.addEventListener("fullscreenchange", function(e) {
    if (document.fullscreenElement) return;
    full_screen_element_off(literal_btn_fullscreen[0], false);
    literal_btn_fullscreen.on("click", full_screen_element_on );
});

close_equivalent
    .not(element)
    .on("click", function() {
        full_screen_element_off(this, false);
        literal_btn_fullscreen.on("click", full_screen_element_on );
    });

};


jq(".btn-fullscreen").on("click", full_screen_element_on );

$.fn.fullscreen = function() {
    return jq(this).on("click", full_screen_element_on);
};

$.fn.restore_fullscreen = function() {

    const element = jq(this);

    if (!document.fullscreenElement)
    {
        element
            .find(".btn-fullscreen")
            .off("click")
            .on("click", full_screen_element_on);
        return;
    }

    const parent = element.find(".modal-lg, .fullscreen-container");
    const literal_btn_fullscreen = parent.find("button.btn-fullscreen");


    const content = parent.find(".modal-content, fullscreen-content");
    content.addClass("full-height");

    literal_btn_fullscreen.find("i.fa")
        .removeClass("fa-window-maximize")
        .addClass("fa-window-restore");


    literal_btn_fullscreen.find("i.fa").attr("data-original-title","Close to exit fullscreen");
    literal_btn_fullscreen.off("click").on("click", full_screen_element_off);

    const close_equivalent = parent.find(".header-action button, .close-fullscreen ");

    close_equivalent
        .not(element)
        .off("click")
        .on("click", function() {
            full_screen_element_off(this, false);
            literal_btn_fullscreen.on("click", full_screen_element_on );
        });

}

})(jQuery);



(function(jq) {

jq.cus_alert = function(obj)
{

    const of_default = {
        title : "Alert!",
        body : ""
    };

    obj = typeof(obj) === "object" ? obj : { title : obj };
    obj = {...of_default, ...obj};
    obj.title = obj.title || "Alert!";

    const overlay = jq("<div>");
    overlay.addClass("cus-alert overlay is-visible");

    const container = jq("<div>");
    container.addClass("window-message-container type-alert");

    const content = jq("<div>");
    content.addClass("window-message-container__text");

    const title = jq("<h1>");
    title.text(obj.title);

    const description = jq("<p>");
    description.text(obj.body);

    content.append(title);
    if(obj.body.trim()) content.append(description);

    container.append(content);

    const action = jq("<ul>");
    action.addClass("buttons alert");

    const li = jq("<li>");
    const a1 = jq("<a>");
    a1.text("Ok");

    a1.on("click", function() {
        overlay.remove();
    });

    li.append(a1);
    action.append(li);

    container.append(action);

    const close = jq("<a>");
    close.addClass("alert-close img-replace");
    close.text("Close");

    container.append(close);
    overlay.append(container);

    jq("body").append(overlay);

    return overlay;

}


})(jQuery)

