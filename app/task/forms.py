from django import forms
from .models import StudentsActivity

class StudentsActivityForms(forms.ModelForm):

    class Meta:
        model = StudentsActivity;
        fields = [
            'batch',
            'section',
            'user',
            'date_added'
        ];



    pass;