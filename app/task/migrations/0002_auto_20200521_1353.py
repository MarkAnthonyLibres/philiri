# Generated by Django 3.0 on 2020-05-21 05:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('exercises', '0012_auto_20200521_1353'),
        ('task', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentsactivity',
            name='is_preTest',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='studentsactivity',
            name='name',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='studentsactivity',
            name='passage_set',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='studentsactivity',
            name='activity',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='exercises.Activity'),
        ),
    ]
