from django.shortcuts import render, redirect, HttpResponse
from django.forms.models import model_to_dict
from django.contrib import messages;
from django.core.exceptions import ValidationError
from django.template import RequestContext
from modules import helpers
from group.models import Section, Batch
from .forms import StudentsActivityForms
from .models import StudentsActivity, TempStudentsActivity
from django.core.paginator import Paginator
from django.db.models import Q
from exercises.models import Activity
from datetime import date;
from django.db.models.functions import Concat
from django.db.models import CharField, TextField, Value as V, Exists
import json


# Create your views here.

def v_add(request):
    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "section",
        "batch",
    ], True);

    try:
        ofSection = Section.objects.get(id=data['section']);
        ofBatch = Batch.objects.get(id=data['batch']);

        return render(request, "task/task_list.html", {
            "section_id": ofSection.id,
            "section" : ofSection,
            "batch" : ofBatch,
            "batch_id": ofBatch.id
        });

    except Section.DoesNotExist:
        messages.error(request, "The Section id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Section id does not exist and invalid"
        });

    except Batch.DoesNotExist:
        messages.error(request, "The Batch id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Batch id does not exist and invalid"
        });

    pass;

    return HttpResponse("Success");

    pass;


def v_add_activity(request):
    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "id",
        "section",
        "batch"
    ], True);

    try:
        ofSection = Section.objects.get(id=data['section']);
        ofBatch = Batch.objects.get(id=data['batch']);
        ofActivity = Activity.objects.get(id=data['id']);

        data["user"] = request.user;
        data["section"] = ofSection;
        data["batch"] = ofBatch;
        data["activity"] = ofActivity;

        form = StudentsActivity(**data);

        form.clean();
        form.save();

        return redirect(
            "/class?index=" + str(ofBatch.id)
            +"&section="+ str(ofSection.id)
            +"#manage_task"
        );

        return HttpResponse("Hawd");

    except Section.DoesNotExist:
        messages.error(request, "The Section id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Section id does not exist and invalid"
        });

    except Batch.DoesNotExist:
        messages.error(request, "The Batch id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Batch id does not exist and invalid"
        });

    except Activity.DoesNotExist:
        messages.error(request, "The Activity id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Activity id does not exist and invalid"
        });

    pass;


def r_load_task(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "section",
        "batch"
    ], True);

    db = TempStudentsActivity\
        .objects\
        .filter(**data)\
        .values(
        "section",

        # task added

        "user__id",
        "user__username",
        "user__first_name",
        "user__last_name",
        "user__email",
        "user__is_staff",
        "date_added",
        "batch__name",
        "activity__id",
        "title",
        "description",
        "exercise_date",

        # activity create

        "user_added__id",
        "user_added__username",
        "user_added__first_name",
        "user_added__last_name",
        "user_added__email",
        "user_added__is_staff",
    );

    json_format = json.dumps(list(db), default=helpers._JSON.json_serial);


    return HttpResponse(json_format);

    pass;

def r_load_available_task(request):

    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'pageIndex',
        'pageSize',
        'section_id',
        'search',
        'batch',
        'group_level'
    ], False);

    student_activity = TempStudentsActivity \
                .objects \
                .filter(section=data['section_id'], batch=data['batch']);

    activities = Activity \
        .objects \
        .annotate(
        full_name=Concat(
            'user_added__first_name',
            V(' '),
            'user_added__last_name',
            output_field=TextField()
        )
    ) \
        .filter(
        Q(group_level=data['group_level']),
        Q(title__icontains=data['search']) | Q(full_name__icontains=data['search'])
    ) \
        .exclude(id__in=student_activity)\
        .order_by("-id") \
        .values(
        "id",
        "title",
        "full_name",
        "description",
        "date_added",
        "user_added__id",
        "user_added__username",
        "user_added__first_name",
        "user_added__last_name",
        "user_added__email",
        "user_added__is_staff"
    );

    result = list(activities);

    p = Paginator(result, data['pageSize']);
    page = p.page(data['pageIndex'])

    json_format = json.dumps({
        "data": page.object_list,
        "length": len(result)
    }, default=helpers._JSON.json_serial);

    return HttpResponse(json_format);

    pass;

