from django.urls import path;
from django.contrib import admin
from . import views

urlpatterns = [
    path("task/add", views.v_add, name="task"),
    # path("task/add/activity", views.v_add_activity, name="task"),
    path("task/activity", views.v_add_activity, name="task"),
    path("task/load", views.r_load_task, name="task"),
    path("task/available", views.r_load_available_task, name="task"),
];