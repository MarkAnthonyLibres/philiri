from django.db import models
from django.contrib.auth.models import User
from group.models import Batch,Section, GroupLevel
from datetime import date;
from exercises.models import Activity
from django.core.exceptions import ValidationError
from django.db.models import Sum;
from modules import helpers

# Create your models here.

class StudentsActivity(models.Model):
    id = models.AutoField(primary_key=True);
    section = models.ForeignKey(Section,on_delete=models.CASCADE, default=None, null=False, blank=False);
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=False, blank=False);
    date_added = models.DateField(blank=False, null=False, default=date.today);
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE, default=None, null=False, blank=False);
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, default=None, null=True, blank=True);
    name = models.TextField(default="",null=True, blank=True);
    passage_set = models.IntegerField(default=0);
    is_preTest = models.BooleanField(default=False);
    description = models.TextField(default="",null=True, blank=True);

    def clean(self):

        if self.activity :

            isExists = StudentsActivity.objects.filter(
                section=self.section,
                batch=self.batch,
                activity=self.activity
            );

            if len(isExists):
                raise ValidationError("Activity id is already exists on target Section and Batch");

        pass;

    def of_comprehension_level_percent(self,of_batch_student):

        by_batch_student = of_batch_student\
            .objects\
            .filter(section=self.section,batch=self.batch);

        total_comprehension_score = by_batch_student.\
            aggregate(Sum('rate_percent'))['rate_percent__sum'] or 0;

        total_set = by_batch_student.count();

        if not total_set: return 0;

        total_by = int(total_comprehension_score ) / int(total_set);
        return int(total_by * 100);

        pass;

    def roman_grade_level(self):
        return helpers._String.int_to_roman(self.section.group.level);
        return




class TempStudentsActivity(models.Model):
    section = models.ForeignKey(Section,on_delete=models.DO_NOTHING, default=None, null=False, blank=False);
    user = models.ForeignKey(
        User,
        related_name="temp_student_activity_user_added",
        on_delete=models.DO_NOTHING,
        default=None,
        null=False,
        blank=False
    );
    date_added = models.DateField(blank=False, null=False, default=date.today);
    batch = models.ForeignKey(Batch, on_delete=models.DO_NOTHING, default=None, null=False, blank=False);
    activity = models.ForeignKey(Activity, on_delete=models.DO_NOTHING, default=None, null=False, blank=False);
    title = models.CharField(max_length=100, blank=False, null=False);
    description = models.TextField(max_length=400);
    exercise_date = models.DateField(blank=False, null=False, default=date.today);
    user_added = models.ForeignKey(
        User,
        related_name="temp_student_activity",
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING
    );
    group_level = models.ForeignKey(GroupLevel, on_delete=models.DO_NOTHING, default=None, null=False, blank=False);

    class Meta:
        managed = False
        db_table = "temp_student_activity";

