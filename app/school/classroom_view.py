import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from group.models import GroupLevel, Section, Batch
from account.v_school import \
    has_user_profile, \
    is_not_personal_set , \
    is_personal_set, \
    is_member
import django;
import json;
import random, string;
from class_sem.models import batch_student


# Create your views here.

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def index(request):

    of_group_level = GroupLevel\
        .objects\
        .filter(school=request.user)\
        .order_by("level")\
        .values("id","level","name","date_created","description");

    for per_group in of_group_level:

        per_group['rates'] = GroupLevel\
            .of_comprehension_level_percent(per_group['id'],batch_student);

        per_group['section_length'] = Section\
            .objects\
            .filter(group_id=per_group['id'])\
            .count();

        per_group['roman'] = helpers._String.int_to_roman(per_group['level']);

        pass;

    return render(request, "testers_temp/school/classroom/ins_classroom.html", {
        'title': "HOME",
        "user_profile" : request.session['user_profile'],
        "list_group_levels" : of_group_level,
        "init": ""
    });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def v_add_grade_level(request):

    return render(request, "testers_temp/school/classroom/ins_add_level.html", {
        'title': "HOME",
        "user_profile" : request.session['user_profile'],
        "init": ""
    });

    pass;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def r_add_grade_level(request):

    constraint = helpers.constraint(request, 'POST');
    data = constraint.strict([
        "name",
        "level",
        "description",
        "redirect_to"
    ], False);

    of_group_level = GroupLevel(
        name=data['name'],
        level=data['level'],
        description=data['description'],
        school=request.user
    );

    of_group_level.full_clean();
    of_group_level.clean();
    of_group_level.save();

    messages.success(request, "Successfully addded new class level");

    if bool(int(data['redirect_to'])) or int(data['redirect_to']) > 0 :
        return redirect("/school/classroom/add");

    return redirect("/school/classroom");


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def v_grade(request):

    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        of_group_level = GroupLevel.objects.get(id=data['group']);

        return render(request, "testers_temp/school/classroom/ins_classroom_info.html", {
            'title': "HOME",
            "user_profile" : request.session['user_profile'],
            "group_level" : of_group_level,
            "group_roman" : helpers._String.int_to_roman(of_group_level.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def v_sections_list(request):

    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        of_group_level = GroupLevel.objects.get(id=data['group']);

        of_section = Section\
            .objects\
            .filter(group_id = of_group_level.id)\
            .order_by("-id","name")\
            .values("id","name","description");

        for per_section in of_section:

            per_section['rates'] = Section.of_comprehension_level_percent(
                per_section['id'],
                batch_student
            );
            pass;


        return render(request, "testers_temp/school/classroom/ins_sections.html", {
            'title': "HOME",
            "user_profile" : request.session['user_profile'],
            "group_level" : of_group_level,
            "section_list" : of_section,
            'group_roman' : helpers._String.int_to_roman(of_group_level.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "Group level does not exist and invalid");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def r_gotoPage(request):


    constraint = helpers.constraint(request, 'GET');
    data = constraint.strict([
        "class_level"
    ], True);

    try:
      of_group_level = GroupLevel.objects.get(id=data['class_level']);

      of_section = Section\
          .objects\
          .filter(group_id=data['class_level'])\
          .order_by("name")\
          .values("id","name");

      of_batch = Batch\
          .objects\
          .all()\
          .order_by("start_date")\
          .values("id","name");

      of_json = json.dumps({
          "sections" : list(of_section),
          "batch" : list(of_batch)
      });

      return HttpResponse(of_json);

    except GroupLevel.DoesNotExist:
        messages.error(request, "Group level does not exist and invalid");
        return redirect("/home");

    pass;


#Template

@login_required(login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_edit_grade_info(request):

    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school_id=request.user.id
        );

        return render(request, "testers_temp/school/classroom/ins_edit_classroom_info.html", {
            'title': "HOME",
            "user_profile" : request.session['user_profile'],
            "group_level" : of_group_level,
            "group_roman" : helpers._String.int_to_roman(of_group_level.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;


#Template

@login_required(login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_remove_grade(request):

    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school_id=request.user.id
        );

        return render(request, "testers_temp/school/classroom/ins_remove_warning_classroom.html", {
            'title': "HOME",
            "user_profile" : request.session['user_profile'],
            "group_level" : of_group_level,
            "group_roman" : helpers._String.int_to_roman(of_group_level.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;



#Database Update
#redirect /school/classroom

@login_required(login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def request_remove_grade(request):

    try:

        constraint = helpers.constraint(request, "POST");
        data = constraint.strict([
            "group"
        ], True);

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school_id=request.user.id
        );

        of_group_level.delete();

        messages.success(request,
                        str(of_group_level.name) + "has been removed",
                         extra_tags="Successfully remove");

        return redirect("/school/classroom");

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;




