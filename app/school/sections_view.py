import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from group.models import GroupLevel, Section, Batch
from account.v_school import \
    has_user_profile, \
    is_not_personal_set , \
    is_personal_set, \
    is_member
import django;
import json;
import random, string;
from class_sem.models import batch_student


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def index(request):
    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "section"
        ], True);

        of_section = Section.objects.get(
            id=data['section'],
            group__school=request.user
        );

        rates = Section.of_comprehension_level_percent(
            of_section.id,
            batch_student
        );

        return render(request, "testers_temp/school/classroom/ins_section_info.html", {
            'title': "SECTION",
            "user_profile" : request.session['user_profile'],
            "section" : of_section,
            "group_roman" :  helpers._String.int_to_roman(of_section.group.level),
            "rates" : rates,
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");
    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def v_add_section(request):

    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        of_group_level = GroupLevel.objects.get(id=data['group']);

        return render(request, "testers_temp/school/classroom/ins_add_sections.html", {
            'title': "SECTION",
            "user_profile" : request.session['user_profile'],
            "group_level" : of_group_level,
            "group_roman" :  helpers._String.int_to_roman(of_group_level.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def r_add_section(request):


    try:

        constraint = helpers.constraint(request, "POST");
        data = constraint.strict([
            "name",
            "description",
            "group",
            "redirect_to"
        ], False);

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school=request.user
        );

        of_section = Section(
            name=data['name'],
            description=data['description'],
            group=of_group_level
        );

        of_section.full_clean();
        of_section.clean();
        of_section.save();

        messages.success(request, "Successfully addded new section");

        if bool(int(data['redirect_to'])) or int(data['redirect_to']) > 0 :
            return redirect("/school/section/add?group=" + str(of_group_level.id));

        return redirect("/school/classroom/sections?group=" + str(of_group_level.id));

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def v_batch_list(request):

     try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "section"
        ], True);

        of_section = Section.objects.get(
            id=data['section'],
            group__school=request.user
        );

        of_batch = Batch\
             .objects\
             .filter(school=request.user)\
             .order_by("start_date","end_date")\
             .values("id","name","start_date","end_date","description");

        for per_batch in of_batch:

          per_batch['rates'] = Batch\
             .of_comprehension_level_percent(per_batch['id'],batch_student);

        pass;

        return render(request, "testers_temp/school/classroom/ins_batch.html", {
            'title': "SECTION",
            "user_profile" : request.session['user_profile'],
            "section" : of_section,
            "batch_lists" : of_batch,
            "group_roman" :  helpers._String.int_to_roman(of_section.group.level),
            "init": ""
        });

     except helpers.InvalidRequest:
        return redirect("/home");

     except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");
     pass;


     pass;
