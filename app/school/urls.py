
from . import views
from . import classroom_view
from . import sections_view
from . import batch_view
from django.urls import path

urlpatterns = [
    path("school/home", views.index, name="home"),
    path("school/home/info", views.v_info_set, name="home"),
    path("school/home/info/set", views.r_info_set, name="home"),
    path("school/classroom", classroom_view.index, name="ins_classroom.html"),
    path("school/classroom/add", classroom_view.v_add_grade_level, name="home"),
    path("school/classroom/add/request", classroom_view.r_add_grade_level, name="home"),
    path("school/classroom/view", classroom_view.v_grade, name="ins_classroom_info.html"),
    path("school/classroom/edit", classroom_view.v_edit_grade_info, name="ins_classroom_info.html"),
    path("school/classroom/remove", classroom_view.v_remove_grade, name="ins_classroom_info.html"),
    path("school/classroom/remove/request", classroom_view.request_remove_grade, name="ins_classroom_info.html"),

    path("school/classroom/sections", classroom_view.v_sections_list, name="home"),
    path("school/section/add", sections_view.v_add_section, name="home"),
    path("school/section/add/request", sections_view.r_add_section, name="home"),
    path("school/section", sections_view.index, name="home"),
    path("school/classroom/goto", classroom_view.r_gotoPage, name="home"),
    path("school/section/batch", sections_view.v_batch_list, name="home"),
    path("school/batch", batch_view.index, name="home"),
    path("school/batch/add", batch_view.v_add_batch, name="home"),
    path("school/batch/add/request", batch_view.r_add_batch, name="home"),
    path("school/batch/is_exists", batch_view.check_date_is_exists_batch, name="home"),



]
