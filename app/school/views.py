import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from account.v_school import \
    has_user_profile, \
    is_not_personal_set , \
    is_personal_set, \
    is_member
import django;
import json;
import random, string;



# Create your views here.

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def index(request):

    of_user_profile = UserProfile.objects.get(user=request.user);

    user_profile_model = helpers._Model(of_user_profile);
    user_profile_info = user_profile_model.getValue();

    request.session['user_profile'] = user_profile_info;

    return render(request, "testers_temp/school/ins_home.html", {
        'title': "HOME",
        "user_profile" : request.session['user_profile'],
        "init": ""
    });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_not_personal_set, login_url="/school/home")
def v_info_set(request):
    try:
        of_user_profile = UserProfile.objects.get(user=request.user);
    except UserProfile.DoesNotExist:
        of_user_profile = None;
        pass;

    return render(request, "testers_temp/school/ins_required.html", {
        'title': "HOME",
        'school_type': UserProfile.school_type,
        "user": request.user,
        "user_profile": of_user_profile,
        "init": ""
    });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_not_personal_set, login_url="/school/home")
def r_info_set(request):
    constraint = helpers.constraint(request, 'POST');
    data = constraint.strict([
        "email",
        "name_alias",
        "type"
    ], True);

    request.user.email = data['email'];
    request.user.full_clean();
    request.user.save();

    try:

        of_user_profile = UserProfile.objects.get(user=request.user);
        of_user_profile.name_alias = data['name_alias'];
        of_user_profile.type=data['type'];

        of_user_profile.save();

    except UserProfile.DoesNotExist:

        of_user_profile = UserProfile(
            user=request.user,
            school=request.user,
            name_alias=data['name_alias'],
            type=data['type']
        );

        of_user_profile.clean();
        of_user_profile.full_clean();
        of_user_profile.save();

    pass;

    return redirect("/home");

pass;
