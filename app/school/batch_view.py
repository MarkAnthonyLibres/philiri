import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from group.models import GroupLevel, Section, Batch
from account.v_school import \
    has_user_profile, \
    is_not_personal_set, \
    is_personal_set, \
    is_member
import django;
import json;
import random, string;
from class_sem.models import batch_student
from django.core.exceptions import ValidationError;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def index(request):

    of_batch = Batch\
        .objects\
        .filter(school=request.user)\
        .order_by("start_date","end_date")\
        .values("id","name","start_date","end_date","description");

    for per_batch in of_batch:

        per_batch['rates'] = Batch\
            .of_comprehension_level_percent(per_batch['id'],batch_student);

        pass;

    return render(request, "testers_temp/school/batch/ins_index.html", {
        'title': "BATCH",
        "user_profile": request.session['user_profile'],
        "batch" : of_batch,
        # "list_group_levels" : of_group_level,
        "init": ""
    });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def v_add_batch(request):
    return render(request, "testers_temp/school/batch/ins_add_batch.html", {
        'title': "BATCH",
        "user_profile": request.session['user_profile'],
        # "list_group_levels" : of_group_level,
        "init": ""
    });


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def check_date_is_exists_batch(request):
    if len(request.GET) > 0:

        # Batch.check_if_date_has_precesion()

        of_get = dict(request.GET);
        of_get = list(of_get.keys());

        check: bool = Batch.check_if_date_has_precesion(request.GET[of_get[0]]);

        if check: raise Exception("This value has already have precision");

    return HttpResponse("Not Exist");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/school/home/info")
def r_add_batch(request):
    try:

        constraint = helpers.constraint(request, "POST");
        data = constraint.safe({
            "name": True,
            "start_date": True,
            "end_date": True,
            "description": False,
            "redirect_to": True
        });

        of_batch = Batch(
            name=data['name'],
            start_date=data['start_date'],
            end_date=data['end_date'],
            description=data['description'],
            school=request.user
        );

        of_batch.full_clean();
        of_batch.clean();
        of_batch.save();

        if bool(int(data['redirect_to'])) or int(data['redirect_to']) > 0 :
            return redirect("/school/batch/add");

        return redirect("/school/batch");

    except ValidationError as e:
        error_list = dict(e);
        error_list = list(error_list['__all__']);

        error = error_list[0];

        return render(request, "testers_temp/school/batch/ins_add_batch.html", {
            'title': "BATCH",
            "user_profile": request.session['user_profile'],
            "request" : data,
            "init": "",
            "validation_error" : error
        });

        pass;
    except helpers.InvalidRequest:
        return redirect("/school/batch/add");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;
