from django.shortcuts import render, redirect, HttpResponse
from django.forms.models import model_to_dict
from modules import helpers
from group.models import Section, Batch, GroupLevel
from django.contrib import messages;
from .models import Activity, Assesment, Questions, Quiz_choices, Quiz_activity
from .forms import AssesmentForm
from django.forms.models import model_to_dict
import json
import simplejson
from django.core.paginator import Paginator
from django.db.models import Q
from django.db.models.functions import Concat
from django.db.models import CharField, TextField, Value as V
from exercises.models import Task


# Create your views here.


def v_modal_activity(request):
    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "group_level"
    ], False);

    try:
        # Error if does not exists
        group_level = GroupLevel.objects.get(id=data['group_level']);

        return render(request, "class/add_activity.html", {
            'title': "TASK",
            "init": "",
            "group_level": data['group_level'],
            "batch" : request.GET["batch"] if "batch" in request.GET else '',
            "section": request.GET["section"] if "section" in request.GET else ''

        });

    except GroupLevel.DoesNotExist:

        messages.error(request, "Group level does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "Group level does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");


def v_show_list_activity(request):

    # helpers.constraint.session_set_pages(request);

    try:
        get_request = helpers.constraint(request, "GET");
        data = get_request.strict([
            "id"
        ], False);

        ofActivity = Activity.objects.get(id=data['id']);
        ofActivity_model = helpers._Model(ofActivity);
        ofActivity_info = ofActivity_model.getValue();

        return render(request, "testers_temp/index.html", {
            'title': "ADD",
            "activity_id": data['id'],
            "activity": ofActivity,
            "data": ofActivity,
            "is_task" : 1 if "task" in request.GET else 0
        });

    except AttributeError:
        messages.error(request, "No selected activity specified");
        return redirect("/");

    except Activity.DoesNotExist:

        messages.error(request, "section does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "the specified activity does not exist and invalid"
        });

    pass;


def r_new_activity(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "title",
        "description",
        "group_level",
        "batch",
        "section"
    ], False);

    data["user_added"] = request.user;

    try:
        # Error if does not exists
        data["group_level"] = GroupLevel.objects.get(id=data['group_level']);

        del data["batch"];
        del data["section"];

        details = Activity(**data);
        details.clean();

        result = details.save();

        if ("batch" in request.POST and request.POST['batch']) \
                and ("section" in request.POST and request.POST['section']) :

            of_batch = Batch.objects.get(id=request.POST['batch']);
            of_section = Section.objects.get(id=request.POST['section']);

            add_task = dict();

            add_task["user"] = request.user;
            add_task["section"] = of_section;
            add_task["batch"] = of_batch;
            add_task["activity"] = details;

            student_activity = Task(**add_task);
            student_activity.clean();
            student_activity.save();

            pass;


        return redirect("/activity?id=" + str(details.id));


    except Section.DoesNotExist:

        messages.error(request, "section does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "section does not exist and invalid"
        });

    except GroupLevel.DoesNotExist:

        messages.error(request, "Group level does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "Group level does not exist and invalid"
        });

    except Batch.DoesNotExist:

        messages.error(request, "batch does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "batch does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def v_assesment(request):
    # helpers.constraint.session_set_pages(request);

    get_request = helpers.constraint(request, "GET");
    pars = ["index", "type"];
    if "assesment" in get_request.GET_value: pars.append("assesment");
    data = get_request.strict(pars, False);

    try:
        # Error if does not exists
        section = Activity.objects.get(id=data['index']);
        data_respond = {"request_pars" : data};

        if "assesment" in pars:
            _assesment = Assesment \
                .objects \
                .get(id=data['assesment']);

            data_respond["assesment"] = _assesment.__dict__;
            pass;

        assesment_proto = Assesment(type=int(data['type']));
        type = assesment_proto.get_type_display();

        return render(request, "testers_temp/" + str(type).lower() + ".html", {
            'title': "ORAL",
            "activity_id": section.id,
            "activity": section,
            "activity_id" : section.id,
            "activity_title" : section.title,
            "assesment_id": data["assesment"] if "assesment" in data else "",
            "assesment_type": type,
            **data_respond
        });

    except Activity.DoesNotExist:

        messages.error(request, "The Activity id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Activity id does not exist and invalid"
        });

    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def v_questions(request):
    # helpers.constraint.session_set_pages(request);

    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "id"
    ], False);

    try:
        # Error if does not exists
        model = Assesment.objects.get(id=data['id']);

        return render(request, "testers_temp/question.html", {
            'title': "Questions",
            "init": "",
            "assesment": data['id'],
            "activity_id": model.activity.id,
            "activity_title": model.activity.title,
            "assesment_id": model.id,
            "model": model,
            "assesment_type": model.get_type_display(),
            "activity": model.activity
        });

    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def save_section(request):

    form = AssesmentForm(data=request.POST, files=request.FILES);

    if form.is_valid():

        data = form.save();

        obj = {
            "id": data.id,
            "state": True
        }

        messages.info(request, "Successfully added");

        json_format = json.dumps(obj);
        return redirect("/activity/questions?id=" + str(data.id));

    else:
        print(form.errors);
        raise AttributeError("Invalid Request");

    return HttpResponse(id);

    pass;


def update_section(request):
    post_request = helpers.constraint(request, "POST");
    post_data = post_request.strict([
        "assesment"
    ], False);

    try:

        _assesment = Assesment.objects.get(id=request.POST['assesment']);
        form = AssesmentForm(request.POST or None, request.FILES or None, instance=_assesment)

        if form.is_valid():
            edit = form.save(commit=False)
            edit.save()

        messages.info(request, "Successfully Update");
        return redirect("/activity/questions?id=" + str(_assesment.id));

    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def r_questions_add(request):
    data = json.loads(request.body);
    post_request = helpers.constraint(request, "POST");
    question = post_request.filter_keys(store=["body","type","assesment"], value=data);
    question["user"] = request.user;

    try:

        question["assesment"] = Assesment.objects.get(id=question["assesment"]);

        r_question = Questions(**question);
        r_question.clean();
        result = r_question.save();

        for per in data["choices"]:
            choices = post_request.filter_keys(store=["body", "is_correct"], value=per);
            choices["question"] = r_question

            r_choices = Quiz_choices(**choices);
            r_choices.clean();
            r_choices.save();

            pass;

        return HttpResponse("{}");


    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def r_load_question(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "assesment"
    ], False);

    try:

        assesment = Assesment.objects.get(id=data["assesment"]);
        questions = Questions \
            .objects \
            .filter(assesment=assesment.id) \
            .order_by("-id") \
            .values("id", "body", "user_id");

        questions = list(questions);

        for perQuestion in questions:
            choices = Quiz_choices \
                .objects \
                .filter(question=perQuestion["id"]) \
                .values("id", "body", "is_correct");

            choices = list(choices);

            perQuestion["choices"] = choices;

        json_value = json.dumps(questions);
        return HttpResponse(json_value);

    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def r_remove_question(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "id"
    ], False);

    try:

        question = Questions.objects.get(id=data["id"], user=request.user);
        question.delete();
        return HttpResponse(1);

    except Questions.DoesNotExist:

        messages.error(request, "The Question id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Question id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;

def load_assesments(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "activity_id"
    ], False);

    try:
        # Error if does not exists
        ofActivity = Activity.objects.get(id=data['activity_id']);
        of_assesment = Assesment\
            .objects\
            .filter(activity=data['activity_id'])\
            .order_by("-id")

        for per in of_assesment:
            per.no_questions = Questions\
                .objects\
                .filter(assesment_id=per.id)\
                .count();
            pass;



        return render(request, "testers_temp/teachers/activity/misc/"
                               "teacher_list_load_passage.html", {
            "assesment" : of_assesment,
            "itemCount" : len(of_assesment),
            "init": ""
        });

        returne = list(assesment);
        json_format = json.dumps(returne);

        return HttpResponse(json_format);

    except Activity.DoesNotExist:
        messages.error(request, "The Activity id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Activity id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

def remove_assesment(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "assesment_id"
    ], False);

    try:
        # Error if does not exists
        ofAssesment = Assesment.objects.get(id=data['assesment_id']);
        ofAssesment.delete();

        return HttpResponse("{}");

    except Assesment.DoesNotExist:
        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;

def r_load_activity(request):

    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'pageIndex',
        'pageSize',
        'group_level',
        'search'
    ], False);

    activities = Activity\
        .objects\
        .annotate(
            full_name=Concat(
                'user_added__first_name',
                V(' '),
                'user_added__last_name',
                output_field=TextField()
            )
        )\
        .filter(
            Q(group_level=data['group_level']),
            Q(title__icontains=data['search']) | Q(full_name__icontains=data['search'])
        )\
        .order_by("-id")\
        .values(
            "id",
            "title",
            "full_name",
            "description",
            "date_added",
            "user_added__id",
            "user_added__username",
            "user_added__first_name",
            "user_added__last_name",
            "user_added__email",
            "user_added__is_staff"
        );


    result = list(activities);

    p = Paginator(result, data['pageSize']);
    page = p.page(data['pageIndex'])

    json_format = json.dumps({
        "data": page.object_list,
        "length": len(result)
    }, default=helpers._JSON.json_serial);

    return HttpResponse(json_format);

    pass;


