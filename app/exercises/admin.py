from django.contrib import admin
from .models import Activity
from django.template.defaultfilters import truncatewords


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ('title', 'group_level', 'get_description', 'date_added','user_added')
    list_filter = ('group_level', "date_added" , "user_added");
    search_fields = (
        'title',
        'group_level',
        'user_added'
    );

    def get_description(self, obj):
        return truncatewords(obj.description, 10);

    get_description.short_description = "description"




# Register your models here.
