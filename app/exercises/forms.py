from django import forms;
from exercises.models import Assesment, Quiz_assesment;


class test: pass;

class AssesmentForm(forms.ModelForm):

    class Meta:
        model = Assesment;
        fields = [
            'title',
            'body',
            'audio',
            'type',
            'activity',
            'grade_level_of_complexity'
        ]

    pass;

class QuizAssesentForm(forms.ModelForm):
    class Meta:
        model = Quiz_assesment;
        fields = [
            'quiz_activity',
            "audio",
            'assesment',
            'read_duration',
            'no_play_record'
        ]
    pass;
