from django.urls import path;
from django.contrib import admin
from . import views


urlpatterns = [
    path("activity/add", views.r_new_activity, name="section"),
    path("activity/section/load", views.r_load_activity, name="section"),
    path("activity/modal_add", views.v_modal_activity, name="task"),
    path("activity", views.v_show_list_activity, name="task"),
    path("activity/oral", views.v_assesment, name="task"),
    path("activity/silent", views.v_assesment, name="task"),
    path("activity/listening", views.v_assesment, name="task"),
    path("activity/load_questions", views.r_load_question, name="task"),
    path("activity/add/section", views.save_section, name="task"),
    path("activity/update/section", views.update_section, name="task"),
    path("activity/questions", views.v_questions, name="task"),
    path("activity/questions/add", views.r_questions_add, name="task"),
    path("activity/questions/remove", views.r_remove_question, name="task"),
    path("activity/load", views.load_assesments, name="task"),
    path("activity/remove", views.remove_assesment, name="task"),
    # path("class/student/students_not_in_class", views.v_class_student, name="students")

]

