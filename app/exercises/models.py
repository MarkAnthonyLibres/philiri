from django.db import models;
# from app.students.models import Student, Batch;
from datetime import date;
from django.contrib.auth.models import User, AnonymousUser;
from group.models import Batch , Section, GroupLevel;
from class_sem.models import batch_student;
from django.core.exceptions import ValidationError
from students.models import Student;
from django.db.models import Sum, Q;
from modules import helpers;
from django.db.models import Count;
from group.models import GroupLevel;
import uuid;
import os;


class Activity(models.Model):
    ques_type = [
        (0, 'Literal'),
        (1, 'Inferential'),
        (2, 'Critical')
    ];

    of_test_type = [
        (0, 'Group Screening Test'),
        (1, 'Pre test'),
        (2, 'Post test')
    ];

    literacy_type = [
        (1, 'Oral Reading'),
        (2, 'Silent Reading Comprehension'),
        (3, 'Listening Comprehension')
    ];


    of_literacy_type = {
        "1" : "oral",
        "2" : "silent",
        "3" : "listening"
    }

    complexity_level = [
        (0, 'Easy'),
        (1, 'Medium'),
        (2, 'Hard')
    ];

    title_min_length = 5;
    description_length = 67;
    id = models.AutoField(primary_key=True);
    title = models.CharField(max_length=100, blank=False, null=False);
    test_type= models.IntegerField(blank=False, null=False, default=0, choices=of_test_type);
    type_of_literacy= models.IntegerField(blank=False, null=False, default=0, choices=literacy_type);
    level_of_complexity= models.IntegerField(blank=False, null=False, default=0, choices=complexity_level);
    group_level = models.ForeignKey(GroupLevel, on_delete=models.CASCADE, default=None, null=False, blank=False);
    description = models.TextField(max_length=400, blank=True, null=True);
    date_added = models.DateField(blank=False, null=False, default=date.today);
    user_added = models.ForeignKey(User, blank=False, null=False, on_delete=models.DO_NOTHING);

    def clean(self):

        # if len(str(self.description)) < self.description_length:
        #     raise ValidationError("Description min length should 67 above");

        pass;

    def __str__(self): return self.title;

    def comprehension_level_percent_all(self):

        of_quiz_activity = Quiz_activity\
            .objects\
            .filter(task__activity_id=self.id);

        sum = of_quiz_activity.aggregate(Sum('word_reading_percent'))['word_reading_percent__sum'];
        total_length = of_quiz_activity.count() * 100;

        if not total_length: return 0;

        total_by = int(sum or 0) / int(total_length or 0);
        return int(total_by * 100);

        pass;

    def questions_type_length(self):

        of_questions = Questions\
            .objects\
            .filter(assesment__activity_id=self.id)\
            .aggregate(
                literal=Count("type",filter=Q(type=0)),
                inferential=Count("type",filter=Q(type=1)),
                critical=Count("type",filter=Q(type=2)),
            );

        return of_questions;

        pass;

    def get_literacy_type(self):
        return self.of_literacy_type[str(self.type_of_literacy)];
        pass;


def audio_filename(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext);
    return os.path.join(instance.directory_string_var, filename);


class Assesment(models.Model):
    quiz_type = [
        (1, 'Oral'),
        (2, 'Silent'),
        (3, 'Listening')
    ];
    directory_string_var = "audio_base";
    id = models.AutoField(primary_key=True);
    title = models.TextField(max_length=60, blank=False, null=False);
    body = models.TextField(blank=False, null=False);
    audio = models.FileField(upload_to=audio_filename, help_text="Select Audio File", blank=True, null=True);
    type = models.IntegerField(blank=False, null=False, choices=quiz_type);
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, null=False, blank=False, default=None);
    grade_level_of_complexity = models.ForeignKey(GroupLevel, on_delete=models.CASCADE, null=False, blank=False, default=None);
    date_added = models.DateField(blank=False, null=False, default=date.today);


    def no_questions(self):
        return Questions.objects.filter(assesment_id=self.id).count();
        pass;



class Questions(models.Model):
    ques_type = [
        (0, 'Literal'),
        (1, 'Inferential'),
        (2, 'Critical')
    ];
    id = models.AutoField(primary_key=True);
    assesment = models.ForeignKey(Assesment, on_delete=models.CASCADE, default=None, null=False, blank=False)
    body = models.TextField(max_length=500);
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=False, blank=False);
    type = models.IntegerField(blank=False, null=False, choices=ques_type, default=0);


pass;


class Quiz_choices(models.Model):
    id = models.AutoField(primary_key=True);
    question = models.ForeignKey(Questions, on_delete=models.CASCADE, default=None, null=False, blank=False)
    body = models.TextField(max_length=500);
    is_correct = models.BooleanField(default=False, null=False, blank=False)


class Task(models.Model):
    id = models.AutoField(primary_key=True);
    section = models.ForeignKey(Section,on_delete=models.CASCADE, default=None, null=False, blank=False);
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None, null=False, blank=False);
    date_added = models.DateField(blank=False, null=False, default=date.today);
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE, default=None, null=False, blank=False);
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, default=None, null=False, blank=False);
    name = models.TextField(default="",null=True, blank=True);
    is_preTest = models.BooleanField(default=False);
    description = models.TextField(default="",null=True, blank=True);
    gst = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True);

    def clean(self):

        if self.activity :

            isExists = Task.objects.filter(
                section=self.section,
                batch=self.batch,
                activity=self.activity,
                is_preTest=False
            );

            if len(isExists):
                raise ValidationError("Activity id is already exists on target Section and Batch");

        pass;

    def of_comprehension_level_percent(self,of_batch_student):

        by_batch_student = of_batch_student\
            .objects\
            .filter(section=self.section,batch=self.batch);

        total_comprehension_score = by_batch_student.\
            aggregate(Sum('rate_percent'))['rate_percent__sum'] or 0;

        total_set = by_batch_student.count();

        if not total_set: return 0;

        total_by = int(total_comprehension_score ) / int(total_set);
        return int(total_by * 100);

        pass;

    def roman_grade_level(self):
        return helpers._String.int_to_roman(self.section.group.level);
        return

    def pretest_is_gst_set(self):
        if not self.is_preTest: return True;
        return Quiz_activity.objects.filter(task_id=self.gst_id,is_done=True).exists();
        pass;

    def no_sudents(self):

        return batch_student.objects.filter(
            batch_id=self.batch,
            section_id=self.section
        ).count();

        pass;

    def completed_students_num(self):
        return Quiz_activity.objects.filter(is_done=1,task_id=self.id).count();
        pass;

    def not_completed_students_num(self):
        of_quiz_activity =  Quiz_activity\
            .objects\
            .filter(is_done=1,task_id=self.id)\
            .values("batch_student__id");

        of_batch_student = batch_student\
            .objects\
            .filter(batch=self.batch,section=self.section)\
            .exclude(id__in=of_quiz_activity)\
            .count();

        return of_batch_student;

        pass;

    def completed_students_list(self):
        return Quiz_activity.objects.filter(is_done=1,task_id=self.id);
        pass;

    def not_completed_students_list(self):

        of_quiz_activity =  Quiz_activity\
            .objects\
            .filter(is_done=1,task_id=self.id)\
            .values("batch_student__id");

        of_batch_student = batch_student\
            .objects\
            .filter(batch=self.batch,section=self.section)\
            .exclude(id__in=of_quiz_activity);


        return of_batch_student;

        pass;

    def get_all_quiz_activity(self):

        return Quiz_activity\
            .objects\
            .filter(task_id=self.id,is_done=True)\
            .order_by("batch_student__student__first_name");

        pass;




class vw_task(models.Model):
    section = models.ForeignKey(Section,on_delete=models.DO_NOTHING, default=None, null=False, blank=False);
    user = models.ForeignKey(
        User,
        related_name="task_user_added",
        on_delete=models.DO_NOTHING,
        default=None,
        null=False,
        blank=False
    );
    date_added = models.DateField(blank=False, null=False, default=date.today);
    batch = models.ForeignKey(Batch, on_delete=models.DO_NOTHING, default=None, null=False, blank=False);
    activity = models.ForeignKey(Activity, on_delete=models.DO_NOTHING, default=None, null=False, blank=False);
    title = models.CharField(max_length=100, blank=False, null=False);
    description = models.TextField(max_length=400);
    exercise_date = models.DateField(blank=False, null=False, default=date.today);
    user_added = models.ForeignKey(
        User,
        related_name="activity_user_added",
        blank=False,
        null=False,
        on_delete=models.DO_NOTHING
    );
    group_level = models.ForeignKey(GroupLevel, on_delete=models.DO_NOTHING, default=None, null=False, blank=False);

    class Meta:
        managed = False
        db_table = "vw_task";


class Quiz_activity(models.Model):
    level = [
        (0, 'Frustration'),
        (1, 'Instructional'),
        (2, 'Independent')
    ];
    id = models.AutoField(primary_key=True);
    task = models.ForeignKey(Task,on_delete=models.CASCADE,
                null=False,
                blank=False,
                default=None);
    batch_student = models.ForeignKey(batch_student, on_delete=models.CASCADE, null=False, blank=False, default=None);
    date = models.DateField(blank=False, null=False, default=date.today);
    is_done = models.BooleanField(blank=False, null=False, default=False);
    total_correct_literal = models.IntegerField(blank=False, null=False, default=0);
    total_correct_inferential = models.IntegerField(blank=False, null=False, default=0);
    total_correct_critical = models.IntegerField(blank=False, null=False, default=0);
    total_score = models.IntegerField(blank=False, null=False, default=0);
    num_literal = models.IntegerField(blank=False, null=False, default=0);
    num_inferential = models.IntegerField(blank=False, null=False, default=0);
    num_critical = models.IntegerField(blank=False, null=False, default=0);
    word_reading_percent = models.IntegerField(blank=False, null=False, default=0);
    comprehension_level_percent = models.IntegerField(blank=False, null=False, default=0);
    word_reading_level = models.IntegerField(blank=False, null=False, default=0, choices=level);
    comprehension_level = models.IntegerField(blank=False, null=False, default=0, choices=level);
    question_set = models.IntegerField(blank=False, null=False, default=0);

    def clean(self):

        self.__check_duplicates__();

        pass;

    def get_word_reading_percent(self):

        of_oral_reading = Oral_reading\
            .objects\
            .filter(
                quiz_assesment__quiz_activity_id=self.id,
                quiz_assesment__quiz_activity__batch_student_id = self.batch_student
            );

        sum = of_oral_reading.aggregate(Sum('no_oral_reading_score'))['no_oral_reading_score__sum'] or 0;
        total_length = of_oral_reading.count() * 100;

        if not total_length: return 0;

        total_by = int(sum or 0) / int(total_length or 0);
        result = int(total_by * 100);

        self.word_reading_percent = result;
        self.save();

        return result;

        pass;

    def get_word_reading_level(self):

        percentage = self.get_word_reading_percent();

        if 97 <= percentage: #Frustration
            self.word_reading_level = 2;
        elif 96 >= percentage and 90 <= percentage: #Instructional
            self.word_reading_level = 1;
        else: #Frustration
            self.word_reading_level = 0;

        self.save();
        return self.word_reading_level;


        pass;

    def __check_duplicates__(self):

        is_exists = Quiz_activity\
            .objects\
            .filter(
                batch_student_id=self.batch_student,
                task_id=self.task
            ).exists();

        if is_exists:
            raise ValidationError("Quiz activity already exists");

        pass;

    def score(self):
        return Students_answer \
            .objects \
            .filter(
            quiz_assesment__quiz_activity_id=self.id,
            answer__is_correct=True
        ).count();

        pass;

    def total_correct_question_type(self, type: int):
        return Students_answer \
            .objects \
            .filter(
            quiz_assesment__quiz_activity_id=self.id,
            answer__is_correct=True,
            question__type=type
        ).count();

        pass;

    def total_question_type(self, type: int):
        return Questions \
            .objects \
            .filter(
            assesment__activity_id=self.task.activity_id,
            type=type
        ).count();

        pass;

    def comprehension_level_identifier(self,percentage : int = -1):

        if percentage < 0:
            percentage = self.comprehension_level_percent;

        if percentage >= 80:
            return 2;
        elif percentage >= 59 and percentage <= 79:
            return 1;

        return 0;

        pass;

    def of_comprehension_level_percent(self):

        total_comprehension_score = Quiz_assesment\
            .objects\
            .filter(quiz_activity_id=self.id)\
            .aggregate(Sum('comprehension_score'))['comprehension_score__sum'];

        total_question_set = Quiz_assesment\
            .objects\
            .filter(quiz_activity_id=self.id)\
            .aggregate(Sum('no_question_set'))["no_question_set__sum"];

        if not total_question_set: return 0;

        total_by = int(total_comprehension_score or 0) / int(total_question_set or 0);
        return int(total_by * 100);

        pass;

    def no_question_set(self):

        return Questions\
            .objects\
            .filter(assesment__activity_id=self.task.activity).count();

        pass;

    def student_duration_taken(self,is_format=False):

        of_quiz_assesment = Quiz_assesment.objects.filter(quiz_activity_id=self.id);
        sum = of_quiz_assesment.aggregate(Sum('read_duration'))['read_duration__sum'];

        return sum;

        pass;

    def oral_ungraded_passages(self):

        of_oral_reading = Oral_reading.objects.filter(
            quiz_assesment__quiz_activity_id=self.id,
            is_confirm=True
        );

        of_un_graded_assesment = Quiz_assesment.objects.filter(
            quiz_activity_id=self.id
        )\
            .exclude(id__in=of_oral_reading.values_list('quiz_assesment_id'))\
            .count();

        return of_un_graded_assesment;

        pass;

    def get_oral_list(self):

        return Oral_reading\
            .objects\
            .filter(
                quiz_assesment__quiz_activity_id=self.id,
                is_confirm=True
            ).order_by("quiz_assesment__assesment_id");

        pass;

    def get_assesment_list(self):

        return Quiz_assesment\
            .objects\
            .filter(
                quiz_activity_id=self.id
            ).order_by("id");

        pass;




    pass;


class Quiz_assesment(models.Model):
    compre_level = [
        (0, 'Frustration'),
        (1, 'Instructional'),
        (2, 'Independent')
    ];
    directory_string_var = "student_audio_base";
    id = models.AutoField(primary_key=True);
    quiz_activity = models.ForeignKey(Quiz_activity, on_delete=models.CASCADE, null=False, blank=False, default=None);
    assesment = models.ForeignKey(Assesment, on_delete=models.CASCADE, null=False, blank=False, default=None);
    audio = models.FileField(upload_to=audio_filename, help_text="Select Audio File", blank=True, null=True);
    read_duration = models.IntegerField(blank=False, null=False, default=0);
    no_play_record = models.IntegerField(blank=False, null=False, default=0);
    comprehension_score = models.IntegerField(blank=False, null=False, default=0);
    no_respond = models.IntegerField(blank=False, null=False, default=0);
    comprehension_level = models.IntegerField(choices=compre_level,blank=False, null=False, default=0);
    no_question_set = models.IntegerField(blank=False, null=False, default=0);
    reading_speed = models.IntegerField(blank=False, null=False, default=0);
    words_length = models.IntegerField(blank=False, null=False, default=0);


    def clean(self):

        self.words_length = self.words_length or self.get_no_of_words_in_passage();
        self.no_question_set = self.no_question_set or self.get_no_questions();

        isAlreadyExists = Quiz_assesment.objects.filter(
            quiz_activity_id=self.quiz_activity,
            assesment_id=self.assesment
        ).exists();

        if isAlreadyExists :
            raise helpers.AlreadyExist("Assesment is already been exists");

        pass;

    def get_percentage(self):

        compute = int(self.comprehension_score) / int(self.no_question_set);
        total = compute * 100;
        return round(total);

        pass;

    def get_questions(self):

        of_question = Questions\
            .objects\
            .filter(assesment_id=self.assesment)\
            .order_by("id");

        for per in of_question:

            try:

                per.answer = Students_answer\
                    .objects\
                    .get(question_id=per.id, quiz_assesment_id=self.id);

            except Students_answer.DoesNotExist:
                per.answer = None;
                pass;

            per.choices = Quiz_choices\
                .objects\
                .filter(question_id=per.id)\
                .order_by("id");

            identifier_list = "ABCDEFGHIJKLMNOPRSTUVWXYZ";
            per.answer_identifier = None;

            for index, per_choices in enumerate(per.choices):

                per_choices.tmp_identifier = identifier_list[index];

                if per.answer and per_choices.id == per.answer.answer_id:
                    per.answer_identifier = identifier_list[index];
                    pass;

                pass;

            pass;

        return of_question;

        pass;

    def get_no_of_words_in_passage(self):

        passages = str(self.assesment.title) + " " +  str(self.assesment.body);
        length_words = len(passages.split());

        return length_words;

        pass;

    def get_no_questions(self):

        return Questions \
                .objects \
                .filter(assesment_id=self.assesment_id) \
                .count();

        pass;



    pass;


class Students_answer(models.Model):
    id = models.AutoField(primary_key=True);
    quiz_assesment = models.ForeignKey(Quiz_assesment, on_delete=models.CASCADE, null=False, blank=False, default=None);
    question = models.ForeignKey(Questions, on_delete=models.CASCADE, null=False, blank=False, default=None);
    time_lapse = models.IntegerField(default=0, null=False, blank=False);
    answer = models.ForeignKey(Quiz_choices, on_delete=models.CASCADE, null=False, blank=False, default=None);

    def clean(self):

        check_exist = Students_answer.objects.filter(
            quiz_assesment=self.quiz_assesment.id,
            question=self.question.id
        ).exists();

        if check_exist:
            raise helpers.AlreadyExist("Answer is already been exists");


    pass;


class Oral_reading(models.Model):
    level = [
        (0, 'Frustration'),
        (1, 'Instructional'),
        (2, 'Independent')
    ];
    id = models.AutoField(primary_key=True);
    quiz_assesment = models.OneToOneField(Quiz_assesment, on_delete=models.CASCADE, null=False, blank=False);
    audio = models.FileField(upload_to=audio_filename, help_text="Select Audio File", blank=True, null=True,
                             default=0);
    text_base = models.TextField(blank=False, null=False,default="");
    title_base = models.TextField(blank=False, null=False,default="");
    no_mispronunciation = models.IntegerField(blank=True, null=True, default=0);
    no_omission = models.IntegerField(blank=True, null=True, default=0);
    no_substitution = models.IntegerField(blank=True, null=True, default=0);
    no_insertion = models.IntegerField(blank=True, null=True, default=0);
    no_repetition = models.IntegerField(blank=True, null=True, default=0);
    no_transposition = models.IntegerField(blank=True, null=True, default=0);
    no_reversal = models.IntegerField(blank=True, null=True, default=0);
    no_total_miscues = models.IntegerField(blank=True, null=True, default=0);
    no_oral_reading_score = models.IntegerField(blank=True, null=True, default=0);
    no_reading_level = models.IntegerField(blank=True, null=True, default=0, choices=level);
    is_confirm = models.BooleanField(blank=True, null=True, default=False);
    confirm_by = models.ForeignKey(User, blank=True, null=True, on_delete=models.DO_NOTHING);
    words_length = models.IntegerField(blank=True, null=True, default=0);

    of_miscue_type = [
        (0, 'Mispronunciation'),
        (1, 'Omission'),
        (2, 'Substitution'),
        (3, 'Insertion'),
        (4, 'Repetition'),
        (5, 'Transposition'),
        (6, 'Reversal'),
    ];

    def before_save_fill_params(self):

        self.no_mispronunciation = self.get_num_of_miscues(0);
        self.no_omission = self.get_num_of_miscues(1);
        self.no_substitution = self.get_num_of_miscues(2);
        self.no_insertion = self.get_num_of_miscues(3);
        self.no_repetition = self.get_num_of_miscues(4);
        self.no_transposition = self.get_num_of_miscues(5);
        self.no_reversal = self.get_num_of_miscues(6);
        self.no_total_miscues = self.get_total_miscues();
        self.words_length = self.get_no_of_words_in_passage();
        self.no_oral_reading_score = self.get_score_percentage();
        self.no_reading_level = self.get_no_reading_level();

        pass;


    def mispronunciation(self):
        return self.__get_miscue_words__(0);
        pass;

    def omission(self):
        return self.__get_miscue_words__(1);
        pass;

    def substitution(self):
        return self.__get_miscue_words__(2);
        pass;

    def insertion(self):
        return self.__get_miscue_words__(3);
        pass;

    def repetition(self):
        return self.__get_miscue_words__(4);
        pass;

    def transposition(self):
        return self.__get_miscue_words__(5);
        pass;

    def reversal(self):
        return self.__get_miscue_words__(6);
        pass;

    def get_list_of_miscues(self, miscue_type = 0):

        x =  Word_recognition.objects.filter(
            oral_reading=self.id,
            miscue_type=miscue_type
        ).order_by("-id");
        return x;

        pass;
    def get_num_of_miscues(self, miscue_type = 0):

        return Word_recognition.objects.filter(
            oral_reading=self.id,
            miscue_type=miscue_type
        ).count();

        pass;

    def get_total_miscues(self):

        return Word_recognition.objects.filter(
            oral_reading=self.id
        ).count();

        pass;

    def get_no_of_words_in_passage(self):

        passages = str(self.title_base) + " " +  str(self.text_base);
        length_words = len(passages.split());

        return length_words;

        pass;

    def get_score_percentage(self):

        total_miscues = self.get_total_miscues();
        words_length = self.get_no_of_words_in_passage();

        words_length = words_length or 0;
        total_miscues = total_miscues or 0;

        result = ((words_length - total_miscues) / words_length) * 100;

        return round(result);

        pass;

    def get_no_reading_level(self):

        result = self.get_score_percentage() or 0;

        if result >= 97:
            return 2;
        elif result >= 90 and result <= 96:
            return 1;

        return 0;

        pass;

    def __get_miscue_words__(self, miscue_type = 0):

        return Word_recognition.objects.filter(
            oral_reading=self.id,
            miscue_type=miscue_type
        );

        pass;


class Word_recognition(models.Model):
    of_miscue_type = [
        (0, 'Mispronunciation'),
        (1, 'Omission'),
        (2, 'Substitution'),
        (3, 'Insertion'),
        (4, 'Repetition'),
        (5, 'Transposition'),
        (6, 'Reversal'),
    ];
    id = models.AutoField(primary_key=True);
    oral_reading = models.ForeignKey(Oral_reading, on_delete=models.CASCADE, null=False, blank=False, default=None);
    miscue_type = models.IntegerField(blank=False, null=False, default=0, choices=of_miscue_type);
    index = models.IntegerField(blank=False, null=False);
    miscue_text = models.TextField(blank=False, null=False);
    miscue_recognize = models.TextField(blank=True, null=True);

    def clean(self):

        if self.is_exists():
             raise ValidationError("The word is already exists");

        pass;

    def is_exists(self):

        is_exist = Word_recognition.objects.filter(
            index=self.index,
            oral_reading=self.oral_reading,
            miscue_type=self.miscue_type
        ).exists();

        return is_exist;

        pass;

    pass;


