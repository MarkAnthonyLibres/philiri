from django.db import models
from datetime import date
from modules import helpers
from django.contrib.auth.models import User, AnonymousUser
from django.core.exceptions import ValidationError
from django.db.models import Sum;
# from class_sem.models import batch_student
# Create your models here.



today = date.today();

class GroupLevel(models.Model):
    id = models.AutoField(primary_key=True);
    name = models.CharField(max_length=22, null=False, blank=False);
    level = models.IntegerField(blank=True, default=0, null=True)
    date_created = models.DateField(default=date.today, editable=False);
    description = models.TextField(blank=True);
    school = models.ForeignKey(User,on_delete=models.CASCADE, default=None,blank=False,null=False);

    def __str__(self): return str(self.name).upper()

    @staticmethod
    def of_comprehension_level_percent(id, of_batch_student):

        by_batch_student = of_batch_student\
            .objects\
            .filter(section__group_id=id);

        total_comprehension_score = by_batch_student.\
            aggregate(Sum('rate_percent'))['rate_percent__sum'] or 0;

        total_set = by_batch_student.count();

        if not total_set: return 0;

        total_by = int(total_comprehension_score ) / int(total_set);
        return int(total_by * 100);

        pass;


class Section(models.Model):
    STATE_ACTIVE = 1;
    STATE_NOTACTIVE = 0;
    STATE_CHOICES = (
        (STATE_ACTIVE, "Active"),
        (STATE_NOTACTIVE, "Not Active"),
    );
    groups = GroupLevel.objects.all().values_list("id", "name");
    id = models.AutoField(primary_key=True);
    group = models.ForeignKey(GroupLevel, on_delete=models.CASCADE);
    name = models.CharField(max_length=22, null=False, blank=False);
    date_created = models.DateField(default=date.today, editable=False);
    description = models.TextField(blank=True);
    state = models.BooleanField(null=False, blank=False, choices=STATE_CHOICES, default=STATE_NOTACTIVE)

    @staticmethod
    def search(search, start=0, end=8, removeEmpty=False, **kwargs):
        sql_str = '''Select *
                            from group_section 
                            where
                            {where}
                            (`name` like "{search}" OR 
                            `description` like "{search}")
                             LIMIT {start}, {end}
                          ''';

        if removeEmpty: kwargs = helpers._ofDict.removeEmpty(kwargs);

        params = {
            "start": start,
            "end": end,
            "search": "%%" + search + "%%",
            "where": helpers._ofString.sql_where(kwargs) + " and " \
                if len(kwargs.keys()) else ""
        };

        sql_str = sql_str.format(**params);

        query = Section.objects.raw(sql_str);
        result = helpers._RawQuerySet.toList(query);

        return result;

        pass;

    def __str__(self): return str(self.name).upper()

    @staticmethod
    def of_comprehension_level_percent(section_id, of_batch_student):

        by_batch_student = of_batch_student\
            .objects\
            .filter(section_id=section_id);

        total_comprehension_score = by_batch_student.\
            aggregate(Sum('rate_percent'))['rate_percent__sum'] or 0;

        total_set = by_batch_student.count();

        if not total_set: return 0;

        total_by = int(total_comprehension_score ) / int(total_set);
        return int(total_by * 100);

        pass;


class Batch(models.Model):
    id = models.AutoField(primary_key=True);
    start_date = models.DateField(null=False, blank=False, default=date.today);
    end_date = models.DateField(null=False, blank=False, default=date.today);
    description = models.TextField(blank=True);
    name = models.CharField(max_length=22, null=False, blank=False, default=None);
    date_created = models.DateField(verbose_name='Date created', default=date.today, editable=False);
    school = models.ForeignKey(User,on_delete=models.CASCADE, default=None,blank=False,null=False);

    def __str__(self):
        return "{} - {}".format(self.start_date, self.end_date);

    @staticmethod
    def start_date_precesion(error="Start date is between two other dates"):

        def validate(value):
            if Batch.check_if_date_has_precesion(value):
                raise ValidationError(error)

        pass;

        return validate;

    def clean(self):

        start_date_precesion = Batch.check_if_date_has_precesion(self.start_date);
        end_date_precesion = Batch.check_if_date_has_precesion(self.end_date);
        checkInPrecesion = Batch.check_if_two_date_isIn_precesion(self.start_date,self.end_date);

        if start_date_precesion:
            raise ValidationError("Start date is between two other dates");

        if end_date_precesion:
            raise ValidationError("End date is between two other dates");

        if checkInPrecesion :
            raise ValidationError("Start date and End date is in between or has precesion on two other dates");

        if self.start_date > self.end_date:
            raise ValidationError("Invalid set, The difference between start date and end date is invalid");

        pass;


    @staticmethod
    def check_if_two_date_isIn_precesion(before,after):

        sql = '''SELECT * 
        FROM philiri.group_batch where Date("{before}") < start_date and Date("{after}") >    end_date
        ''';

        sql = sql.format(before=before,after=after);
        query = Batch.objects.raw(sql);
        result = helpers._RawQuerySet.toList(query);

        return len(result) > 0;

        pass;

    @staticmethod
    def check_if_date_has_precesion(date):

        sql = '''SELECT * FROM philiri.group_batch where Date("{date}") BETWEEN start_date and end_date;''';
        sql = sql.format(date=date);
        query = Batch.objects.raw(sql);
        result = helpers._RawQuerySet.toList(query);

        return len(result) > 0;

        pass;

    @staticmethod
    def search(search, start=0, end=8, removeEmpty=False, **kwargs):
        sql_str = '''Select *
                                from group_batch 
                                where
                                {where}
                                (`name` like "{search}" OR 
                                `description` like "{search}" OR 
                                `start_date` like "{search}" OR 
                                `end_date` like "{search}"
                                )
                                ORDER BY date_created DESC
                                 LIMIT {start}, {end} 
                              ''';

        if removeEmpty: kwargs = helpers._ofDict.removeEmpty(kwargs);

        params = {
            "start": start,
            "end": end,
            "search": "%%" + search + "%%",
            "where": helpers._ofString.sql_where(kwargs) + " and " \
                if len(kwargs.keys()) else ""
        };

        sql_str = sql_str.format(**params);

        query = Batch.objects.raw(sql_str);
        result = helpers._RawQuerySet.toList(query);

        return result;

        pass;

    @staticmethod
    def of_comprehension_level_percent(id, of_batch_student):

        by_batch_student = of_batch_student\
            .objects\
            .filter(batch_id=id);

        total_comprehension_score = by_batch_student.\
            aggregate(Sum('rate_percent'))['rate_percent__sum'] or 0;

        total_set = by_batch_student.count();

        if not total_set: return 0;

        total_by = int(total_comprehension_score ) / int(total_set);
        return int(total_by * 100);

        pass;

    pass;

