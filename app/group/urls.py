from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = [
    path("groups", views.index, name="group"),
    path("groups/info",views.v_group_info,name="section"),
    path("groups/section/list", views.r_section_list, name="section"),
    path("groups/section/info", views.v_section_info, name="section"),
    path("group/section/student/add", views.v_student_add, name="section"),
    path("group/section/batch/list", views.r_batch_list, name="section"),
    path("group/section", views.v_section, name="section")
]