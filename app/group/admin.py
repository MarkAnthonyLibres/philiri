from django.contrib import admin
from .models import GroupLevel,Section,Batch

# Register your models here.

@admin.register(Batch)
class BatchAdmin(admin.ModelAdmin):
    list_display = ('start_date','end_date', 'description', 'date_created')
    list_filter = ('start_date','end_date', 'date_created');
    search_fields = (
        'start_date',
        'end_date',
        'description'
    )

@admin.register(Section)
class SectionsAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'date_created', 'group','state')
    list_filter = ('date_created', 'state');
    search_fields = (
        'name',
        'description'
    )




class GroupLevelAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'date_created','level')
    list_filter = ('date_created', 'level');
    search_fields = (
        'name',
        'description',
        'date_created',
)


admin.site.register(GroupLevel,GroupLevelAdmin);
