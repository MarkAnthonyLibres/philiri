from django import template
from ..models import GroupLevel
from modules.helpers import _String
register = template.Library()

@register.simple_tag
def get_groups():
    x = GroupLevel\
        .objects\
        .order_by('level').values(
        "id",
        "name",
        "date_created",
        "description",
        "level"
    );

    return list(x);
    pass;


@register.filter
def index(indexable, i):
   return indexable[i];

@register.filter
def length(value):
   return len(value);

@register.filter
def roman(value):
   return _String.int_to_roman(value);


@register.filter
def less_zeros(value, max = 4):
   length = len(str(value));
   less = max - length;
   list_value = [0] * less;
   return "".join([str(i) for i in list_value]);


@register.filter
def default(value, default_value):
   return value if value else default_value


@register.filter
def json(value, default_value):
   import json;
   return json.dumps(value);


@register.filter
def set_by_index(value):
    sets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return sets[int(value)];
    pass;


@register.filter
def devision(value, value1):
    value = int(value);
    value1 = int(value1);
    compute = value1 / value;
    return round(compute);
    pass;


@register.filter
def seconds_to_hours(value):
    value = int(value);

    if value < 3600:
        raise ValueError("Invalid value");

    return round(value / 3600);

