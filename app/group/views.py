from django.shortcuts import render, redirect, HttpResponse
from .models import Section, GroupLevel, Batch
from modules import helpers
from django.forms.models import model_to_dict
from exercises.models import Activity
import json

# Create your views here.

def index(request):

    return render(request,"include/groups.html",{
        'title': "GROUPS",
        "init": ""
    });

    pass;

def v_group_info(request):
    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "index"
    ], False);

    group_info = GroupLevel \
        .objects \
        .filter(id=data['index']) \
        .values()[0];

    return render(request,"include/group_sections.html",{
        **{'title': "SECTIONS","init": ""},
        **data,
        **group_info
    })

    pass;

def r_section_list(request):

    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "index",
        "pageIndex",
        "pageSize",
        "search",
        "state"
    ], False);

    offset = int(data['pageSize']);
    index = int(data["pageIndex"]);

    startIndex = (index - 1 if index > 0 else 0) * offset;
    endIndex = startIndex + offset;

    search = Section.search(
        search=data["search"],
        start=startIndex,
        end=endIndex,
        removeEmpty=True,
        group_id=data['index'],
        state=data['state']
    );

    partial_search = {
        "name": data['search'],
        "group_id": data['index'],
        "state": data['state']
    }

    partial_search = helpers._ofDict.removeEmpty(partial_search);

    json_format = json.dumps({
        "data": list(search),
        "length": Section.objects.filter(**partial_search).count()
    },default=helpers._JSON.json_serial);

    return HttpResponse(json_format);

    pass;

def v_section_info(request):
    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "index"
    ], False);

    try:
        x = Section.objects.get(id=data['index'])
        x.clean();

        _model = helpers._Model(x);
        info = _model.getValue();

        return render(request, "include/section_info.html", {
            **{'title': "SECTIONS", "init": "", "group_id" : x.group_id },
            **info,
            **data
        })
    except Section.DoesNotExist:
        raise Exception("Not Found");
        pass;

    pass;

def v_student_add(request):

    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "section"
    ], False);


    try:
        x = Section.objects.get(id=data['section'])
        x.clean();

        _model = helpers._Model(x);
        info = _model.getValue();

        return render(request, "include/section_add_student.html", {
            **{'title': "Section", "init": "", "group_id" : x.group_id },
            **info,
            **data
        })
    except Section.DoesNotExist:
        raise Exception("Not Found");
        pass;

    pass;

    pass;

def r_batch_list(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'search',
        "index",
        "pageIndex",
        "pageSize",
    ], False);

    offset = int(data['pageSize']);
    index = int(data["pageIndex"]);

    startIndex = (index - 1 if index > 0 else 0) * offset;
    endIndex = startIndex + offset;

    search = Batch.search(
        search=data["search"],
        start=startIndex,
        end=endIndex,
        removeEmpty=True
    );

    partial_search = {"name": data['search']}

    partial_search = helpers._ofDict.removeEmpty(partial_search);

    json_format = json.dumps({
        "data": list(search),
        "length": Batch.objects.filter(**partial_search).count()
    }, default=helpers._JSON.json_serial);

    return HttpResponse(json_format);

    pass;

def v_section(request):

    # return render(request, "sections/sections.html", {
    #     'title': "SECTIONS",
    #     "init": ""
    # });

    pass;

