from django.db import models
from django.contrib.auth.models import User;
import random, string
from django.core.exceptions import ValidationError
from datetime import datetime, date
# Create your models here.


class UserProfile(models.Model):
    school_type = [
        (0, ''),
        (1, 'Private'),
        (2, 'Public')
    ];
    id = models.AutoField(primary_key=True);
    user = models.OneToOneField(User,related_name="Profile", unique=True, on_delete=models.CASCADE,default=None,
                                blank=False);
    name_alias = models.CharField(max_length=50,default="",blank=True,null=True);
    school = models.ForeignKey(User,related_name="Institution",on_delete=models.CASCADE, default=None,blank=False,null=False);
    birthdate = models.DateField(blank=True,null=True,default=date.today);
    access_code = models.CharField(max_length=20,default="",blank=True,null=True);
    street = models.CharField(max_length=20,default="",blank=True,null=True);
    city = models.IntegerField(blank=True,null=True);
    province = models.IntegerField(blank=True,null=True);
    type = models.IntegerField(default=0, choices=school_type,blank=True,null=True);
    isSchool = models.BooleanField(default=True);

    def clean(self):

        if not self.access_code:
            self.access_code = UserProfile.generate_unique_access_code();
            pass;

        if self.isSchool:
            isExists = UserProfile\
                .objects\
                .filter(access_code=self.access_code)\
                .exists();

            if isExists:
                raise ValidationError("Access code is not availlable");
                pass;

            pass;

        pass;


    @staticmethod
    def generate_unique_access_code(length=6):

        random_str = ''.join(random.choices(string.ascii_letters + string.digits, k=length));
        random_str = str(random_str).upper();

        is_exists = UserProfile.check_access_code_is_exists(random_str);

        if is_exists:
            return UserProfile.generate_unique_access_code(length+1);

        return random_str;

        pass;


    @staticmethod
    def check_access_code_is_exists(access_code : str):

        return UserProfile\
            .objects\
            .filter(access_code=access_code)\
            .exists();

        pass;

    def get_age(self):

        birth_year = self.birthdate.strftime("%Y");
        year_now = datetime.now().strftime("%Y");
        return int(year_now) - int(birth_year);

        pass;


    def get_school_user_profile(self):

        return UserProfile.objects.get(user_id=self.school_id, isSchool=True);

        pass;


