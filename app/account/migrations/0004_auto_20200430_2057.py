# Generated by Django 3.0 on 2020-04-30 12:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20200430_1025'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='user',
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='city',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='province',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='street',
            field=models.CharField(max_length=20),
        ),
    ]
