import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
import django;
from django.contrib.auth.models import Group
from account.models import UserProfile;
from account.views import Category;

# Create your views here.

def is_member(user):
    return user.groups.filter(id=Category.id_teacher).exists();
    pass;


def is_personal_set(user):
    try:
        user_profile = UserProfile.objects.get(user=user.id);
        if not user_profile.access_code: return False;
        return True;
    except UserProfile.DoesNotExist:
        return False;
        pass;
    pass;


def is_not_personal_set(user):
    return not is_personal_set(user);
    pass;

def has_user_profile(user : User):
    return UserProfile.objects.filter(user=user).exists();
    pass;

def has_no_user_profile(user : User):
    return not UserProfile.objects.filter(user=user).exists();
    pass;


