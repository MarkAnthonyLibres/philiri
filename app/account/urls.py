
from . import views
from django.urls import path

urlpatterns = [
    path("account/new", views.v_create_account, name="Account"),
    path("account/check_username", views.is_username_exists, name="Account"),
    path("account/register", views.r_register, name="Account")
]
