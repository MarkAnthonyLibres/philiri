import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
import django;
from django.contrib.auth.models import Group
from account.models import UserProfile;


# Create your views here.

class Category:

    School = "School";
    id_school = Group.objects.get(name=School).id;
    Teacher = "Teacher";
    id_teacher = Group.objects.get(name=Teacher).id;
    Students = "Student";
    id_student = Group.objects.get(name=Students).id;

def is_member(user):
    return user.groups.filter(name="School").exists();
    pass;


def is_personal_set(user):
    try:
        user_profile = UserProfile.objects.get(user=user.id);

        if not str(user_profile.name_alias): return False;
        if user_profile.type <= 0: return False;
        if not user.email: return False;
        return True;

    except UserProfile.DoesNotExist:
        return False;
        pass;
    pass;


def is_not_personal_set(user):
    return not is_personal_set(user);
    pass;

def has_user_profile(user : User):
    return UserProfile.objects.filter(user=user).exists();
    pass;


def is_username_exists(request):
    if not User.is_authenticated: return;
    if len(request.GET) > 0:

        constraint = helpers.constraint(request, "GET");

        data = constraint.strict([
            "username",
        ], False);

        check: bool = User \
            .objects \
            .filter(username=data['username']) \
            .exists();

        if check: raise Exception("Already exist");

    return HttpResponse("Not Exist");


def v_create_account(request):
    if request.user.is_authenticated:

        if request.user.is_superuser:
            return redirect("/admin");
        else:
            return redirect("/home");

    list_of_group = Group.objects.all().values('id', 'name');

    return render(request, "testers_temp/authentication/create_account.html", {
        'title': "HOME",
        "auth_group": list_of_group,
        "button_type": ['btn-primary', 'btn-google', 'btn-facebook'],
        "init": ""
    });

    pass;


def r_register(request):
    constraint = helpers.constraint(request, 'POST');
    data = constraint.strict([
        "first_name",
        "last_name",
        "username",
        "password",
        "group_type"
    ], True);

    user = User.objects.create_user(
        username=data['username'],
        password=data['password'],
        first_name=data['first_name'],
        last_name=data['last_name'],
        is_staff=True
    );

    try:
        my_group = Group.objects.get(id=data['group_type']);
        my_group.user_set.add(user);

        return redirect("/home");

    except Group.DoesNotExist:

        user.delete();

        messages.error(request, "Group does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "the specified Group does not exist and invalid"
        });

        pass;

    pass;


