from django.urls import path;
from django.contrib import admin
from . import views

urlpatterns = [
    path("", views.login, name="login"),
    path("logout/", views.logout, name="login"),
    path("login/request", views.request, name="loginRequest"),
    path("login/register", views.register, name="register"),
    path("login/registerRequest", views.registerRequest, name="register")
]
