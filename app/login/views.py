import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
import django;


# from ..modules.helper.helpers import constraint;


# Create your views here.


def login(request):

    if request.user.is_authenticated :

        if request.user.is_superuser: return redirect("/admin");
        else: return redirect("/home");

    return render(request, "testers_temp/authentication/login.html", {
        'title': "HOME",
        "init": ""
    });


def logout(request):

    if not request.user.is_authenticated:
        raise AttributeError("Invalid request");

    django.contrib.auth.logout(request);

    return redirect("/");


def register(request):

    return render(request, "include/register.html", {
        'title': "HOME",
        "init": ""
    });


def registerRequest(request):

    if User.is_authenticated:

        raise AttributeError("Invalid request");

    if len(request.POST) > 0:
        constraint = helpers.constraint(request);

        data = constraint.strict([
            "username",
            "password",
            "first_name",
            "last_name",
            "email",
            "c_password"
        ], True);

        confirm_password = data['c_password'];
        del data['c_password'];

        if data['password'] != confirm_password:
            raise AttributeError("Password not match");

        if User.objects.filter(username=data['username']).exists():
            raise AttributeError("Username already taken");

        if User.objects.filter(email=data['email']).exists():
            raise AttributeError("Email already taken");

        user = User.objects.create_user(**data);
        user.save();

        return HttpResponse("User is created");

    return register(request);


pass;


def request(request):

    if len(request.POST) > 0:

        # print(request.__class__.__name__);

        constraint = helpers.constraint(request);
        data = constraint.strict(["account", "password"], True);

        user = auth.authenticate(
            username=data['account'],
            password=data['password']
        );

        if user and not user.is_superuser:
            auth.login(request, user);
            return redirect("/home");

        str_message = """
        Please enter the correct username and password for a Teacher/Student/School
        account. Note that both fields may be case-sensitive.
        """;

        messages.error(request, str_message);

    return login(request);
