from django.urls import path;
from django.contrib import admin
from . import views
from . import v_group

urlpatterns = [
    path("student/home", views.index, name="students"),
    path("student/home/personal", views.v_personal_info, name="students"),
    path("student/home/personal/set", views.required_personal_info_set, name="home"),


    path("students", views.v_students, name="students"),
    path("students/new", views.v_new, name="students"),
    path("students/register", views.r_register, name="students"),
    path("students/key-check", views.is_key_exists, name="students"),
    path("students/student_list", views.r_student_list, name="students"),
    path("student/welcome", views.v_student_login, name="students"),
    path("student/login", views.v_student_login, name="students"),
    path("student/login/auth", views.r_student_login, name="students"),
    path("student/logout", views.r_logout, name="students"),
    path("student/load/activity", views.r_load_student_activity, name="students"),
    path("student/activity/test", views.v_activity_available_test, name="students"),
    path("student/activity/start", views.v_activity_start, name="students"),
    path("student/activity/load", views.v_activity_on_start, name="students"),
    path("student/activity/get_question", views.r_activity_get_question, name="students"),
    path("student/activity/answered", views.v_activity_answered, name="students"),
    path("student/activity/score", views.v_activity_score, name="students"),
    path("student/activity/result", views.r_activity_result, name="students"),
    path("student/activity/level_question/result", views.r_activity_level_question_result, name="students"),
    path("student/groups", views.v_students_groups, name="students"),
    path("student/group/activity", v_group.v_activity, name="students"),

]
