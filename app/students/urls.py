from django.urls import path;
from django.contrib import admin
from . import views
from . import v_activity
from . import views_backup
from . import v_classroom

urlpatterns = [
    path("student/home", views.index, name="students"),
    path("student/home/personal", views.v_personal_info, name="students"),
    path("student/home/personal/set", views.required_personal_info_set, name="home"),
    path("student/load/activity",v_activity.r_load_student_activity, name="students"),
    path("student/activity/test", v_activity.v_test_type, name="students"),
    path("student/activity/start", v_activity.v_start, name="students"),
    path("student/activity/load", v_activity.v_ready_load, name="students"),
    path("student/activity/get_question", v_activity.r_activity_get_question, name="students"),
    path("student/activity/answered", v_activity.v_activity_answered, name="students"),
    path("student/activity/score", v_activity.v_activity_score, name="stud_activity_score.html"),

    # Post request
    path("student/activity/oral", v_activity.oral_result, name="stud_group_activity.html"),

    path("student/activity/result", v_activity.r_activity_result, name="students"),
    path("student/activity/level_question/result", v_activity.r_activity_level_question_result, name="students"),
    path("student/groups", v_classroom.index, name="students"),
    path("student/group/activity", v_classroom.v_activity, name="stud_group_activity.html"),


    path("students/student_list", views_backup.r_student_list, name="students"),



]
