from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from modules import helpers
from .models import Student
from django.contrib import messages;
from django.http import JsonResponse
from django.http.response import HttpResponse
from django.db.models import Q
from class_sem.models import batch_student;
from datetime import date, datetime, timezone;
from . import views;
from exercises.models import Task
from exercises.models import Quiz_activity;
from django.db.models import Sum;
from django.contrib.auth.decorators import login_required, user_passes_test;

from account.v_student import \
    has_user_profile, \
    is_not_personal_set, \
    is_personal_set, \
    is_member, \
    has_no_user_profile;



def index(request):
    Quiz_activity.objects.all().delete();
    school_id = request.session['school_id'];

    of_batch_student = batch_student\
        .objects\
        .filter(
            student_id=request.user,
            batch__school_id=school_id
        )\
        .order_by("section__group__level")

    of_batch_student = of_batch_student.values(
        "id",
        "batch_id",
        "batch__name",
        "section__group_id",
        "section__group__name",
        "section_id",
        "section__name",
        "rate_percent"
    );

    for per in of_batch_student:

        per['task_length'] = Task.objects.filter(
            batch_id=per['batch_id'],
            section_id = per['section_id']
        ).count();

        pass;

    return render(request, "testers_temp/student/classroom/stud_groups.html", {
        'title': "CLASSROOM",
        "init": "",
        "user": request.user,
        "group_data" : of_batch_student
    });


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_activity(request):

    try:

        get_request = helpers.constraint(request, "GET");
        data = get_request.strict([
            'batch_id',
        ], False);

        classroom = batch_student.objects.get(
            id=data['batch_id'],
            student=request.user
        );


        activitys = Task \
            .objects \
            .filter(
                batch_id=classroom.batch_id,
                section_id=classroom.section_id) \
            .order_by("-date_added");

        for per_activitys in activitys:

            of_quiz_activity = Quiz_activity \
                .objects \
                .filter(
                    batch_student_id=classroom.id,
                    task__activity_id=per_activitys.activity_id,
                    is_done=True
            );

            total_comprehension_score = of_quiz_activity.aggregate(
                Sum('comprehension_level_percent')
            )['comprehension_level_percent__sum'];

            total_length = of_quiz_activity.count();

            if total_length > 0:
                per_activitys.rates = int((total_comprehension_score or 0) / (total_length * 100) * 100);
            else:
                per_activitys.rates = 0;

            per_activitys.has_graded = Quiz_activity \
                .objects \
                .filter(batch_student_id=classroom.id,task__activity_id=per_activitys.activity_id)\
                .exists();

        return render(request, "testers_temp/student/classroom/stud_group_activity.html", {
            'title': "CLASSROOM",
            "init": "",
            "section_name": classroom.section.name,
            "section_id": classroom.section_id,
            "group_name": classroom.section.group.name,
            "group_id": classroom.section.group_id,
            "batch_name": classroom.batch.name,
            "batch_id": classroom.batch_id,
            "activitys": activitys,
            "user": request.user,
            "school" : request.session['school_profile']
        });

    except helpers.InvalidRequest:

        messages.error(request, "Invalid Parameters");
        return redirect("/student/home");

    except batch_student.DoesNotExist:

        messages.error(request, "Could not find the classroom");
        return redirect("/student/login");

        pass;

    pass;


