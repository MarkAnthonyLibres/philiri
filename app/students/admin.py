from django.contrib import admin
from .models import Student

# Register your models here.

class StudentAdmin(admin.ModelAdmin):
    list_display = ('firstname', 'lastname', 'middlename', 'key')
    search_fields = (
        'firstname',
        'lastname',
        'middlename',
        "key"
)


admin.site.register(Student,StudentAdmin);