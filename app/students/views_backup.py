from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth
from modules import helpers
from .models import Student
from django.core.exceptions import ValidationError
from django.contrib import messages;
from django.http import JsonResponse
from django.http.response import HttpResponse
from django.db.models import Q
from datetime import date, datetime, timezone;
from class_sem.models import batch_student;
from exercises.models import Task, vw_task
from exercises.models import Quiz_activity, \
    Activity, \
    Quiz_assesment, \
    Assesment, \
    Questions, \
    Students_answer, \
    Quiz_choices

from students.models import Student;
from class_sem.models import batch_student
import json, simplejson
import ast
import random;
import time;
from django.contrib import messages;
from django.db.models import F;
from account.v_student import \
    has_user_profile, \
    is_not_personal_set , \
    is_personal_set, \
    is_member,\
    has_no_user_profile;

# Create your views here.
from account.models import UserProfile;
from django.contrib.auth.decorators import login_required, user_passes_test;
from django.shortcuts import HttpResponse, HttpResponseRedirect

@login_required(login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/student/home/personal")
@user_passes_test(test_func=is_member, login_url="/")
def index(request):

    return render(request, "testers_temp/student/stud_home.html", {
        'title': "HOME",
        "init": "",
        "user": request.user
    });

    pass;



@login_required(login_url="/")
@user_passes_test(test_func=is_not_personal_set, login_url="/student/home")
@user_passes_test(test_func=is_member, login_url="/")
def v_personal_info(request):

    return render(request, "testers_temp/student/home/student_required.html", {
        'title': "INFO",
        "user": request.user,
        "init": ""
    });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_not_personal_set, login_url="/home")
def required_personal_info_set(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "access"
    ], True);

    try:

        school = UserProfile.objects.get(access_code=data['access'],isSchool=True);

        isExists = UserProfile.objects.filter(user=request.user).exists();

        if isExists:
            raise helpers.AlreadyExist("Invalid, Profile is already set");

        by_user_profile = UserProfile(
            user=request.user,
            access_code=data['access'],
            school=school.user,
            isSchool=False
        );

        by_user_profile.full_clean();
        by_user_profile.clean();
        by_user_profile.save();

    except UserProfile.DoesNotExist:

        return render(request, "testers_temp/student/home/student_required.html", {
            'title': "INFO",
            "user": request.user,
            "validation_error" : "Invalid Request. code is does'nt exist or you have no permission to join this "
                                 "institution",
            "init": ""
        });

        pass;

    return redirect("/home");




"""---------------------------------"""


def s_is_login(request):
    if request.user.is_authenticated:

        if 'login_student' in request.session: del request.session['login_student'];

        messages.error(request, "You are authenticated as admin, but are not authorized to access this page");
        return render(request, "error.html", {
            "status": "404",
            "message": "you are authenticated as admin, but are not authorized to access this page"
        });

    stud_login = helpers.constraint.check_student_login(request);

    return stud_login;

    pass;


def v_students(request):
    if not request.user.is_authenticated:
        return redirect("/");

    return render(request, "include/students.html", {
        'title': "STUDENTS",
        "init": ""
    });

    pass;


def v_new(request, post=dict()):
    if not request.user.is_authenticated:
        return redirect("/");

    params = {
        'title': "STUDENTS",
        "init": ""
    };

    params.update(post);

    return render(request, "include/addNewStudent.html", params);

    pass;


def r_register(request):
    if not User.is_authenticated: redirect("/");

    if len(request.POST) > 0:

        constraint = helpers.constraint(request);

        data = constraint.strict([
            "lastname",
            "firstname",
            "middlename",
            "key",
            "gender",
            "contact_num",
            "description",
            "action"
        ], False);

        try:

            action = data['action'];
            del data['action'];

            details = Student(**data);
            details.clean();

            result = details.save();

        except Exception as e:

            e.args = [err for err in e.args if err];

            messages.error(request, e.args[-1]);
            return v_new(request, data);

    messages.success(request, "Successfully added");
    if action == "save_add": return v_new(request);

    pass;


def is_key_exists(request):
    if not User.is_authenticated: return;

    if len(request.GET) > 0:

        constraint = helpers.constraint(request);

        data = constraint.strict([
            "key",
        ], False);

        check: bool = Student.objects.filter(key=data['key']);

        if check: raise Exception("Alredy exist");

    return HttpResponse("Not Exist");


def r_student_list(request):
    if not User.is_authenticated or len(request.POST) <= 0:
        raise AttributeError("Invalid request");

    post_request = helpers.constraint(request, "POST");

    data = post_request.strict([
        "pageIndex",
        "pageSize",
        "search",
        "state"
    ], False);

    offset = int(data['pageSize']);
    index = int(data["pageIndex"]);

    startIndex = (index - 1 if index > 0 else 0) * offset;
    endIndex = startIndex + offset;

    search = Student.search(
        search=data["search"],
        start=startIndex,
        end=endIndex,
        removeEmpty=True,
        gender=data['state']
    );

    json_format = json.dumps({
        "data": list(search),
        "length": Student.objects.count()
    });

    return HttpResponse(json_format);


def r_student_login(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'user',
        'password'
    ], False);

    try:

        of_student = Student.objects.get(
            Q(id=data['user']) |
            Q(contact_num=("639" + str(data['user']))),
            Q(key=str(data['password']))
        );

        value = of_student.__dict__;
        del value['_state'];

        if of_student.id < 999:
            less = 4 - len(str(of_student.id));
            value['less_zeros'] = [0] * less;
            value['less_zeros'] = "".join([str(i) for i in value['less_zeros']]);


        request.session['login_student'] = value;
        return redirect("/student/home");


    except Student.DoesNotExist:
        messages.error(request, "Please enter the correct username and password "
                                "for a student account.Note that both "
                                "fields may be case-sensitive.");

        return redirect("/student/login");

    except Exception as e:
        messages.error(request, e);
        return redirect("/student/login");

    pass;


def v_home(request):
    is_valid = s_is_login(request);

    if (not isinstance(is_valid, HttpResponse)) and not is_valid:
        return redirect("/student/login");

    if isinstance(is_valid, HttpResponse): return is_valid;

    return render(request, "testers_temp/student/stud_home.html", {
        'title': "HOME",
        "init": "",
        "student": request.session['login_student']
    });

    pass;


def r_logout(request):
    if 'login_student' in request.session: del request.session['login_student'];
    return redirect("/student/login");

    pass;


def v_student_login(request):
    stud_login = helpers.constraint.check_student_login(request);
    if stud_login: return redirect("/student/home");

    if request.user.is_authenticated:
        messages.error(request, "You are authenticated as admin, but are not authorized to access this page");
        return render(request, "error.html", {
            "status": "404",
            "message": "you are authenticated as admin, but are not authorized to access this page"
        });

    return render(request, "testers_temp/student/login.html", {
        'title': "LOGIN",
        "init": ""
    });

    pass;


def r_load_student_activity(request):

    student_levels = batch_student \
        .objects \
        .filter(student=request.user);

    taken_activity = Quiz_activity \
        .objects \
        .filter(batch_student__student=request.user,
                state=True,
                is_done=True);

    activity_list = [];

    for per_batch in student_levels:

        of_activity = Task \
            .objects \
            .filter(batch=per_batch.batch_id, section=per_batch.section_id) \
            .exclude(activity_id__in=taken_activity.values_list('activity_id')) \
            .order_by("-section__group__level")

        if not len(of_activity): continue;

        for per_activity in of_activity:
            data_set = {
                "id": per_activity.id,
                "activity_id": per_activity.activity_id,
                "batch_id": per_activity.batch.id,
                "batch_name": per_activity.batch.name,
                "section": per_activity.section.name,
                "section_id": per_activity.section.id,
                "level": per_activity.section.group.name,
                "level_id": per_activity.section.group.id,
                "title": per_activity.title,
                "description": per_activity.description,
                "user": {
                    "id": per_activity.user.id,
                    "first_name": per_activity.user.first_name,
                    "last_name": per_activity.user.last_name,
                    "email": per_activity.user.email
                }
            };

            activity_list.append(data_set);

            pass;
        pass;

    result = json.dumps(activity_list);
    return HttpResponse(result);

    pass;


def v_activity_available_test(request):
    post_request = helpers.constraint(request, "GET");
    data = post_request.strict([
        'id'
    ], False);

    try:

        of_student = request.session['login_student'];

        of_activity = Task.objects.get(id=data['id']);
        teacher = of_activity.user_added;

        of_batch_student = batch_student \
            .objects \
            .get(student=of_student['id'],
                 batch=of_activity.batch_id,
                 section=of_activity.section_id);

        teacher_name = teacher.email;

        if teacher.first_name:
            teacher_name = teacher.first_name \
                                 + " " \
                           + teacher.last_name;
        elif teacher.username:
            teacher_name = "@" + teacher.username;

        result = {
            "activity_title": of_activity.title,
            'teacher_name': teacher_name,
            "batch_name": of_activity.batch.name,
            "group_level_name": of_activity.group_level.name,
            "section_name": of_activity.section.name,
            "batch_student_id": of_batch_student.id,
            "postTest": False,
            "preTest": False,
            "parameters": data
        };

        return render(request, "testers_temp/student/stud_activity_info.html", {
            **result,
            "student": request.session['login_student'],
            "title": "INFO"
        });


    except Task.DoesNotExist:

        messages.error(request, "Student Activity id is not valid and not exist");
        return render(request, "error.html", {
            "status": "505",
            "message": "Student Activity id is not valid and not exist"
        });

    except batch_student.DoesNotExist:

        messages.error(request, "Student is not on the group");
        return render(request, "error.html", {
            "status": "505",
            "message": "Student is not on the group"
        });

    except vw_task.DoesNotExist:

        messages.error(request, "Activity is not available on the specified group");
        return render(request, "error.html", {
            "status": "505",
            "message": "Activity is not available on the specified group"
        });

    pass;


def v_activity_start(request):
    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'temp_student_activity_id',
        'batch_student_id',
        'test_type'
    ], False);

    data = post_request.data;

    if is_valid:

        try:

            of_student = request.session['login_student'];

            of_batch_student = batch_student \
                .objects \
                .get(id=data['batch_student_id'],
                     student=of_student['id']);

            of_student_activity = vw_task \
                .objects \
                .get(
                id=data['temp_student_activity_id'],
                batch=of_batch_student.batch_id,
                section=of_batch_student.section_id
            );

            of_quiz_activity = Quiz_activity.objects.get(
                batch_student=of_batch_student.id,
                activity=of_student_activity.activity_id,
                state=data['test_type'],
                is_done=True
            );

            return redirect("/student/activity/score?quiz=" + str(of_quiz_activity.id));


        except Quiz_activity.DoesNotExist:

            is_continue = Quiz_activity \
                .objects \
                .filter(
                batch_student=of_batch_student.id,
                activity=of_student_activity.activity_id,
                state=data['test_type']
            ).exists();

            description = helpers \
                ._String \
                .format_by_paragraph(of_student_activity.description);

            result = {
                'activity_title': of_student_activity.title,
                'activity_description': description,
                "activity_id": of_student_activity.activity_id,
                "batch_student_id": of_batch_student.id,
                "is_continue": is_continue,
                "parameters": data
            };

            return render(request, "testers_temp/student/stud_activity_start.html", {
                **result,
                "student": request.session['login_student'],
                "title": "ACTIVITY"
            });

            pass;


        except batch_student.DoesNotExist:

            messages.error(request, "Student is not on the group");
            return render(request, "error.html", {
                "status": "505",
                "message": "Student is not on the group"
            });

        except vw_task.DoesNotExist:

            messages.error(request, "Activity is not available on the specified group");
            return render(request, "error.html", {
                "status": "505",
                "message": "Activity is not available on the specified group"
            });

        pass;

    else:
        # messages.error(request,"The specified activity could not found or not exist")
        return redirect("/student/home");
    pass;


def v_activity_score(request):
    get_request = helpers.constraint(request, "GET");
    is_valid = get_request.check([
        'quiz'
    ], False);

    data = get_request.data;

    if not is_valid: return redirect("/student/home"); pass;

    try:

        of_quiz_activity = Quiz_activity \
            .objects \
            .get(id=data['quiz']);

        of_batch_student = of_quiz_activity.batch_student;
        of_batch_student.rate_percent = of_quiz_activity.batch_student.rate(Quiz_activity);

        of_batch_student.save();

        student = of_quiz_activity.batch_student.student;
        teacher = of_quiz_activity.activity.user_added;
        of_activity = of_quiz_activity.activity;

        teacher_name = teacher.email;

        if teacher.first_name:

            teacher_name = " ".join(str(x) for x in [
                teacher.first_name,
                teacher.last_name
            ]);

        elif teacher.username:
            teacher_name = "@" + teacher.username;

        other_quiz = Quiz_activity.objects.filter(
            activity_id=of_quiz_activity.activity_id,
            batch_student_id=of_quiz_activity.batch_student_id,
            state=True if not of_quiz_activity.state else False
        );

        return render(request, "testers_temp/student/stud_activity_score.html", {
            "student": request.session['login_student'] if "login_student" in request.session else False,
            "other_test": other_quiz[0].id if other_quiz.exists() else -1,
            "student_name": {
                "first": student.firstname,
                "middle": student.middlename,
                "last": student.lastname
            },
            "title": "RESULTS",
            "teacher_name": teacher_name,
            "activity_title": of_activity.title,
            "batch_name": of_quiz_activity.batch_student.batch.name,
            "batch_id" : of_batch_student.id,
            "grade_name": of_quiz_activity.batch_student.section.group.name,
            "section_name": of_quiz_activity.batch_student.section.name,
            "activity_type": of_quiz_activity.state,
            "comprehension_level" : of_quiz_activity.comprehension_level,
            "compre_percent" : of_quiz_activity.comprehension_level_percent,
            **data
        });

    except Quiz_activity.DoesNotExist:

        messages.error(request, "The student has not been take this quiz ");
        return render(request, "error.html", {
            "status": "505",
            "message": "The student has not been take this quiz "
        });

        pass;

    except Quiz_assesment.DoesNotExist:

        messages.error(request, "Quiz_assesment id is not valid and not exist");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz_assesment id is not valid and not exist"
        });

        pass;

    pass;


def m_activity_load_passage(request, of_quiz_activity: Quiz_activity, params):
    request.session['start_lapse'] = helpers.of_date.getCurrentTimeStamp();

    try:

        of_assesment = Assesment \
            .objects \
            .filter(activity=of_quiz_activity.activity);

        of_student_answers = Students_answer \
            .objects \
            .filter(quiz_assesment__quiz_activity=of_quiz_activity.id);

        of_questions = Questions \
            .objects \
            .filter(assesment__in=of_assesment.values_list('id')) \
            .exclude(id__in=of_student_answers.values_list('question'))

        of_quiz_assesment = Quiz_assesment \
            .objects \
            .filter(quiz_activity_id=of_quiz_activity.id);

        of_by_assesment = Assesment \
            .objects \
            .filter(id__in=of_questions.values_list('assesment')) \
            .exclude(id__in=of_quiz_assesment.values_list('assesment'))

        if not (len(of_by_assesment) > 0):
            raise helpers.EmptySequence("Cannot choose from an empty sequence");

        pick_one = random.choice(of_by_assesment);
        body = helpers._String.format_by_paragraph(pick_one.body);

        result = {
            "quiz_activity_id": of_quiz_activity.id,
            "title": pick_one.title,
            "body": body,
            "type": pick_one.type,
            "of_type": pick_one.get_type_display(),
            "audio": pick_one.audio.name if pick_one.audio else "",
            "id": pick_one.id,
            "activity": pick_one.activity.title,
            "level": pick_one.activity.group_level.level
        };

        filename = "stud_" + str(result["of_type"]).lower() + ".html";

        return render(request, "testers_temp/student/quiz_type/" + filename, {
            **result,
            **params
        });

    except helpers.EmptySequence:

        literal = of_quiz_activity.total_correct_question_type(0);
        inferential = of_quiz_activity.total_correct_question_type(1);
        critical = of_quiz_activity.total_correct_question_type(2);

        score = of_quiz_activity.score();

        of_quiz_activity.total_correct_literal = literal;
        of_quiz_activity.total_correct_inferential = inferential;
        of_quiz_activity.total_correct_critical = critical;
        of_quiz_activity.total_score = score;
        of_quiz_activity.num_literal = of_quiz_activity.total_question_type(0);
        of_quiz_activity.num_inferential = of_quiz_activity.total_question_type(1);
        of_quiz_activity.num_critical = of_quiz_activity.total_question_type(2);
        of_quiz_activity.comprehension_level_percent = of_quiz_activity.of_comprehension_level_percent();
        of_quiz_activity.comprehension_level = of_quiz_activity.comprehension_level_identifier();

        of_quiz_activity.is_done = True;
        of_quiz_activity.save();

        return redirect("/student/activity/score?quiz=" + str(of_quiz_activity.id));

        pass;

    pass;


def v_activity_on_start(request):
    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'batch_student',
        'activity',
        'type'
    ], False);

    data = post_request.data;
    of_student = request.session['login_student'];

    if not is_valid: return redirect("/student/home");

    try:

        of_batch = batch_student.objects.get(id=data['batch_student']);

        of_activity = Activity \
            .objects \
            .get(id=data['activity']);

        of_quiz_activity = Quiz_activity \
            .objects \
            .get(
            batch_student=data['batch_student'],
            activity=data['activity'],
            state=bool(int(data['type']))
        );

        if of_quiz_activity.is_done: raise helpers.QuizDone("Quiz is already done");

        return m_activity_load_passage(request, of_quiz_activity, {
            "is_continue": 1,
            "student": request.session['login_student'],
            **data
        });

    except helpers.QuizDone as e:

        messages.error(request, e);
        return render(request, "error.html", {
            "status": "505",
            "message": e
        });

    except Quiz_activity.DoesNotExist:

        of_student = Student.objects.get(id=of_student['id']);

        of_quiz_activity = Quiz_activity(
            batch_student=of_batch,
            activity=of_activity,
            state=bool(int(data['type']))
        );

        of_quiz_activity.clean();
        of_quiz_activity.save();

        return v_activity_on_start(request);

        pass;

    except batch_student.DoesNotExist:

        messages.error(request, "Student is not on the group");
        return render(request, "error.html", {
            "status": "505",
            "message": "Student is not on the group"
        });

    except Student.DoesNotExist:

        messages.error(request, "Student is not valid and not exist");
        return render(request, "error.html", {
            "status": "505",
            "message": "Student is not valid and not exist"
        });

    except Activity.DoesNotExist:

        messages.error(request, "Activity is not valid and not exist");
        return render(request, "error.html", {
            "status": "505",
            "message": "Activity is not valid and not exist"
        });

    pass;


def m_activity_load_question(request, of_quiz_assesment: Quiz_assesment, params: list = []):
    try:

        request.session['start_lapse'] = helpers.of_date.getCurrentTimeStamp();

        of_students_answer = Students_answer \
            .objects \
            .filter(quiz_assesment_id=of_quiz_assesment.id);

        of_questions_not_excluded = Questions \
            .objects \
            .filter(assesment=of_quiz_assesment.assesment_id);

        of_questions = of_questions_not_excluded \
            .exclude(id__in=of_students_answer.values_list("question"));

        if not (len(of_questions) > 0):
            raise helpers.EmptySequence("Cannot choose from an empty sequence");

        pick_one = random.choice(of_questions);

        of_question_choices = Quiz_choices \
            .objects \
            .filter(question=pick_one.id);

        # dict_question = dict(pick_one.values('id','body'));
        list_choices = list(of_question_choices.values('id', 'body'));
        random.shuffle(list_choices)

        result = {
            "quiz_activity_id": of_quiz_assesment.quiz_activity_id,
            "quiz_assesment": of_quiz_assesment.id,
            "question_id": pick_one.id,
            "question_body": pick_one.body,
            "choices": list_choices,
            "index": len(of_students_answer) + 1,
            "length": len(of_questions_not_excluded),
            "alpha": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        };

        return render(request, "testers_temp/student/quiz_type/stud_question.html", {
            **params,
            **result,
            "title": "QUESTION",
            "student": request.session['login_student']
        });


    except helpers.EmptySequence:

        correct_answer = Students_answer.objects.filter(
            answer__is_correct=True,
            quiz_assesment_id=of_quiz_assesment.id
        ).count();

        responses_question = Students_answer \
            .objects \
            .filter(quiz_assesment_id=of_quiz_assesment.id) \
            .count();

        of_quiz_assesment.comprehension_score = correct_answer;
        of_quiz_assesment.no_respond = responses_question;
        of_quiz_assesment.comprehension_level = 0;

        passage_words = str(of_quiz_assesment.assesment.body).split(" ");
        reading_speed = (len(passage_words) / int(of_quiz_assesment.read_duration)) * 60;

        of_quiz_assesment.reading_speed = round(reading_speed);

        total_no_questions = of_quiz_assesment.no_question_set;
        total_compre_level_percentage = (correct_answer / total_no_questions) * 100;

        if total_compre_level_percentage >= 80:
            of_quiz_assesment.comprehension_level = 2;
        elif total_compre_level_percentage >= 59 and total_compre_level_percentage <= 79:
            of_quiz_assesment.comprehension_level = 1;

        of_quiz_assesment.save();

        return m_activity_load_passage(request, of_quiz_assesment.quiz_activity, {
            "is_continue": 1,
            "student": request.session['login_student'],
            **params
        });

    pass;


def v_activity_answered(request):

    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'question',
        'quiz_assesment',
        'radio-button'
    ], False);

    data = post_request.data;

    if not is_valid: return redirect("/student/home");

    that_student = request.session['login_student'];

    request.session['end_lapse'] = helpers.of_date.getCurrentTimeStamp();

    time_lapse = int(request.session['end_lapse']) - int(request.session['start_lapse']);

    try:

        of_quiz_assesment = Quiz_assesment \
            .objects \
            .get(id=data['quiz_assesment']);

        of_question = Questions \
            .objects \
            .get(id=data['question']);

        of_choices = Quiz_choices \
            .objects \
            .get(id=data['radio-button']);

        answered = Students_answer(
            answer=of_choices,
            question=of_question,
            quiz_assesment=of_quiz_assesment,
            time_lapse=time_lapse
        );

        answered.clean();
        answered.save();

        return m_activity_load_question(
            request,
            of_quiz_assesment,
            data
        );


    except helpers.AlreadyExist as e:

        messages.error(request, e);
        return render(request, "error.html", {
            "status": "505",
            "message": e
        });

        pass;

    except Quiz_activity.DoesNotExist:

        messages.error(request, "Quiz activity id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz activity id is invalid and not exists"
        });

    except Questions.DoesNotExist:

        messages.error(request, "Question id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Question id is invalid and not exists"
        });

    except Quiz_choices.DoesNotExist:

        messages.error(request, "Selection id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Selection id is invalid and not exists"
        });

    except Assesment.DoesNotExist:

        messages.error(request, "Assesment id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Assesment id is invalid and not exists"
        });

        pass;

    pass;


def r_activity_get_question(request):

    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'quiz_activity',
        'assesment',
        'time',
        'playRecord'
    ], False);

    data = post_request.data;

    if is_valid:

        try:

            that_student = request.session['login_student'];

            of_quiz_activity = Quiz_activity \
                .objects \
                .get(id=data['quiz_activity']);

            if of_quiz_activity.is_done: raise helpers.QuizDone("Quiz is already done");

            of_assesment = Assesment \
                .objects \
                .get(id=data['assesment']);

            of_quiz_assesment = Quiz_assesment \
                .objects \
                .get(assesment=data['assesment'],
                     quiz_activity=data['quiz_activity']);

            return m_activity_load_question(request, of_quiz_assesment, data);


        except helpers.QuizDone as e:

            messages.error(request, e);
            return render(request, "error.html", {
                "status": "505",
                "message": e
            });

        except Quiz_assesment.DoesNotExist:

            of_quiz_assesment = Quiz_assesment(
                quiz_activity=of_quiz_activity,
                assesment=of_assesment,
                read_duration=data['time'],
                no_play_record=data['playRecord']
            );

            total_no_questions = Questions \
                .objects \
                .filter(assesment_id=of_assesment) \
                .count();

            of_quiz_assesment.no_question_set = total_no_questions;

            of_quiz_assesment.clean();
            of_quiz_assesment.save();

            return m_activity_load_question(
                request,
                of_quiz_assesment,
                data
            );

            pass;

        except helpers.AlreadyExist as e:

            messages.error(request, e);
            return render(request, "error.html", {
                "status": "505",
                "message": e
            });

        except Quiz_activity.DoesNotExist:

            messages.error(request, "Quiz activity id is invalid and not exists");
            return render(request, "error.html", {
                "status": "505",
                "message": "Quiz activity id is invalid and not exists"
            });

        except Assesment.DoesNotExist:

            messages.error(request, "Assesment id is invalid and not exists");
            return render(request, "error.html", {
                "status": "505",
                "message": "Assesment id is invalid and not exists"
            });

            pass;

    else:
        return redirect("/student/home");


def r_activity_result(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'quiz_activity'
    ], False);

    try:

        of_quiz_activity = Quiz_activity.objects.get(id=data['quiz_activity']);

        of_assesment = Quiz_assesment \
            .objects \
            .filter(quiz_activity=of_quiz_activity.id) \
            .order_by("assesment") \
            .values(
            "id",
            "assesment_id",
            "comprehension_level",
            "no_respond",
            "comprehension_score",
            "no_question_set",
            "read_duration",
            "no_play_record",
            "audio",
            "reading_speed",
            title=F("assesment__title"),
            body=F("assesment__body"),
            activity=F("assesment__activity"),
            type=F("assesment__type"),
        );

        # ,"title","body","audio","activity","type"

        for those_assesment in of_assesment:

            those_assesment['body'] = helpers._String.format_by_paragraph(those_assesment["body"]);

            questions = Questions \
                .objects \
                .filter(assesment=those_assesment['assesment_id']) \
                .order_by("id").values("id", "body", "type", "assesment");

            those_assesment['questions'] = list(questions);

            for those_questions in those_assesment['questions']:

                try:
                    of_students_answers = Students_answer \
                        .objects \
                        .get(question=those_questions['id'], quiz_assesment=those_assesment['id']);

                    those_questions["answer"] = of_students_answers.answer.is_correct;
                    those_questions["time_lapse"] = of_students_answers.time_lapse;

                except Students_answer.DoesNotExist:
                    those_questions["answer"] = "";
                    pass;

            pass;

        return HttpResponse(json.dumps(list(of_assesment)));

    except Quiz_activity.DoesNotExist:

        messages.error(request, "Quiz activity id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz activity id is invalid and not exists"
        });

    pass;


def r_activity_level_question_result(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'quiz_activity'
    ], False);

    try:

        of_quiz_activity = Quiz_activity.objects.get(id=data['quiz_activity']);
        value = [];

        for per_type in Questions.ques_type:
            obj = {
                "index": int(per_type[0]),
                "value": str(per_type[1])
            };

            str_total_correct = "total_correct_" + obj["value"].lower();
            str_num = "num_" + obj["value"].lower();

            value_total_correct = getattr(of_quiz_activity, str_total_correct);
            value_num = getattr(of_quiz_activity, str_num);

            obj['total_correct'] = value_total_correct;
            obj['length'] = value_num;

            value.append(obj);

            pass;

        return HttpResponse(json.dumps(list(value)));

        pass;

    except Quiz_activity.DoesNotExist:

        messages.error(request, "Quiz activity id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz activity id is invalid and not exists"
        });

    pass;


def v_students_groups(request):

    is_valid = s_is_login(request);

    if (not isinstance(is_valid, HttpResponse)) and not is_valid:
        return redirect("/student/login");

    if isinstance(is_valid, HttpResponse): return is_valid;

    student = request.session['login_student'];

    of_batch_student = batch_student\
        .objects\
        .filter(student_id=student['id'])\
        .order_by("section__group__level")

    of_batch_student = of_batch_student.values(
        "id",
        "batch_id",
        "batch__name",
        "section__group_id",
        "section__group__name",
        "section_id",
        "section__name",
        "rate_percent"
    );

    for per in of_batch_student:

        per['task_length'] = Task.objects.filter(
            batch_id=per['batch_id'],
            section_id = per['section_id']
        ).count();

        pass;

    return render(request, "testers_temp/student/stud_groups.html", {
        'title': "GROUPS",
        "init": "",
        "student": request.session['login_student'],
        "group_data" : of_batch_student
    });


pass;
