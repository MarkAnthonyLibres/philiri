from django.db import models
from django.core.validators import MinLengthValidator
from django.forms import ModelForm
from django.forms import forms;
from django.core.exceptions import ValidationError
from app.validators import minLength;
from modules import helpers


# Create your models here.


# model named Post
class Student(models.Model):
    Male = 0
    FeMale = 1
    GENDER_CHOICES = (
        (Male, "Male"),
        (FeMale, "Female"),
    );

    _filter: list = ["firstname", "lastname", "middlename", "key"]

    contact_num_length: int = 9;
    key_min_length: int = 4;

    id = models.AutoField(primary_key=True);

    lastname = models.CharField(max_length=22, blank=False, null=False);

    firstname = models.CharField(max_length=22, blank=False, null=False);

    middlename = models.CharField(max_length=22);

    key = models.IntegerField(unique=True, null=False, blank=False);

    gender = models.IntegerField(blank=False, null=False, choices=GENDER_CHOICES, default=Male);

    contact_num = models.IntegerField();

    description = models.TextField();

    def clean(self):

        self.contact_num = self.contact_num or None;

        if self.contact_num and (len(str(self.contact_num)) != self.contact_num_length):
            raise ValidationError("Invalid Contact Number");

        if self.key and (len(str(self.key)) < self.key_min_length):
            raise ValidationError("Invalid Key Number");

        if len(str(self.key)) < 4:
            raise ValidationError('This value is too short. It should have 4 characters or more.')

        pass;

    def __str__(self):

        middlename = self.middlename + ("." if self.middlename else "");
        firstname = self.firstname + (" " if self.firstname else "");
        lastname = self.lastname + (" " if self.lastname else "");

        return '{}{}{} ({})'.format(
            lastname,
            firstname,
            middlename,
            self.key
        )

    @staticmethod
    def search(search, start=0, end=8,removeEmpty= False, **kwargs):

        sql_str = '''Select *
                    from vw_student 
                    where
                    {where}
                    (`fullname` like "{search}" OR 
                    `key` like "{search}")
                     LIMIT {start}, {end}
                  ''';


        if removeEmpty : kwargs = helpers._ofDict.removeEmpty(kwargs);

        params = {
            "start": start,
            "end": end,
            "search": "%%" + search + "%%",
            "where": helpers._ofString.sql_where(kwargs) + " and "\
                if len(kwargs.keys()) else ""
        };

        sql_str = sql_str.format(**params);

        query = Student.objects.raw(sql_str);
        result = helpers._RawQuerySet.toList(query);

        return result;

        pass;
