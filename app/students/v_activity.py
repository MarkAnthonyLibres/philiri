import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers;
from modules.SpeechRecognition import SpeechAnalyzer, Recognizer;
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from account.v_student import \
    has_user_profile, \
    is_not_personal_set, \
    is_personal_set, \
    is_member, \
    has_no_user_profile;

from class_sem.models import batch_student
from exercises.models import Task, vw_task
from exercises.models import Quiz_activity, \
    Activity, \
    Quiz_assesment, \
    Assesment, \
    Questions, \
    Students_answer, \
    Quiz_choices, \
    Oral_reading, \
    Word_recognition

import json;
import random;
from django.db.models import F;
from modules.SpeechRecognition import SpeechAnalyzer, Recognizer;
from exercises.forms import QuizAssesentForm


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_load_student_activity(request):

    student_levels = batch_student \
        .objects \
        .filter(student_id=request.user.id);

    taken_activity = Quiz_activity \
        .objects \
        .filter(batch_student__student_id=request.user.id,
                is_done=True);

    required_gst_pretest = Quiz_activity \
        .objects \
        .filter(batch_student__student_id=request.user.id,
                is_done=True);

    activity_list = [];

    for per_batch in student_levels:

        of_task = Task \
            .objects \
            .filter(batch=per_batch.batch_id, section=per_batch.section_id) \
            .exclude(id__in=taken_activity.values_list('task_id')) \
            .order_by("-section__group__level","task__date_added");

        for per_task in of_task:
            if not per_task.pretest_is_gst_set(): continue;
            activity_list.append(per_task);
            pass;

    return render(request, "testers_temp/student/home/misc/load_task_list.html", {
        "activity_list": activity_list,
        "item_count" : len(activity_list)
    });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_test_type(request):
    try:
        post_request = helpers.constraint(request, "GET");
        data = post_request.strict([
            'id'
        ], True);

        school_id = request.session['school_id'];

        of_student_activity = Task.objects.get(
            id=data['id'],
            batch__school_id=school_id
        );

        of_batch_student = batch_student \
            .objects \
            .get(student=request.user,
                batch=of_student_activity.batch_id,
                section=of_student_activity.section_id,
                batch__school_id=school_id
            );

        return render(request, "testers_temp/student/activity/stud_activity_info.html", {
            "batch_student" : of_batch_student,
            "user" : request.user,
            "batch" : of_batch_student.batch,
            "activity" : of_student_activity.activity,
            "section" : of_student_activity.section,
            "student_activity" : of_student_activity,
            "title": "INFO"
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Task.DoesNotExist:

        messages.error(request, "Student Activity id is not valid and not exist");
        return render(request, "error.html", {
            "status": "505",
            "message": "Student Activity id is not valid and not exist"
        });

    except batch_student.DoesNotExist:

        messages.error(request, "Student is not on the group");
        return render(request, "error.html", {
            "status": "505",
            "message": "Student is not on the group"
        });

    except vw_task.DoesNotExist:

        messages.error(request, "Activity is not available on the specified group");
        return render(request, "error.html", {
            "status": "505",
            "message": "Activity is not available on the specified group"
        });

    pass;



@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_start(request):

    post_request = helpers.constraint(request, "GET");
    is_valid = post_request.check([
        'id'
    ], False);

    data = post_request.data;

    if is_valid:

        try:

            school_id = request.session['school_id'];

            of_student_activity = Task.objects.get(
                id=data['id'],
                batch__school_id=school_id
            );

            of_batch_student = batch_student \
            .objects \
            .get(student=request.user,
                batch=of_student_activity.batch_id,
                section=of_student_activity.section_id,
                batch__school_id=school_id
            );

            of_quiz_activity = Quiz_activity.objects.get(
                batch_student=of_batch_student.id,
                task_id=of_student_activity.id,
                is_done=True
            );

            return redirect("/student/activity/score?quiz=" + str(of_quiz_activity.id));


        except Quiz_activity.DoesNotExist:

            is_continue = Quiz_activity \
                .objects \
                .filter(
                batch_student=of_batch_student.id,
                task_id=of_student_activity.id
            ).exists();

            description = of_student_activity.description\
                if of_student_activity.is_preTest else of_student_activity.activity.description;

            description = helpers \
                ._String \
                .format_by_paragraph(description);

            return render(request, "testers_temp/student/activity/stud_activity_start.html", {
                "task" : of_student_activity,
                "batch_student" : of_batch_student,
                "activity" : of_student_activity.activity,
                "parameters" : data,
                "activity_description" : description,
                "user": request.user,
                "is_continue": is_continue,
                "title": "ACTIVITY"
            });

            pass;


        except batch_student.DoesNotExist:

            messages.error(request, "Student is not on the group");
            return render(request, "error.html", {
                "status": "505",
                "message": "Student is not on the group"
            });

        except vw_task.DoesNotExist:

            messages.error(request, "Activity is not available on the specified group");
            return render(request, "error.html", {
                "status": "505",
                "message": "Activity is not available on the specified group"
            });

        pass;

    else:
        # messages.error(request,"The specified activity could not found or not exist")
        return redirect("/student/home");
    pass;



# GST and Post Test
def m_load_passage_quiz(of_quiz_activity: Quiz_activity):

     of_assesment = Assesment \
        .objects \
        .filter(activity=of_quiz_activity.task.activity);

     of_student_answers = Students_answer \
        .objects \
        .filter(quiz_assesment__quiz_activity=of_quiz_activity.id);

     of_questions = Questions \
        .objects \
        .filter(assesment__in=of_assesment.values_list('id')) \
        .exclude(id__in=of_student_answers.values_list('question'))

     of_quiz_assesment = Quiz_assesment \
        .objects \
        .filter(quiz_activity_id=of_quiz_activity.id);

     of_by_assesment = Assesment \
        .objects \
        .filter(id__in=of_questions.values_list('assesment')) \
        .exclude(id__in=of_quiz_assesment.values_list('assesment'));

     return of_by_assesment;


#PreTest
def m_load_passage_pretest(of_quiz_activity: Quiz_activity):
    breakpoint();
    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def m_activity_load_passage(request, of_quiz_activity: Quiz_activity, params):

    request.session['start_lapse'] = helpers.of_date.getCurrentTimeStamp();

    try:

        of_by_assesment = m_load_passage_quiz(of_quiz_activity);

        if not (len(of_by_assesment) > 0):
            raise helpers.EmptySequence("Cannot choose from an empty sequence");

        pick_one = random.choice(of_by_assesment);

        body = helpers._String.format_by_paragraph(pick_one.body);

        result = {
            "quiz_activity_id": of_quiz_activity.id,
            "title": pick_one.title,
            "body": body,
            "type": pick_one.type,
            "of_type": pick_one.get_type_display(),
            "audio": pick_one.audio.name if pick_one.audio else "",
            "id": pick_one.id,
            "activity": pick_one.activity.title,
            "level": pick_one.activity.group_level.level
        };

        filename = "stud_" + str(result["of_type"]).lower() + ".html";

        return render(request, "testers_temp/student/quiz_type/" + filename, {
            **result,
            **params
        });

    except helpers.EmptySequence:

        literal = of_quiz_activity.total_correct_question_type(0);
        inferential = of_quiz_activity.total_correct_question_type(1);
        critical = of_quiz_activity.total_correct_question_type(2);
        score = of_quiz_activity.score();

        of_quiz_activity.total_correct_literal = literal;
        of_quiz_activity.total_correct_inferential = inferential;
        of_quiz_activity.total_correct_critical = critical;
        of_quiz_activity.total_score = score;
        of_quiz_activity.num_literal = of_quiz_activity.total_question_type(0);
        of_quiz_activity.num_inferential = of_quiz_activity.total_question_type(1);
        of_quiz_activity.num_critical = of_quiz_activity.total_question_type(2);
        of_quiz_activity.comprehension_level_percent = of_quiz_activity.of_comprehension_level_percent();
        of_quiz_activity.comprehension_level = of_quiz_activity.comprehension_level_identifier();
        of_quiz_activity.question_set = of_quiz_activity.no_question_set();

        of_quiz_activity.is_done = True;
        of_quiz_activity.save();

        return redirect("/student/activity/score?quiz=" + str(of_quiz_activity.id));

        pass;

    pass;



@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def m_activity_load_question(request, of_quiz_assesment: Quiz_assesment, params: list = []):
    try:

        request.session['start_lapse'] = helpers.of_date.getCurrentTimeStamp();

        of_students_answer = Students_answer \
            .objects \
            .filter(quiz_assesment_id=of_quiz_assesment.id);

        of_questions_not_excluded = Questions \
            .objects \
            .filter(assesment=of_quiz_assesment.assesment_id);

        of_questions = of_questions_not_excluded \
            .exclude(id__in=of_students_answer.values_list("question"));

        if not (len(of_questions) > 0):
            raise helpers.EmptySequence("Cannot choose from an empty sequence");

        pick_one = random.choice(of_questions);

        of_question_choices = Quiz_choices \
            .objects \
            .filter(question=pick_one.id);

        # dict_question = dict(pick_one.values('id','body'));
        list_choices = list(of_question_choices.values('id', 'body'));
        random.shuffle(list_choices)

        result = {
            "quiz_activity_id": of_quiz_assesment.quiz_activity_id,
            "quiz_assesment": of_quiz_assesment.id,
            "question_id": pick_one.id,
            "question_body": pick_one.body,
            "choices": list_choices,
            "index": len(of_students_answer) + 1,
            "length": len(of_questions_not_excluded),
            "alpha": "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        };

        # print("Gago moko!!!");
        # breakpoint();

        return render(request, "testers_temp/student/quiz_type/stud_question.html", {
            **params,
            **result,
            "title": "QUESTION",
            "user": request.user
        });


    except helpers.EmptySequence:

        correct_answer = Students_answer.objects.filter(
            answer__is_correct=True,
            quiz_assesment_id=of_quiz_assesment.id
        ).count();

        responses_question = Students_answer \
            .objects \
            .filter(quiz_assesment_id=of_quiz_assesment.id) \
            .count();

        of_quiz_assesment.comprehension_score = correct_answer;
        of_quiz_assesment.no_respond = responses_question;
        of_quiz_assesment.comprehension_level = 0;

        passage_words = str(of_quiz_assesment.assesment.body).split(" ");
        reading_speed = (len(passage_words) / int(of_quiz_assesment.read_duration)) * 60;

        of_quiz_assesment.reading_speed = round(reading_speed);

        total_no_questions = of_quiz_assesment.no_question_set;

        total_compre_level_percentage = (correct_answer / total_no_questions) * 100;

        if total_compre_level_percentage >= 80:
            of_quiz_assesment.comprehension_level = 2;
        elif total_compre_level_percentage >= 59 and total_compre_level_percentage <= 79:
            of_quiz_assesment.comprehension_level = 1;

        of_quiz_assesment.save();

        return m_activity_load_passage(request, of_quiz_assesment.quiz_activity, {
            "is_continue": 1,
            "user": request.user,
            **params
        });

    pass;



@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_ready_load(request):
    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'batch_student',
        'task'
    ], False);

    data = post_request.data;
    if not is_valid: return redirect("/student/home");

    school_id = request.session['school_id'];

    try:

        of_batch = batch_student.objects.get(
            id=data['batch_student'],
            batch__school_id= school_id
        );

        of_task = Task.objects.get(
            id=data['task'],
            batch_id=of_batch.batch_id,
            section_id=of_batch.section_id
        );

        of_activity = of_task.activity;

        of_quiz_activity = Quiz_activity \
            .objects \
            .get(batch_student=of_batch.id,task=of_task);

        if of_quiz_activity.is_done: raise helpers.QuizDone("Quiz is already done");

        return m_activity_load_passage(request, of_quiz_activity, {
            "is_continue": 1,
            "user": request.user,
            **data
        });

    except Quiz_activity.DoesNotExist:

        of_quiz_activity = Quiz_activity(
            batch_student=of_batch,
            task=of_task
        );

        of_quiz_activity.clean();
        of_quiz_activity.save();

        return v_ready_load(request);

        pass;


    except helpers.QuizDone as e:

        messages.error(request, e);
        return render(request, "error.html", {
            "status": "505",
            "message": e
        });

    except batch_student.DoesNotExist:

        messages.error(request, "Student is not on the group");
        return render(request, "error.html", {
            "status": "505",
            "message": "Student is not on the group"
        });


    except Task.DoesNotExist:

        messages.error(request, "Task is not on the group");
        return render(request, "error.html", {
            "status": "505",
            "message": "Task is not on the group"
        });


    except Activity.DoesNotExist:

        messages.error(request, "Activity is not valid and not exist");
        return render(request, "error.html", {
            "status": "505",
            "message": "Activity is not valid and not exist"
        });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_activity_get_question(request):
    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'quiz_activity',
        'assesment',
        'time',
        'playRecord'
    ], False);

    data = post_request.data;

    if is_valid:

        try:

            of_quiz_activity = Quiz_activity \
                .objects \
                .get(id=data['quiz_activity']);

            if of_quiz_activity.is_done: raise helpers.QuizDone("Quiz is already done");

            of_assesment = Assesment \
                .objects \
                .get(id=data['assesment']);

            of_quiz_assesment = Quiz_assesment \
                .objects \
                .get(assesment=data['assesment'],
                     quiz_activity=data['quiz_activity']);

            return m_activity_load_question(request, of_quiz_assesment, data);


        except helpers.QuizDone as e:

            messages.error(request, e);
            return render(request, "error.html", {
                "status": "505",
                "message": e
            });

        except Quiz_assesment.DoesNotExist:

            of_quiz_assesment = Quiz_assesment(
                quiz_activity=of_quiz_activity,
                assesment=of_assesment,
                read_duration=data['time'],
                no_play_record=data['playRecord']
            );

            total_no_questions = Questions \
                .objects \
                .filter(assesment_id=of_assesment) \
                .count();

            of_quiz_assesment.no_question_set = total_no_questions;

            of_quiz_assesment.clean();
            of_quiz_assesment.save();

            return m_activity_load_question(
                request,
                of_quiz_assesment,
                data
            );

            pass;

        except helpers.AlreadyExist as e:

            messages.error(request, e);
            return render(request, "error.html", {
                "status": "505",
                "message": e
            });

        except Quiz_activity.DoesNotExist:

            messages.error(request, "Quiz activity id is invalid and not exists");
            return render(request, "error.html", {
                "status": "505",
                "message": "Quiz activity id is invalid and not exists"
            });

        except Assesment.DoesNotExist:

            messages.error(request, "Assesment id is invalid and not exists");
            return render(request, "error.html", {
                "status": "505",
                "message": "Assesment id is invalid and not exists"
            });

            pass;

    else:
        return redirect("/student/home");


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_activity_answered(request):

    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'question',
        'quiz_assesment',
        'radio-button'
    ], False);

    data = post_request.data;

    if not is_valid: return redirect("/student/home");

    request.session['end_lapse'] = helpers.of_date.getCurrentTimeStamp();

    time_lapse = int(request.session['end_lapse']) - int(request.session['start_lapse']);

    try:

        of_quiz_assesment = Quiz_assesment \
            .objects \
            .get(id=data['quiz_assesment']);

        of_question = Questions \
            .objects \
            .get(id=data['question']);

        of_choices = Quiz_choices \
            .objects \
            .get(id=data['radio-button']);

        answered = Students_answer(
            answer=of_choices,
            question=of_question,
            quiz_assesment=of_quiz_assesment,
            time_lapse=time_lapse
        );

        answered.clean();
        answered.save();

        return m_activity_load_question(
            request,
            of_quiz_assesment,
            data
        );


    except helpers.AlreadyExist as e:

        messages.error(request, e);
        return render(request, "error.html", {
            "status": "505",
            "message": e
        });

        pass;

    except Quiz_activity.DoesNotExist:

        messages.error(request, "Quiz activity id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz activity id is invalid and not exists"
        });

    except Questions.DoesNotExist:

        messages.error(request, "Question id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Question id is invalid and not exists"
        });

    except Quiz_choices.DoesNotExist:

        messages.error(request, "Selection id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Selection id is invalid and not exists"
        });

    except Assesment.DoesNotExist:

        messages.error(request, "Assesment id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Assesment id is invalid and not exists"
        });

        pass;

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_activity_score(request):
    get_request = helpers.constraint(request, "GET");
    is_valid = get_request.check([
        'quiz'
    ], False);

    data = get_request.data;

    if not is_valid: return redirect("/student/home"); pass;

    try:

        of_quiz_activity = Quiz_activity \
            .objects \
            .get(id=data['quiz']);

        of_batch_student = of_quiz_activity.batch_student;
        of_batch_student.rate_percent = of_quiz_activity.batch_student.rate(Quiz_activity);

        of_batch_student.save();

        student = of_quiz_activity.batch_student.student;
        teacher = of_quiz_activity.task.activity.user_added;
        of_activity = of_quiz_activity.task.activity;

        teacher_name = teacher.email;

        if teacher.first_name:

            teacher_name = " ".join(str(x) for x in [
                teacher.first_name,
                teacher.last_name
            ]);

        elif teacher.username:
            teacher_name = "@" + teacher.username;


        return render(request, "testers_temp/student/activity/stud_activity_score.html", {
            "other_test": "",
            "student" : student,
            "title": "RESULTS",
            "teacher_name": teacher_name,
            "quiz_activity" : of_quiz_activity,
            "activity" : of_quiz_activity.task.activity,
            "school" : request.session['school_profile'],
            **data
        });

    except Quiz_activity.DoesNotExist:

        messages.error(request, "The student has not been take this quiz ");
        return render(request, "error.html", {
            "status": "505",
            "message": "The student has not been take this quiz "
        });

        pass;

    except Quiz_assesment.DoesNotExist:

        messages.error(request, "Quiz_assesment id is not valid and not exist");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz_assesment id is not valid and not exist"
        });

        pass;

    pass;



def r_activity_result(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'quiz_activity'
    ], False);

    try:

        of_quiz_activity = Quiz_activity.objects.get(id=data['quiz_activity']);

        of_assesment = Quiz_assesment \
            .objects \
            .filter(quiz_activity=of_quiz_activity.id) \
            .order_by("assesment") \
            .values(
            "id",
            "assesment_id",
            "comprehension_level",
            "no_respond",
            "comprehension_score",
            "no_question_set",
            "read_duration",
            "no_play_record",
            "audio",
            "reading_speed",
            title=F("assesment__title"),
            body=F("assesment__body"),
            activity=F("assesment__activity"),
            type=F("assesment__type"),
        );


        # ,"title","body","audio","activity","type"

        for those_assesment in of_assesment:

            those_assesment['body'] = helpers._String.format_by_paragraph(those_assesment["body"]);

            questions = Questions \
                .objects \
                .filter(assesment=those_assesment['assesment_id']) \
                .order_by("id").values("id", "body", "type", "assesment");


            those_assesment['questions'] = list(questions);

            for those_questions in those_assesment['questions']:

                try:
                    of_students_answers = Students_answer \
                        .objects \
                        .get(question=those_questions['id'], quiz_assesment=those_assesment['id']);

                    those_questions["answer"] = of_students_answers.answer.is_correct;
                    those_questions["time_lapse"] = of_students_answers.time_lapse;

                except Students_answer.DoesNotExist:
                    those_questions["answer"] = "";
                    pass;

            pass;

        return HttpResponse(json.dumps(list(of_assesment)));

    except Quiz_activity.DoesNotExist:

        messages.error(request, "Quiz activity id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz activity id is invalid and not exists"
        });

    pass;


def r_activity_level_question_result(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        'quiz_activity'
    ], False);

    try:

        of_quiz_activity = Quiz_activity.objects.get(id=data['quiz_activity']);
        value = [];

        for per_type in Questions.ques_type:
            obj = {
                "index": int(per_type[0]),
                "value": str(per_type[1])
            };

            str_total_correct = "total_correct_" + obj["value"].lower();
            str_num = "num_" + obj["value"].lower();

            value_total_correct = getattr(of_quiz_activity, str_total_correct);
            value_num = getattr(of_quiz_activity, str_num);

            obj['total_correct'] = value_total_correct;
            obj['length'] = value_num;

            value.append(obj);

            pass;

        return HttpResponse(json.dumps(list(value)));

        pass;

    except Quiz_activity.DoesNotExist:

        messages.error(request, "Quiz activity id is invalid and not exists");
        return render(request, "error.html", {
            "status": "505",
            "message": "Quiz activity id is invalid and not exists"
        });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def oral_result(request):

    post_request = helpers.constraint(request, "POST");
    is_valid = post_request.check([
        'quiz_activity',
        'assesment',
        'time',
        'playRecord'
    ], False);

    data = post_request.data;

    if not is_valid:
        return redirect("/student/home");

    try:

        of_quiz_activity = Quiz_activity \
            .objects \
            .get(id=data['quiz_activity']);

        if of_quiz_activity.is_done: raise helpers.QuizDone("Quiz is already done");

        # data['assesment']
        of_assesment = Assesment \
            .objects \
            .get(id=data['assesment']);

        body = of_assesment.body;
        title = of_assesment.title;
        join_test = " ".join([title, body]);

        that_voice = Recognizer(join_test, request.FILES['audio']);
        request.session['oral_test'] = {};

        if len(that_voice.error_parse) > 0:

            form_data = {
                "quiz_activity" : data['quiz_activity'],
                "assesment" : data['assesment'],
                "read_duration" : data['time'],
                "no_play_record" : data['playRecord']
            };

            of_quiz_assesment = QuizAssesentForm(data=form_data,files=request.FILES );

            of_quiz_assesment.full_clean();
            of_quiz_assesment.clean();
            of_quiz_assesment.save();

            of_quiz_assesment = of_quiz_assesment.save();

            of_oral_reading = Oral_reading(
                quiz_assesment=of_quiz_assesment,
                audio=of_quiz_assesment.audio,
                text_base=of_assesment.body,
                title_base=of_assesment.title
            );

            of_oral_reading.full_clean();
            of_oral_reading.clean();
            of_oral_reading.save();

            for per in that_voice.miscue:

                miscue_info =  per.get_info();

                of_word_recognition = Word_recognition(
                    oral_reading=of_oral_reading,
                    miscue_type= miscue_info["type"],
                    index= miscue_info["index"],
                    miscue_text=miscue_info["miscue_base"],
                    miscue_recognize=miscue_info["miscue_result"]
                );

                if not of_word_recognition.is_exists():
                    of_word_recognition.full_clean();
                    of_word_recognition.clean();
                    of_word_recognition.save();
                pass;

            return m_activity_load_question(
                request,
                of_quiz_assesment,
                data
            );

            pass;
        else:

            request.session['oral_test']["miscue"] = that_voice.miscue;
            request.session['oral_test']["parse_error"] = that_voice.error_parse;
            pass;

        return HttpResponse('Success');

    except Quiz_activity.DoesNotExist:

            messages.error(request, "Quiz activity id is invalid and not exists");
            return render(request, "error.html", {
                "status": "505",
                "message": "Quiz activity id is invalid and not exists"
            });



    pass;
