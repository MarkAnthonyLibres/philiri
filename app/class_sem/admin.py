from django.contrib import admin
from .models import batch_student

@admin.register(batch_student)
class BatchStudentAdmin(admin.ModelAdmin):
    list_display = ('section', 'student', 'batch', 'date_added','user_added')
    list_filter = ('section', 'batch', "date_added" , "user_added");
    search_fields = (
        'section',
        'student',
        'batch'
    )

# Register your models here.
