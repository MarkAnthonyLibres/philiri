from django.shortcuts import render, redirect, HttpResponse
from django.forms.models import model_to_dict
from group.models import Batch, Section
from students.models import Student;
from modules import helpers
from django.contrib import messages;
from exercises.models import Quiz_activity;
from .models import batch_student
from django.core.exceptions import ValidationError
from django.template import RequestContext
import json



huwaw = Quiz_activity;


# Create your views here.


def v_class(request):
    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "index",
        "section"
    ], False);

    try:
        x = Batch.objects.get(id=data['index'])

        batch_model = helpers._Model(x);
        batch_info = batch_model.getValue();

        y = Section.objects.get(id=data['section'])

        section_model = helpers._Model(y);
        section_info = section_model.getValue();

        section_info["group_id"] = y.group_id;

        # print(section_info);

        return render(request, "include/class_info.html", {
            'title': "CLASS",
            "init": "",
            "batch_info": batch_info,
            "batch_object" : x,
            "section_info": section_info,
            "section_object" : y,
            "full_width" : True,
        });

    except Batch.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    except Section.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    pass;


def v_students_not_in_class(request):
    get_request = helpers.constraint(request, "GET");
    data = get_request.strict([
        "section",
        "batch",
    ], True);

    try:
        x = Batch.objects.get(id=data['batch'])

        batch_model = helpers._Model(x);
        batch_info = batch_model.getValue();

        y = Section.objects.get(id=data['section'])

        section_model = helpers._Model(y);
        section_info = section_model.getValue();

        section_info["group_id"] = y.group_id;

        return render(request, "modal/add_student.html", {
            'title': "CLASS",
            "init": "",
            "batch": batch_info,
            "section": section_info
        });

    except Batch.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    except Section.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    pass;

    # return render(request, "include/section_add_student.html", { });

    pass;


def add_students_in_batch(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "section",
        "batch",
        "student"
    ], True);

    try:

        # Error if does not exists
        batch = Batch.objects.get(id=data['batch']);
        # Error if does not exists
        section = Section.objects.get(id=data['section']);
        # Error if does not exists
        student = Student.objects.get(id=data['student'])

        toInsert = {
            "student": student,
            "batch": batch,
            "section": section,
            "user": request.user
        }

        details = batch_student(**toInsert);
        details.clean();
        result = details.save();

        return HttpResponse(1);


    except Batch.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    except Section.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    except Student.DoesNotExist:
        messages.error(request, "Student does not exist and invalid");
        return redirect("/");

    except ValidationError as e:
        messages.error(request, e);
        return redirect("/");

        # dito ko sya ilalagay!!!
        # return redirect(request.current_page_url);

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def listof_students_not_in_class(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "batch",
        "section",
        "pageSize",
        "search"
    ], False);

    try:

        # Error if does not exists
        batch = Batch.objects.get(id=data['batch']);
        students = batch_student.students_not_in_class(
            batch_id=batch.id,
            section_id=data['section'],
            limit=data['pageSize'],
            search=data['search']
        );

        value = helpers._ofList.onlyKeys(students, [
            "id",
            "lastname",
            "firstname",
            "middlename",
            "gender",
            "contact_num",
            "description",
            "fullname"
        ]);

        json_format = json.dumps(value);

        return HttpResponse(json_format);

        pass;

    except Batch.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;


def r_listof_students(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "section",
        "batch",
        "pageIndex",
        "pageSize",
        "search",
        "state"
    ], False);

    try:

        # Error if does not exists
        batch = Batch.objects.get(id=data['batch']);
        # Error if does not exists
        section = Section.objects.get(id=data['section']);

        offset = int(data['pageSize']);
        index = int(data["pageIndex"]);

        startIndex = (index - 1 if index > 0 else 0) * offset;
        endIndex = startIndex + offset;

        students = batch_student.getStudents(
            search=data["search"],
            start=startIndex,
            end=endIndex,
            removeEmpty=True,
            gender=data['state'],
            batch_id=batch.id,
            section_id=section.id
        );

        count_rows = batch_student.getStudents(
            search=data["search"],
            isCount=True,
            removeEmpty=True,
            gender=data['state'],
            batch_id=batch.id,
            section_id=section.id
        )[0]['num'];

        json_format = json.dumps({
            "data": list(students),
            "length": count_rows
        }, default=helpers._JSON.json_serial);

        return HttpResponse(json_format);


    except Batch.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    except Section.DoesNotExist:
        messages.error(request, "class does not exist and invalid");
        return redirect("/");

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;

