from django.db import models
# from app.students.models import Student, Batch;
from datetime import date;
from django.contrib.auth.models import User, AnonymousUser;
from students.models import Student;
from django.core.exceptions import ValidationError
from modules import helpers;
from django.db.models import Sum;
from group.models import Batch, Section;
from account.models import UserProfile



# from exercises.models import Activity

# Create your models here.


class batch_student(models.Model):
    id = models.AutoField(primary_key=True);
    section = models.ForeignKey(Section, on_delete=models.DO_NOTHING, default=None,null=False, blank=False);
    student = models.ForeignKey(User, related_name="Student", on_delete=models.DO_NOTHING);
    batch = models.ForeignKey(Batch, on_delete=models.DO_NOTHING);
    date_added = models.DateField(null=False, blank=False, default=date.today);
    user_added = models.ForeignKey(User, related_name="Added_by", blank=False, null=False,on_delete=models.DO_NOTHING);
    rate_percent = models.IntegerField(blank=False, null=False,default=0);

    def clean(self):

        if self.check_if_student_exist():
            raise ValidationError("Student already been added on selected batch");

        pass;

    def check_if_student_exist(self):

        details = batch_student\
            .objects\
            .filter(student=self.student, batch=self.batch, section=self.section)

        return details.count() > 0

        pass;

    @staticmethod
    def getStudents(search,
                    isCount = False,
                    start = None,
                    end = None,
                    removeEmpty= False,
                    **kwargs):

        sql_str = '''
        Select {isCount} from vw_student_batch
        where
        {where}
        `full_name` like "{search}"
        {limit}
        ''';

        if removeEmpty: kwargs = helpers._ofDict.removeEmpty(kwargs);

        params = {
            "isCount" : "1 as id, COUNT(1) as num" if isCount else "*",
            "limit": ("LIMIT " + str(start) + "," + str(end)) if end else '' ,
            "search": "%%" + search + "%%",
            "where": helpers._ofString.sql_where(kwargs) + " and " \
                if len(kwargs.keys()) else ""
        };

        sql_str = sql_str.format(**params);

        query = batch_student.objects.raw(sql_str);
        return helpers._RawQuerySet.toList(query);

        pass;


    @staticmethod
    def students_not_in_class(batch_id,section_id, limit, search):

        sql_str = '''
        SELECT *, (
            select 1 from vw_student_batch 
            where vw_student_batch.student_id = vw_student.id and
            vw_student_batch.batch_id = {batch_id} and 
            vw_student_batch.section_id = {section_id}
            limit 1
        ) as hasBatch FROM vw_student
         where vw_student.fullname like '%%{search}%%'
         having hasBatch is null
         {limit}  
        ''';

        params = {
            'batch_id': batch_id,
            'section_id' : section_id,
            'limit' : ("LIMIT " + limit) if limit else '',
            'search' : search,

        }

        sql_str = sql_str.format(**params);

        print(sql_str);

        query = batch_student.objects.raw(sql_str);
        return helpers._RawQuerySet.toList(query);

        pass;

    def rate(self,that_quiz_activity):

        of_quiz_activity = that_quiz_activity\
            .objects\
            .filter(batch_student_id=self.id);

        num_length = of_quiz_activity.count();
        total_over = num_length * 100;

        total_compre_level = of_quiz_activity\
            .aggregate(Sum('comprehension_level_percent'))['comprehension_level_percent__sum'] or 0;

        print()

        if not total_over: return 0;

        return int((total_compre_level / total_over) * 100);

        pass;

    def student_profile(self):

        return UserProfile.objects.get(user_id=self.student)

        pass;

    def school_profile(self):

        return UserProfile.objects.get(user_id=self.student, )

        pass;


pass;



