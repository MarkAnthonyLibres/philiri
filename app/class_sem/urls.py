from django.urls import path;
from django.contrib import admin
from . import views


urlpatterns = [
    path("class", views.v_class, name="students"),
    path("class/student/students_not_in_class", views.v_students_not_in_class, name="students"),
    path("class/student/add", views.add_students_in_batch, name="students"),
    path("class/student/not_listed", views.listof_students_not_in_class, name="students"),
    path("class/students", views.r_listof_students, name="section")
    # path("class/student/students_not_in_class", views.v_class_student, name="students")
]