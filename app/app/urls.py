"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', include("login.urls")),
    path('', include("home.urls")),
    path('', include("students.urls")),
    path('', include("group.urls")),
    path('', include("class_sem.urls")),
    path('', include("task.urls")),
    path('', include("exercises.urls")),
    path('', include("account.urls")),
    path('', include("school.urls")),

    path('', include("teachers.urls")),
    path('', include("teachers.urls_classroom")),
    path('', include("teachers.urls_activity")),
    path('', include("teachers.urls_batch")),
    path('', include("teachers.urls_task")),
    path('', include("teachers.urls_my_task")),


    path('admin/', admin.site.urls)
]


urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT);

admin.site.site_header = "PhilIRI Admin"
admin.site.site_title = "PhilIRI Admin Portal"
admin.site.index_title = "Welcome to PhilIRI Admin Portal"
admin.site.site_url = "/home"
