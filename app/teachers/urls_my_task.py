
from . import views
from . import v_my_task
from django.urls import path

urlpatterns = [
    path("teacher/task/index", v_my_task.index, name="teacher_add_activity.html"),
    path("teacher/task/populate", v_my_task.r_populate_list_of_task, name="teacher_add_activity.html"),
]
