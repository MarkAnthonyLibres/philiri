
from . import views
from django.urls import path

from . import v_classroom

urlpatterns = [
    path("teacher/classroom", v_classroom.index, name="home"),
    path("teacher/classroom/view", v_classroom.v_grade, name="teacher_classroom_info.html"),
    path("teacher/classroom/sections", v_classroom.v_sections_list, name="home"),
    path("teacher/classroom/goto", v_classroom.r_gotoPage, name="home"),
]
