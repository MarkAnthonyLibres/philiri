
from . import v_task
from . import v_gst
from django.urls import path

from . import v_classroom

urlpatterns = [
     # View
    # get information of specified batch, section and grade level on GST tab

    path("teacher/gst/batch/list", v_gst.v_list, name="home"),

    # View
    # get information of specified batch, section and grade level on POST TEST tab

    path("teacher/task/list", v_task.v_postTest, name="home"),

    # View
    # get information of specified batch, section and grade level on PRE TEST tab

    path("teacher/task/list/pre", v_task.v_preTest, name="teacher_batch_pre_test.html"),


    # modal
    # @ajax INSERT
    # Insert or add new task as Pre test

    path("teacher/task/list/pre/add", v_task.r_add_preTest, name="home"),

    # modal
    # @ajax SELECT
    # populate a list of available activity and already added task
    # GST and PostTest

    path("teacher/task/list/add", v_task.r_modal_activity_list, name="home"),

    # modal
    # @ajax INSERT
    # Insert or add new task from available activity
    # GST and PostTest

    path("teacher/task/list/add/request", v_task.r_modal_add_activity_as_task, name="home"),

    # VIEW
    # @ajax SELECT
    # populate list of task that has been added through modal
    # GST and PostTest

    path("teacher/task/list/load", v_task.r_gst_post_task_list, name="teacher_ajax_tmp_task_list.html"),


    # VIEW
    # @ajax SELECT
    # populate list of task that has been added through modal
    # PreTest
    path("teacher/task/list/pre/load", v_task.r_load_preTest, name="home"),


    # MODAL
    # @ajax SELECT
    # populate list of available activity to be added as task
    # PreTest
    path("teacher/task/list/pre/activity/load", v_task.r_load_activity_preTest, name="home"),


    # VIEW
    # @ajax GET
    # GET and View Information of selected task

    path("teacher/task/view", v_task.index, name="home"),


    # VIEW
    # @ajax GET
    # GET and View Information and the result of selected task
    path("teacher/task/result", v_task.v_task_result, name="teacher_batch_task_result.html"),


    # SELECT
    # @ajax POST
    # populate the list of students
    path("teacher/task/result/students/completed/list", v_task.r_task_student_state, name="home"),


    # VIEW
    # @ajax GET
    # view results of student quiz
    path("teacher/task/student/quiz/score", v_task.v_task_quiz_result, name="teacher_batch_student_task_score.html"),


    # VIEW
    # @ajax GETteacher/task/oral/student/pending
    # view pending request of oral reading
    path("teacher/task/oral/student/pending", v_task.v_task_pending_oral, name="teacher_batch_student_task_oral_pending.html"),


    # @ajax POST
    path("teacher/task/oral/student/pending/populate", v_task.r_populate_pending_passages_oral, name="teacher_tmp_task_populate_ungraded_passages.html"),


    # @ajax POST
    path("teacher/task/oral/student/passage/update", v_task.v_oral_update,
         name="teacher_tmp_task_populate_ungraded_passages.html"),

    # @ajax POST
    path("teacher/task/oral/miscue/add", v_task.r_oral_miscue_add,
         name="teacher_tmp_task_populate_ungraded_passages.html"),

    # @ajax POST
    path("teacher/task/oral/miscue/remove", v_task.r_oral_miscue_remove,
         name="teacher_tmp_task_populate_ungraded_passages.html"),


    # @ajax POST
    path("teacher/task/oral/miscue/next", v_task.r_oral_miscue_next,
         name="teacher_tmp_task_populate_ungraded_passages.html"),


    # @ajax POST
    path("teacher/task/oral/print", v_task.v_oral_printing,
         name="teacher_tmp_task_populate_ungraded_passages.html"),

    # @ajax POST
    # silent and listening
    path("teacher/task/result/print", v_task.v_silent_listening_printing,
         name="teacher_tmp_task_populate_ungraded_passages.html"),


    # @ajax POST
    # silent and listening
    path("teacher/task/print", v_task.v_print_all_students_result,
         name="teacher_tmp_task_populate_ungraded_passages.html"),

]
