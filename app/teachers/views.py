import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
import django;
from account.models import UserProfile;
from account.v_teacher import \
    has_user_profile, \
    is_not_personal_set , \
    is_personal_set, \
    is_member,\
    has_no_user_profile;


@login_required(login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/personal")
@user_passes_test(test_func=is_member, login_url="/")
def index(request):


    of_user_profile = UserProfile.objects.get(user=request.user);

    user_profile_model = helpers._Model(of_user_profile);
    user_profile_info = user_profile_model.getValue();

    school_profile = UserProfile.objects.get(user=of_user_profile.school);

    school_profile_model = helpers._Model(school_profile);
    school_profile_info = school_profile_model.getValue();

    request.session['user_profile'] = user_profile_info;
    request.session['school_profile'] = school_profile_info;
    request.session['school_id'] = school_profile.school_id;

    return render(request, "testers_temp/teachers/teachers_home.html", {
        'title': "HOME",
        "user": request.user,
        "user_profile" : request.session['user_profile'],
        "school_profile" : request.session['school_profile'],
        "init": ""
    });


@login_required(login_url="/")
@user_passes_test(test_func=is_not_personal_set, login_url="/teacher/home")
@user_passes_test(test_func=is_member, login_url="/")
def v_personal_info(request):
    return render(request, "testers_temp/teachers/teachers_required.html", {
        'title': "INFO",
        "user": request.user,
        "init": ""
    });

    pass;


"""------------------------------------------------------------"""



@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_not_personal_set, login_url="/home")
def required_personal_info_set(request):
    post_request = helpers.constraint(request, "POST");
    data = post_request.strict([
        "access"
    ], True);

    try:

        school = UserProfile.objects.get(access_code=data['access'],isSchool=True);

        isExists = UserProfile.objects.filter(user=request.user).exists();

        if isExists:
            raise helpers.AlreadyExist("Invalid, Profile is already set");

        by_user_profile = UserProfile(
            user=request.user,
            access_code=data['access'],
            school=school.user,
            isSchool=False
        );

        by_user_profile.full_clean();
        by_user_profile.clean();
        by_user_profile.save();

    except UserProfile.DoesNotExist:

        return render(request, "testers_temp/teachers/teachers_required.html", {
            'title': "INFO",
            "user": request.user,
            "validation_error" : "Invalid Request. code is does'nt exist or you have no permission to join this "
                                 "institution",
            "init": ""
        });

        pass;

    return redirect("/home");


pass;
