
from . import v_batch
from django.urls import path

from . import v_classroom

urlpatterns = [
    path("teacher/batch", v_batch.index, name="home"),
    path("teacher/batch/students", v_batch.v_students, name="home"),
    path("teacher/batch/student/list", v_batch.v_student_list, name="home"),
    path("teacher/batch/add/student", v_batch.v_add_student_list, name="home"),
    path("teacher/batch/add/student/request", v_batch.r_add_student, name="home"),
    path("teacher/batch/list", v_batch.v_batch_list, name="home"),
    path("teacher/batch/student/info", v_batch.v_student_info, name="home"),
]
