
from . import v_activity
from django.urls import path

from . import v_classroom

urlpatterns = [
    path("teacher/activity", v_activity.index, name="home"),
    path("teacher/activity/remove", v_activity.r_remove, name="home"),
    path("teacher/activity/list", v_activity.v_activity_list, name="teacher_activity_list.html"),
    path("teacher/activity/list/request", v_activity.r_activity_list, name="home"),
    path("teacher/activity/add", v_activity.v_add_activity, name="teacher_add_activity.html"),
    path("teacher/activity/add/request", v_activity.r_add_activity, name="home"),
    path("teacher/activity/manage/home", v_activity.v_activity_manage, name="teacher_activity_manage.html"),

    path("teacher/activity/passage/add", v_activity.v_passage, name="teachers_{{oral,silent,listening}}.html"),
    path("teacher/activity/passage/add/request", v_activity.r_add_passage, name="home"),
    path("teacher/activity/passage/questions", v_activity.v_questions, name="home"),
    path("teacher/activity/passage/edit", v_activity.v_edit, name="home"),
    path("teacher/activity/passage/edit/request", v_activity.r_update_section, name="home"),
    path("teacher/activity/passage/delete", v_activity.r_delete_passage, name="home"),

    # modal

    path("teacher/activity/modal/add/request", v_activity.r_modal_add_activity, name="teacher_modal_activity_passages.html"),
    path("teacher/activity/modal/add", v_activity.v_add_activity, name="teacher_add_activity.html"),



]
