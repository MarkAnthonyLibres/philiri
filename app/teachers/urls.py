
from . import views
from . import v_sections
from . import v_students
from django.urls import path

urlpatterns = [
    path("teacher/home", views.index, name="home"),
    path("teacher/home/personal/set", views.required_personal_info_set, name="home"),
    path("teacher/home/personal", views.v_personal_info, name="home"),

    path("teacher/section", v_sections.index, name="home"),

    path("teacher/students", v_students.index, name="home"),

]
