import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from group.models import GroupLevel, Section, Batch
from account.v_teacher import \
    has_user_profile, \
    is_not_personal_set, \
    is_personal_set, \
    is_member;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def index(request):

    return render(request, "testers_temp/teachers/students/teachers_student_index.html", {
        'title': "STUDENTS",
        "user_profile": request.session['user_profile'],
        "school_profile": request.session['school_profile'],
        "init": ""
    });

    pass;
