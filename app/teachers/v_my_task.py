import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
import django;
from django.core.paginator import Paginator;

from account.v_teacher import \
    has_user_profile, \
    is_not_personal_set , \
    is_personal_set, \
    is_member

from exercises.models import Task;
from django.db.models import Sum, Q;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")

def index(request):

    try:

        return render(request,"testers_temp/teachers/my_task_tab"
                              "/teachers_my_activity.html",{
            "title": "MY ACTIVITY"
        });

    except helpers.InvalidRequest:
        messages.error(request, "Invalid Parameters");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_populate_list_of_task(request):

    try:
        constraint = helpers.constraint(request, "POST");

        data = constraint.safe({
            "pageIndex": True,
            "pageSize": True,
            "search" : False
        });

        school = request.session['school_id'];

        of_task_list = Task\
            .objects\
            .filter(
                Q(user_id=request.user),
                Q(section__group__school_id=school),
                Q(activity__title__icontains=data['search']) |
                Q(name__icontains=data['search']),
            )\
            .order_by("-id");

        split = Paginator(of_task_list, int(data["pageSize"]));
        split_by = split.page(int(data['pageIndex']));
        data_splitted = split_by.object_list;

        return render(request, "testers_temp/teachers/my_task_tab/ajax_temp"
                              "/teachers_my_activity_populate.html", {
            'data': data_splitted,
            'itemCount' : len(of_task_list)
        });


    except helpers.InvalidRequest:
        messages.error(request, "Invalid Parameters");
        return redirect("/home");

    pass;
