import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from group.models import GroupLevel, Section, Batch
from account.v_teacher import \
    has_user_profile, \
    is_not_personal_set , \
    is_personal_set, \
    is_member
import django;
import json;
import random, string;
from class_sem.models import batch_student
from exercises.models import Activity, Quiz_activity, Quiz_assesment
from django.core.exceptions import ValidationError
from exercises.models import Task
from exercises.models import Assesment, Questions
from django.db.models import Sum, Q, Value, Count;
from django.db.models.functions import Concat;
from django.core.paginator import Paginator;
from exercises.forms import AssesmentForm


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def index(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "activity"
        ], True);

        school = request.session['school_id'];

        of_activity = Activity.objects.get(
            id=data['activity'],
            group_level__school_id=school
        );

        of_activity.question_set = Questions\
            .objects\
            .filter(assesment__activity_id=of_activity.id)\
            .count();

        of_assesment = Assesment\
            .objects\
            .filter(activity_id=of_activity.id)\
            .order_by("title");

        of_activity.passage_set = of_assesment.count();

        for per in of_assesment:
            per.question_set = Questions\
                .objects\
                .filter(assesment_id=per.id)\
                .count();
            pass;

        of_activity.assesment_list = of_assesment;

        if not (of_activity.test_type == 1):

            of_activity.student_taken = Quiz_activity\
            .objects\
            .filter(
                task__activity_id=of_activity.id,
                is_done=True
            )\
            .aggregate(Count('batch_student__student_id'))['batch_student__student_id__count'];

        else:
            of_activity.student_taken= Quiz_assesment\
                .objects\
                .filter(
                    quiz_activity__is_done=True,
                    assesment__activity_id=of_activity.id
                ).aggregate(
                    Count('quiz_activity__batch_student__student_id')
                )['quiz_activity__batch_student__student_id__count'];

            pass;

        of_activity.question_type = of_activity.questions_type_length();

        of_activity.task_added = Task\
            .objects\
            .filter(activity_id=of_activity.id)\
            .count();

        return render(request, "testers_temp/teachers/classroom/teacher_activity_info.html", {
            'title': "ACTIVITY",
            "group_level" :of_activity.group_level,
            "user_profile" : request.session['user_profile'],
            "school_profile" : request.session['school_profile'],
            "group_roman" :  helpers._String.int_to_roman(of_activity.group_level.level),
            "activity": of_activity,
            "user" : request.user,
            "rates" : of_activity.comprehension_level_percent_all()
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Activity.DoesNotExist:

        messages.error(request,{
            "title" : helpers.message.INVALID_REQUEST ,
            "content" : "Activity id does not exists and invalid"
        });

        return redirect("/");

    pass;

def r_remove(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "activity"
        ], True);

        school = request.session['school_id'];

        of_activity = Activity.objects.get(
            id=data['activity'],
            group_level__school_id=school,
            user_added=request.user
        );

        title = of_activity.title;
        group = of_activity.group_level_id;

        of_activity.delete();

        messages.error(request,{
            "title" : helpers.message.SUCCESS ,
            "content" : 'Activity, "'+title+'" is successfully removed'
        });

        return redirect("/teacher/activity/list?group="+str(group));

    except helpers.InvalidRequest:
        return redirect("/home");

    except Activity.DoesNotExist:

        messages.error(request,{
            "title" : helpers.message.INVALID_REQUEST ,
            "content" : "Activity id does not exists and invalid"
        });

        return redirect("/");

    pass;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_activity_list(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        school = request.session['school_id'];

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school_id=school
        );

        return render(request, "testers_temp/teachers/classroom/teacher_activity_list.html", {
            'title': "ACTIVITY",
            "activity_params" : Activity,
            "user_profile" : request.session['user_profile'],
            "school_profile" : request.session['school_profile'],
            "group_level" : of_group_level,
            "group_roman" :  helpers._String.int_to_roman(of_group_level.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_activity_list(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        fn_post = helpers.constraint(request, "POST");
        fn_post_data = fn_post.safe({
            "search" : False,
            "pageIndex" : True,
            "pageSize" : True
        });

        school = request.session['school_id'];

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school_id=school
        );

        of_activity_list = Activity\
            .objects\
            .annotate(fullname=Concat('user_added__first_name', Value(' '),'user_added__last_name'))\
            .filter(
                Q(group_level_id=data['group']),
                Q(group_level__school_id=school),
                Q(title__icontains=fn_post_data['search']) |  Q(fullname__icontains=fn_post_data['search'])
            ).order_by("-id");

        if not of_activity_list: return HttpResponse();

        split = Paginator(of_activity_list, int(fn_post_data["pageSize"]));
        split_by = split.page(int(fn_post_data['pageIndex']));
        data_splitted = split_by.object_list;

        for per_activity in data_splitted:

            per_activity.rates = per_activity.comprehension_level_percent_all();
            per_activity.question_set = Questions\
                .objects\
                .filter(assesment__activity_id=per_activity.id).count();

            per_activity.passage_set = Assesment\
                .objects\
                .filter(activity_id=per_activity.id).count();

            per_activity.question_type = per_activity.questions_type_length();

            pass;

        return render(request, "testers_temp/teachers/classroom/misc"
                               "/teacher_tmp_activity_list.html", {
            'title': "ACTIVITY",
            "user_profile" : request.session['user_profile'],
            "school_profile" : request.session['school_profile'],
            "group_level" : of_group_level,
            "activity_list" : data_splitted,
            "group_roman" :  helpers._String.int_to_roman(of_group_level.level),
            "itemCount" : of_activity_list.count(),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_add_activity(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "group"
        ], True);

        school = request.session['school_id'];

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school_id=school
        );

        of_batch = Batch\
            .objects\
            .filter(school_id=school)\
            .order_by("start_date","end_date")\
            .values("id","name");

        of_section = Section\
            .objects\
            .filter(group_id=data['group'])\
            .order_by("name");

        return render(request, "testers_temp/teachers/classroom/teacher_add_activity.html", {
            'title': "ACTIVITY",
            "user_profile" : request.session['user_profile'],
            "school_profile" : request.session['school_profile'],
            "group_level" : of_group_level,
            "section" : of_section,
            "group_roman" :  helpers._String.int_to_roman(of_group_level.level),
            "batch" : of_batch,
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    pass;



@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_add_activity(request, is_render = False):

    try:

        constraint = helpers.constraint(request, "POST");

        data = constraint.safe({
            "group" : True,
            "title" : True,
            "description" : False,
            "type" : True,
            "literacy_type" : True,
            "complexity_level" : True
        });

        school = request.session['school_id'];

        of_group_level = GroupLevel.objects.get(
            id=data['group'],
            school_id=school
        );

        of_activity = Activity(
            title=data['title'],
            description=data['description'],
            group_level=of_group_level,
            test_type=data['type'],
            type_of_literacy=data['literacy_type'],
            level_of_complexity=data['complexity_level'],
            user_added=request.user
        );

        of_activity.full_clean();
        of_activity.clean();
        of_activity.save();

        raise helpers.Success("Request is valid");

    except ValidationError as e:

        messages.error(request,{
            "title" : helpers.message.VALIDATION_ERROR ,
            "content" : e
        });

        return redirect("/teacher/activity/add?group=" + data['group']);

        pass;

    except helpers.Success:

        default_redirect_to = "/teacher/activity/manage/home?id=" + str(of_activity.id);

        if is_render :
            request.redirect_to = request.redirect_to or default_redirect_to;
            return render(request,request.redirect_to, {
                "activity" : of_activity,
                "data" : data
            });
        else :
            return redirect(default_redirect_to);
            pass;


    except helpers.InvalidRequest:
        raise helpers.InvalidRequest("Request is not valid");

    except GroupLevel.DoesNotExist as e:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");



    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_modal_add_activity(request):

    request.redirect_to = "testers_temp/teachers/activity/misc/" \
                          "teacher_modal_activity_passages.html";
    return r_add_activity(request, is_render = True);


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_activity_manage(request):

    try:

        get_request = helpers.constraint(request, "GET");
        data = get_request.strict([
            "id"
        ], False);

        school = request.session['school_id'];

        ofActivity = Activity.objects.get(
            id=data['id'],
            user_added=request.user,
            group_level__school_id=school
        );

        of_assesment_count = Assesment\
            .objects\
            .filter(activity_id=ofActivity.id)\
            .count();



        return render(request, "testers_temp/teachers/classroom/teacher_activity_manage.html", {
            'title': "ACTIVITY",
            "activity_id": data['id'],
            "activity": ofActivity,
            "data": ofActivity,
            "is_task" : 1 if "task" in request.GET else 0,
            "school_profile" : request.session['school_profile'],
            "assesment_count" : of_assesment_count
        });

    except AttributeError:

        messages.error(request,{
            "title" : helpers.message.INVALID_REQUEST ,
            "content" : "No selected activity specified"
        });

        return redirect("/");

    except Activity.DoesNotExist:

        # messages.error(request, "section does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "the specified activity does not exist and invalid"
        });

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_passage(request):

    try:

        get_request = helpers.constraint(request, "GET");
        data = get_request.strict(['index'], True);

        school = request.session['school_id'];

        # Error if does not exists
        of_activity = Activity.objects.get(
            id=data['index'],
            group_level__school_id=school
        );

        title = str(of_activity.get_type_of_literacy_display()).upper();

        return render(request, "testers_temp/teachers/activity/teachers_"
                      + of_activity.get_literacy_type() + ".html", {
            'title': title,
            "activity": of_activity,
            "school_profile" : request.session['school_profile'],
            "group_levels" : GroupLevel.objects.order_by("id","level").all()
        });

    except Activity.DoesNotExist:

        messages.error(request, "The Activity id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Activity id does not exist and invalid"
        });

    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_add_passage(request):
    try:

        constraint = helpers.constraint(request, "POST");

        data = constraint.safe({
            "type" : True,
            "activity" : True,
            "assesment" : False,
            "title" : True,
            "body" : True,
            "grade_level_of_complexity" : True
        });


        school = request.session['school_id'];

        of_activity = Activity.objects.get(
            id=data['activity'],
            group_level__school_id=school,
            type_of_literacy=data['type']
        );


        of_grade = GroupLevel.objects.get(
            id=data['grade_level_of_complexity'],
            school_id=school
        );

        assesment_form = {
            'title' : data['title'],
            'body' : data['body'],
            'type' : data['type'],
            'activity' : data['activity'],
            'grade_level_of_complexity' : data['grade_level_of_complexity']
        };

        form = AssesmentForm(data=assesment_form, files=request.FILES);

        form.full_clean();
        form.clean();
        form.save();

        messages.success(request,"New passage has been added", extra_tags="Success");

        return redirect("/teacher/activity/manage/home?id=" + str(data['activity']));


    except ValidationError as e:
        messages.error(request,{
            "title" : helpers.message.VALIDATION_ERROR ,
            "content" : e
        });

        return redirect("/teacher/activity/add?group=" + data['group']);

        pass;

    except helpers.InvalidRequest:

        messages.error(request,
                       "invalid data parameters supply through action.",
                       extra_tags="External Error");

        return redirect("/home");

    except GroupLevel.DoesNotExist:
        messages.success(request,"Group id does not exists and invalid", extra_tags="Internal Error");
        return redirect("/home");

    except Activity.DoesNotExist as e:
        messages.error(request, "Activity id does not exist and invalid");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_questions(request):

    try:

        constraint = helpers.constraint(request, "GET");

        data = constraint.strict(["assesment"],True);

        school = request.session['school_id'];

        of_assesment = Assesment.objects.get(
            id=data['assesment'],
            activity__group_level__school_id=school,
            activity__user_added=request.user
        );

        return render(request, "testers_temp/teachers/activity/teachers_question.html", {
            'title': "ACTIVITY",
            "init": "",
            "assesment": of_assesment,
            "activity" : of_assesment.activity,
            "school_profile" : request.session['school_profile']
        });

    except ValidationError as e:

        messages.error(request,{
            "title" : helpers.message.VALIDATION_ERROR ,
            "content" : e
        });

        return redirect("/teacher/activity/add?group=" + data['group']);

        pass;

    except helpers.InvalidRequest:
        return redirect("/home");

    except Assesment.DoesNotExist as e:
        messages.error(request, "Assesment id does not exist and invalid");
        return redirect("/home");

    pass;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_edit(request):

    try:

        constraint = helpers.constraint(request, "GET");

        data = constraint.strict(["assesment"],True);

        school = request.session['school_id'];

        of_assesment = Assesment.objects.get(
            id=data['assesment'],
            activity__group_level__school_id=school,
            activity__user_added=request.user
        );

        type = of_assesment.activity.get_literacy_type();

        return render(request, "testers_temp/teachers/activity/teachers_" + type + ".html",
        {
            'title': "ACTIVITY",
            "init": "",
            "assesment": of_assesment,
            "activity" : of_assesment.activity,
            "school_profile" : request.session['school_profile'],
            "group_levels" : GroupLevel.objects.order_by("id","level").all(),
            "is_update" : True
        });

    except ValidationError as e:

        messages.error(request,{
            "title" : helpers.message.VALIDATION_ERROR ,
            "content" : e
        });

        return redirect("/teacher/activity/add?group=" + data['group']);

        pass;

    except helpers.InvalidRequest:
        return redirect("/home");

    except Assesment.DoesNotExist as e:
        messages.error(request, "Assesment id does not exist and invalid");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_update_section(request):

    try:

        post_request = helpers.constraint(request, "POST");
        post_data = post_request.strict([
            "assesment"
        ], False);

        _assesment = Assesment.objects.get(id=request.POST['assesment']);
        form = AssesmentForm(request.POST or None, request.FILES or None, instance=_assesment)

        if form.is_valid():
            edit = form.save(commit=False);
            edit.save();

        messages.success(request,"New changes has been saved.", extra_tags="Successfully Update");

        return redirect("/teacher/activity/manage/home?id=" + str(_assesment.activity_id));


    except helpers.InvalidRequest:
        return redirect("/home");

    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;



@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_delete_passage(request):

    try:

        post_request = helpers.constraint(request, "POST");
        data = post_request.strict([
            "assesment"
        ], True);

        of_assesment = Assesment.objects.get(
            id=data['assesment'],
            activity__user_added=request.user
        );

        of_assesment.delete();

        messages.success(request,"Successfully deleted a record", extra_tags="Record Deleted");

        return redirect("/teacher/activity/manage/home?id=" + str(of_assesment.activity_id));

    except helpers.InvalidRequest:
        messages.error(request, "Invalid Parameters");
        return redirect("/home");

    except Assesment.DoesNotExist:

        messages.error(request, "The Assesment id does not exist and invalid");
        return render(request, "error.html", {
            "status": "505",
            "message": "The Assesment id does not exist and invalid"
        });

    except Exception as e:
        messages.error(request, e);
        return redirect("/");

    pass;








