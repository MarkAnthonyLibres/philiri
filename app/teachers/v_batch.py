import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from group.models import GroupLevel, Section, Batch
from account.v_teacher import \
    has_user_profile, \
    is_not_personal_set, \
    is_personal_set, \
    is_member;

from account.views import Category;

import django;
import json;
import random, string;
from class_sem.models import batch_student
from django.db.models import Q, Value, Count;
from django.db.models.functions import Concat;
from django.core.paginator import Paginator;
from exercises.models import Quiz_activity


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def index(request):

    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "batch",
            "section"
        ], True);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );


        of_batch = Batch.objects.get(
            id=data['batch'],
            school_id=school
        );

        return render(request, "testers_temp/teachers/classroom/teacher_batch_info.html", {
            'title': "SECTION",
            "user_profile": request.session['user_profile'],
            "school_profile": request.session['school_profile'],
            "section": of_section,
            "batch": of_batch,
            "group_roman": helpers._String.int_to_roman(of_section.group.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    except Batch.DoesNotExist:
        messages.error(request, "Batch id does not exist and invalid");
        return redirect("/home");
    pass;

    pass;

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_students(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "batch",
            "section"
        ], True);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );

        of_batch = Batch.objects.get(
            id=data['batch'],
            school_id=school
        );

        return render(request, "testers_temp/teachers/classroom/teacher_batch_students.html", {
            'title': "BATCH",
            "user_profile": request.session['user_profile'],
            "school_profile": request.session['school_profile'],
            "section": of_section,
            "batch": of_batch,
            "group_roman": helpers._String.int_to_roman(of_section.group.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    except Batch.DoesNotExist:
        messages.error(request, "Batch id does not exist and invalid");
        return redirect("/home");
    pass;

    pass;

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_student_list(request):

    try:

        constraint = helpers.constraint(request, "POST");
        data = constraint.strict([
            "batch",
            "section",
            "search",
            "pageIndex",
            "pageSize"
        ], False);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );

        of_batch = Batch.objects.get(
            id=data['batch'],
            school_id=school
        );

        of_batch_student = batch_student\
            .objects\
            .annotate(fullname=Concat('student__first_name', Value(' '),'student__last_name'))\
            .filter(
                Q(batch__school_id=school),
                Q(batch=of_batch),
                Q(section=of_section),
                Q(student__groups=Category.id_student),
                Q(fullname__icontains=data['search'])
            );

        split = Paginator(of_batch_student, int(data["pageSize"]));
        split_by = split.page(int(data['pageIndex']));
        data_splitted = split_by.object_list;

        for per in data_splitted:
            per.rate_percent = per.rate(Quiz_activity);
            pass;

        return render(request, "testers_temp/teachers/classroom/misc"
                               "/teacher_tmp_activity_student_added.html", {
            'title': "BATCH",
            "section": of_section,
            "batch": of_batch,
            "batch_student_list" : data_splitted,
            "itemCount" : len(of_batch_student),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    except Batch.DoesNotExist:
        messages.error(request, "Batch id does not exist and invalid");
        return redirect("/home");
    pass;

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_add_student_list(request):

    try:

        constraint = helpers.constraint(request, "POST");
        data = constraint.strict([
            "batch",
            "section",
            "search",
            "pageIndex",
            "pageSize"
        ], False);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );

        of_batch = Batch.objects.get(
            id=data['batch'],
            school_id=school
        );

        of_user_profile = UserProfile\
            .objects\
            .annotate(fullname=Concat('user__first_name', Value(' '),'user__last_name'))\
            .filter(
                Q(school_id=school),
                Q(isSchool=False),
                Q(user__groups=Category.id_student),
                Q(fullname__icontains=data['search'])
            );

        split = Paginator(of_user_profile, int(data["pageSize"]));
        split_by = split.page(int(data['pageIndex']));
        data_splitted = split_by.object_list;

        for per in data_splitted:

            per.is_already_added = batch_student.objects.filter(
                batch__school_id=school,
                batch_id=of_batch.id,
                section_id=of_section.id,
                student_id=per.user_id
            ).exists();

        return render(request, "testers_temp/teachers/classroom/misc"
                               "/teacher_tmp_activity_student.html", {
            'title': "BATCH",
            "section": of_section,
            "batch": of_batch,
            "user_profile_list" : data_splitted,
            "itemCount" : len(of_user_profile),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    except Batch.DoesNotExist:
        messages.error(request, "Batch id does not exist and invalid");
        return redirect("/home");
    pass;

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def r_add_student(request):

    try:

        constraint = helpers.constraint(request, "POST");
        data = constraint.strict([
            "batch",
            "section",
            "student"
        ], True);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );

        of_batch = Batch.objects.get(
            id=data['batch'],
            school_id=school
        );

        of_student = User.objects.get(id=data['student']);

        of_batch_student = batch_student(
            batch=of_batch,
            section=of_section,
            student=of_student,
            user_added=request.user
        );

        of_batch_student.full_clean();
        of_batch_student.clean();
        of_batch_student.save();

        return HttpResponse("true");

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    except Batch.DoesNotExist:
        messages.error(request, "Batch id does not exist and invalid");
        return redirect("/home");

    except User.DoesNotExist:
        messages.error(request, "User id does not exist and invalid");
        return redirect("/home");

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_batch_list(request):
    try:

        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "section"
        ], True);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );

        of_batch = Batch \
            .objects \
            .filter(school_id=school) \
            .order_by("start_date", "end_date") \
            .values("id", "name", "start_date", "end_date", "description");

        for per_batch in of_batch:
            per_batch['rates'] = Batch \
                .of_comprehension_level_percent(per_batch['id'], batch_student);

        pass;

        return render(request, "testers_temp/teachers/classroom/teacher_batch_lists.html", {
            'title': "SECTION",
            "user_profile": request.session['user_profile'],
            "school_profile": request.session['school_profile'],
            "section": of_section,
            "batch_lists": of_batch,
            "group_roman": helpers._String.int_to_roman(of_section.group.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");
    pass;

    pass;


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_student_info(request):

    return render(request, "testers_temp/teachers/classroom"
                           "/teacher_batch_student_info.html", {
        'title': "STUDENT",
        "user_profile": request.session['user_profile'],
        "school_profile": request.session['school_profile'],
        "init": ""
    });

    pass;


