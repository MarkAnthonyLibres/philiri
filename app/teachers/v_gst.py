import sys;
from django.shortcuts import render, HttpResponse
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth import logout
from modules import helpers
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required, user_passes_test;
from account.models import UserProfile;
from group.models import GroupLevel, Section, Batch
from account.v_teacher import \
    has_user_profile, \
    is_not_personal_set, \
    is_personal_set, \
    is_member;

from account.views import Category;

import django;
import json;
import random, string;
from class_sem.models import batch_student
from django.db.models import Q, Value, Count;
from django.db.models.functions import Concat;
from django.core.paginator import Paginator;
from exercises.models import Activity, Quiz_activity, Assesment, Questions
from exercises.models import Task


@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def index(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "batch",
            "section"
        ], True);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );

        of_batch = Batch.objects.get(
            id=data['batch'],
            school_id=school
        );

        return render(request, "testers_temp/teachers/classroom/teacher_batch_task.html", {
            'title': "TASK",
            "user_profile": request.session['user_profile'],
            "school_profile": request.session['school_profile'],
            "section": of_section,
            "batch": of_batch,
            "group_roman": helpers._String.int_to_roman(of_section.group.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    except Batch.DoesNotExist:
        messages.error(request, "Batch id does not exist and invalid");
        return redirect("/home");
    pass;

@login_required(login_url="/")
@user_passes_test(test_func=is_member, login_url="/")
@user_passes_test(test_func=is_personal_set, login_url="/teacher/home/info")
def v_list(request):

    try:
        constraint = helpers.constraint(request, "GET");
        data = constraint.strict([
            "batch",
            "section"
        ], True);

        school = request.session['school_id'];

        of_section = Section.objects.get(
            id=data['section'],
            group__school_id=school
        );

        of_batch = Batch.objects.get(
            id=data['batch'],
            school_id=school
        );

        return render(request, "testers_temp/teachers/classroom/teacher_batch_gst.html", {
            'title': "TASK",
            "user_profile": request.session['user_profile'],
            "school_profile": request.session['school_profile'],
            "section": of_section,
            "batch": of_batch,
            "test_type" : 0,
            "group_roman": helpers._String.int_to_roman(of_section.group.level),
            "init": ""
        });

    except helpers.InvalidRequest:
        return redirect("/home");

    except Section.DoesNotExist:
        messages.error(request, "section does not exist and invalid");
        return redirect("/home");

    except Batch.DoesNotExist:
        messages.error(request, "Batch id does not exist and invalid");
        return redirect("/home");


