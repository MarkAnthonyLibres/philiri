from django.shortcuts import render, HttpResponse, redirect;
from django.contrib.auth.models import User;
from modules import helpers

# Create your views here.

def is_personal_info_set(request):
    of_user = request.user;

    if not of_user.get_full_name() : return False;
    if not of_user.email : return False;
    if not of_user.username : return False;
    return True;

    pass;

def index(request):

    if not request.user.is_authenticated :
        return redirect("/");

    group = request.user.groups.last();
    name = str(group.name).lower();

    return redirect("/" + name + "/home");
