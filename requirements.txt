asgiref==3.2.3
cachetools==4.1.1
cairocffi==1.1.0
CairoSVG==2.4.2
certifi==2020.6.20
cffi==1.14.0
chardet==3.0.4
cryptography==3.0
cssselect2==0.3.0
defusedxml==0.6.0
Django==3.0
django-extensions==3.0.3
django-filter==2.2.0
django-sslserver==0.22
google-api-core==1.22.0
google-auth==1.19.2
google-cloud-speech==1.3.2
googleapis-common-protos==1.52.0
grpcio==1.30.0
html5lib==1.1
httplib2==0.18.1
idna==2.10
mysqlclient==1.4.6
oauth2client==4.1.3
pdfkit==0.6.1
Pillow==6.2.1
protobuf==3.12.2
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycparser==2.20
pyOpenSSL==19.1.0
PyPDF2==1.26.0
Pyphen==0.9.5
pyphonetics==0.5.3
PyQt4==4.11.4
PyQt5==5.15.0
PyQt5-sip==12.8.0
pytz==2019.3
reportlab==3.5.46
requests==2.24.0
rsa==4.6
simpleaudio==1.0.4
simplejson==3.17.0
six==1.15.0
SpeechRecognition==3.8.1
sqlparse==0.3.0
tinycss2==1.0.2
Unidecode==1.1.1
urllib3==1.25.10
WeasyPrint==51
webencodings==0.5.1
Werkzeug==1.0.1
wkhtmltopdf==0.2
xhtml2pdf==0.2.4
